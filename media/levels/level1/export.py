##############################################
# Exporter Hunter
##############################################

ID1 = ' '*2    # Identadores para el xml
ID2 = ' '*4    # Solo con proposito de obtener un xml "bonito"
ID3 = ' '*6
ID5 = ' '*8
ID6 = ' '*10
ID7 = ' '*12
ID8 = ' '*14
ID9 = ' '*16

import bpy, os, sys
import mathutils
from math import *
from bpy import *

LEVEL = 1 # Definición de nivel
TOTAL_DIMENSION_X = 64
TOTAL_DIMENSION_Y = 64
EDGE_CELL_DIMENSION = 6.25

FOLDER = "/home/jose/dev/workspace/Hunter/media/levels/level1/"
FILENAME = FOLDER + "output.xml"       # Archivo XML de salida
GRAPHNAME = "Terrain"           # Nombre del objeto Mesh del grafo




graph = bpy.data.objects[GRAPHNAME]

# Función que hace la exportación de un tipo de elemento
# que es recibido por parámetro
# ----------------------------------------------------------------------
def exportElements(element):
    elCounter=0
    obs = [ob for ob in bpy.data.objects]
    for el in obs:
        eName = el.name    	
        if (eName.find(element) != -1):
            elCounter = elCounter + 1
            print(ID1 + '<element id="'+element+'_'+str(elCounter)+'" type="'+element.upper()+'">')
            print(ID2 + "<mesh>"+element+".mesh</mesh>")  
            x,y,z = el.location
            print (ID2 + '<x>%.2f</x> <y>%.2f</y> <z>%.2f</z>' % (round(x,2),round(y,2),round(z,2)))
            rw,rx,ry,rz = el.rotation_quaternion
            print (ID2 + '<rw>%.2f</rw> <rx>%.2f</rx> <ry>%.2f</ry> <rz>%.2f</rz>' % (round(rw,2), round(rx,2),round(ry,2),round(rz,2)))
            print(ID1 + "</element>")       							

# Función que hace la exportación de un generador de caracteres pasado por parámetro
# ----------------------------------------------------------------------
def exportGenerator(element):
    obs = [ob for ob in bpy.data.objects]
    for el in obs:
        eName = el.name    	
        fName = element + "Generator"
        if (eName.find(fName) != -1):
            print(ID1 + '<' + fName +'>')
            x,y,z = el.location
            print (ID2 + '<x>%.2f</x> <y>%.2f</y> <z>%.2f</z>' % (round(x,2),round(y,2),round(z,2)))
            print (ID2 + '<numCharacters>'+str(el['maxNumber'])+'</numCharacters>')
            rx,ry,rz = el.rotation_euler
            print(ID1 + '</' + fName +'>')      							
                      
# Función que hace la exportación de recarga de municiçon
# ----------------------------------------------------------------------
def exportWeaponGenerator(element):
    obs = [ob for ob in bpy.data.objects]
    for el in obs:
        eName = el.name    	
        fName = element + "Generator"
        if (eName.find(fName) != -1):
            print(ID1 + '<' + fName +'>')
            x,y,z = el.location
            print (ID2 + '<x>%.2f</x> <y>%.2f</y> <z>%.2f</z>' % (round(x,2),round(y,2),round(z,2)))
            print (ID2 + '<numItems>'+str(el['maxNumber'])+'</numItems>')
            rx,ry,rz = el.rotation_euler
            print(ID1 + '</' + fName +'>')                  
       
# Crea una entrada con las características iniciales del tipo de enemigo
# ----------------------------------------------------------------------      
def createEnemy(type,speed,life):
    print(ID1 + '<' + type +'>')
    print(ID2 + '<speed>'+str(speed)+'</speed>')  
    print(ID2 + '<life>'+str(life)+'</life>')  
    print(ID2 + '<mesh>'+type+'.mesh</mesh>')    
    print(ID1 + '</' + type +'>')  


# Crea una entrada con las características iniciales del tipo de enemigo
# ----------------------------------------------------------------------      
def createHuman(type,speed,life):
    print(ID1 + '<' + type +'>')
    print(ID2 + '<speed>'+str(speed)+'</speed>')  
    print(ID2 + '<life>'+str(life)+'</life>')  
    print(ID2 + '<mesh>'+type+'.mesh</mesh>')    
    print(ID1 + '</' + type +'>')  



# Crea una entrada con las características iniciales del tipo de player
# ----------------------------------------------------------------------      
def createPlayer(type,speed,speedPitch):
    obs = [ob for ob in bpy.data.objects]
    for el in obs:
        eName = el.name    	
        fName = type + "Generator"
        if (eName.find(fName) != -1):
            print(ID1 + '<' + type +'>')
            print(ID2 + '<speed>'+str(speed)+'</speed>')  
            print(ID2 + '<speedPitch>'+str(speedPitch)+'</speedPitch>') 
            x,y,z = el.location
            print (ID2 + '<x>%.2f</x> <y>%.2f</y> <z>%.2f</z>' % (round(x,2),round(y,2),round(z,2)))
            rw,rx,ry,rz = el.rotation_quaternion
            print (ID2 + '<rw>%.2f</rw> <rx>%.2f</rx> <ry>%.2f</ry> <rz>%.2f</rz>' % (round(rw,2), round(rx,2),round(ry,2),round(rz,2)))
           
            print(ID2 + '<mesh>'+type+'.mesh</mesh>')  
            print(ID2 + "<weapon>machineGun</weapon>")
            print(ID2 + "<weapon>devastator</weapon>")  
            print(ID1 + '</' + type +'>')  
    
## Crear diccionario de poligonos
## ----------------------------------------------------------------------
#dp = {}        # Diccionario de caras                    
#for face in graph.data.polygons:        
#    dp[face.index] = face.center
#    
## Crear diccionario de vertices
## ----------------------------------------------------------------------
#dv = {}        # Diccionario de vertices                   
#for vertex in graph.data.vertices:        
#	dv[vertex.index] = vertex.co

# Crea una entrada con las características iniciales del tipo de player
# ----------------------------------------------------------------------      
def exportVertex():
    _vertexIndex = 0
    obs = [ob for ob in bpy.data.objects]
    for el in obs:
        if (el.name.find("empty_open_") != -1 or el.name.find("empty_openZombie_") != -1 or el.name.find("empty_closed_") != -1):
            eName = el.name.split("_")[1]
            eNumber = el.name.split("_")[2]    	
            
            print (ID2 + '<cell index="' + str(_vertexIndex) + '" type="'+ eName +'" blender="'+el.name+'">')
            x,y,z = el.location
            print (ID3 + '<x>%.2f</x> <y>%.2f</y> <z>%.2f</z>' % (round(x,2),round(y,2),round(z,2)))
            print (ID2 + '</cell>')
            _vertexIndex = _vertexIndex + 1
     
    
# ################################################################################
# Exportar!!!!
# ################################################################################
file = open(FILENAME, "w")
std=sys.stdout
sys.stdout=file

print ("<?xml version='1.0' encoding='UTF-8'?>")
print ("<map>")

# Datos generales
print(ID1 + "<level>"+str(LEVEL)+"</level>")
print(ID1 + "<totalDimensionX>"+str(TOTAL_DIMENSION_X)+"</totalDimensionX>")
print(ID1 + "<totalDimensionY>"+str(TOTAL_DIMENSION_Y)+"</totalDimensionY>")
print(ID1 + "<edgeCellDimension>"+str(EDGE_CELL_DIMENSION)+"</edgeCellDimension>")

print(ID1 + "<missions>")
print(ID2 +  "<goal type='killEnemies'>")
print(ID3 +  "<title>Kill all zombies</title>")
print(ID2 +  "</goal>")
print(ID2 +  "<goal type='rescueSurvivors'>")
print(ID3 +  "<title>Rescue survivors</title>")
print(ID3 +  "<units>10</units>")
print(ID2 +  "</goal>")
print(ID2 +  "<goal type='specialWeapon'>")
print(ID3 +  "<title>Gather parts of special weapon</title>")
print(ID3 +  "<units>3</units>")
print(ID2 +  "</goal>")
print(ID2 +  "<goal type='killBoss'>")
print(ID3 +  "<title>Kill evil source</title>")
print(ID3 +  "</goal>")
print(ID2 +  "</missions>")

print(ID1 +  "<weapon type='devastator'>")
print(ID2 +  "<ammo>100</ammo>")
print(ID2 +  "<force>5</force>")
print(ID3 +  "<mesh>devastator.mesh</mesh>")
print(ID2 +  "</weapon>")
print(ID1 +  "<weapon type='machineGun'>")
print(ID2 +  "<ammo>100</ammo>")
print(ID2 +  "<force>1</force>")
print(ID3 +  "<mesh>machineGun.mesh</mesh>")
print(ID2 +  "</weapon>")


exportGenerator("zombie") # Exportar generadores
exportGenerator("human") # Exportar generadores



createPlayer("player",5,15)  
createEnemy("zombie1",1.3,5)  
createHuman("human1",1.4,5)  

exportElements("House1") # Exportar las casas
exportElements("House2") # Exportar las casas
exportElements("House3") # Exportar las casas
exportElements("box1") # Exportar las cajas
exportElements("fenceWood1") # Exportar las vayas
exportElements("wagenRaeder1") # Exportar los carros
exportElements("Terrain") # Exportar los carros


exportWeaponGenerator("devastator") # Exportar generadores
exportWeaponGenerator("machineGun") # Exportar generadores

# Exportar GRAFO
print (ID1 + '<grid>')    

exportVertex()
    
print (ID1 + "</grid>")
print ("</map>")

file.close()
sys.stdout = std
