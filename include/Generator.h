#ifndef GENERATOR_H
#define GENERATOR_H

#include "Ogre.h"

typedef enum
{
	 EN_ZOMBIE	= 0x01,
	 EN_HUMAN = 0x02
}ENEMY_TYPE;

class Generator
{
	public:
		Generator(Ogre::Vector3 position = Ogre::Vector3(0,0,0), ENEMY_TYPE typeGenerator = EN_ZOMBIE, int numEnemies = 10);
		Generator(float x = 0.0f, float y = 0.0f, float z = 0.0f, ENEMY_TYPE typeGenerator = EN_ZOMBIE, int numEnemies = 10);

		void setPosition(Ogre::Vector3 pos);
		void setPosition(float x, float y,  float z);
		Ogre::Vector3 getPosition() const;

		void setTypeGenerator(ENEMY_TYPE typeGenerator);
		ENEMY_TYPE getTypeGenerator() const;

		void setNumCharacters(int numCharacters);
		int getNumCharacters() const;

	protected:
		Ogre::Vector3	_position;
		ENEMY_TYPE		_typeGenerator;
		int				_numCharacters;
};

#endif
