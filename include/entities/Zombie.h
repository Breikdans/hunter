#ifndef ZOMBIE_H
#define ZOMBIE_H

#include <iostream>
#include <boost/asio.hpp>
#include <boost/bind.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>

#include "Character.h"

class Zombie : public Character
{
	public:
		Zombie(Ogre::SceneNode* node = 0, float speed = 0.5f, std::string mesh="", int life=5);
		Zombie(const Zombie& Z);
		Zombie& operator=(const Zombie& Z);
		~Zombie();

		void translate(Ogre::Real deltaT, Ogre::Vector3 vt=Ogre::Vector3::ZERO);



		void seekTarget();

		void turn(float deltaT);			// turno de juego
		void die(Ogre::Real deltaT);
		void vaporize(Ogre::Real deltaT);
		void stopAsyncParticles(int milliseconds);
		void startParticles();
		void attack();

	private:
		void stopParticles();
		void CheckState();
		void setDestiny();


		bool							_stop;
		Ogre::Vector3 					_vDestiny;
//		boost::asio::deadline_timer 	_timer;
//		boost::asio::io_service 		_io;
		Ogre::ParticleSystem* 			_particleSystem;
};

#endif
