#ifndef MACHINEGUN_H
#define MACHINEGUN_H

#include <Ogre.h>
#include "Projectile.h"
#include "Weapon.h"


class MachineGun : public Weapon
{
	public:
		MachineGun();
		MachineGun(const MachineGun& D);
		MachineGun& operator=(const MachineGun&);
		~MachineGun() {};

		void createProjectile(const int impulse);

		Ogre::SceneNode* getNodeProjectile();
		void setNodeProjectile(Ogre::SceneNode* nodeProjectile);
		void shoot(const int impulse);

		void setPositionNodeProjectile(Ogre::Vector3 vPos);

	private:
		Ogre::SceneNode* 	_scnNodeProjectile;
		Ogre::Vector3		_positionNodeProjectile;
};



#endif
