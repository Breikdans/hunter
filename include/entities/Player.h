#ifndef PLAYER_H
#define PLAYER_H

#include "Character.h"
#include "Weapon.h"
#include "MyUtils.h"

class Player : public Character
{
	public:
		Player(Ogre::SceneNode* node = 0, float speed = 0.5f, std::string mesh="", float speedPitch = 1, Weapon* weapon = 0);
		Player(const Player& P);
		Player& operator=(const Player& P);
		~Player();

		int getSpeedPitch() const;
		void setSpeedPitch(float speedPitch);

		void lookToTarget(Ogre::SceneManager *scn);
		//bool DetectCollision();

		void seekTarget() {};
		void die(Ogre::Real deltaT) {};

		virtual void turn(Ogre::Real deltaT) {};

		void translate(Ogre::Real deltaT, Ogre::Vector3 vt=Ogre::Vector3::ZERO);

		void changeWeapon(EN_CURRENT_STATE state, Ogre::Real deltaT);

		// Funciones para establecer en player Type, la posicion incial
		// del player, para usarla en la factoría
		// y las armas iniciales
		void setInitialPosition(Ogre::Vector3 pos);
		void setInitialOrientation(Ogre::Quaternion rot);
		Ogre::Vector3 getInitialPosition();
		Ogre::Quaternion getInitialOrientation();
		void addInitialWeapon(const EN_WEAPON_TYPE& weapon);
		std::vector<EN_WEAPON_TYPE> getInitialWeapons() const;

		Weapon* getInventaryWeapon(EN_WEAPON_TYPE weaponType);
		void addWeapon(Weapon* weapon);
		void setWeapon(Weapon* weapon);
		Weapon*	getWeapon() const;

	private:
		float 							_speedPitch;
		Weapon*							_weapon;
		std::vector<Weapon*>			_inventaryWeapons;
		
		Ogre::Vector3					_initialPosition;
		Ogre::Quaternion				_initialOrientation;
		std::vector<EN_WEAPON_TYPE>		_initialWeapons;
};

#endif
