#ifndef PROJECTILE_H
#define PROJECTILE_H

#include <Ogre.h>
#include <OgreBulletDynamicsRigidBody.h>

class Projectile
{
	public:
		Projectile(Ogre::SceneNode* sceneNode = 0, OgreBulletDynamics::RigidBody* rigidBody = 0);
		~Projectile();

		void setSceneNode(Ogre::SceneNode* sceneNode);
		Ogre::SceneNode* getSceneNode();

		void setRigidBody(OgreBulletDynamics::RigidBody* rigidBody);
		OgreBulletDynamics::RigidBody* getRigidBody();

	private:
		Ogre::SceneNode*				_sceneNode;
		OgreBulletDynamics::RigidBody*	_rigidBody;
};


#endif
