#ifndef WEAPON_H
#define WEAPON_H

#include <Ogre.h>

typedef enum
{
	 EN_NONE				= 0
	,EN_DEVASTATOR			= 1
	,EN_MACHINEGUN
	,EN_GUN
	,EN_SPECIAL_WEAPON
}EN_WEAPON_TYPE;

class Weapon
{
	public:
//		Weapon(int ammo = 0, Ogre::SceneNode* node = 0, std::string mesh="",EN_WEAPON_TYPE	weaponType=EN_DEVASTATOR);
		Weapon();
		Weapon(const Weapon& C);
		Weapon& operator= (const Weapon &C);
		virtual ~Weapon();

		void setPosition(Ogre::Vector3 pos);
		void setPosition(float x, float y,  float z);
		Ogre::Vector3 getPosition() const;

		Ogre::SceneNode* getNode();
		void setNode(Ogre::SceneNode*);

		std::string getMesh() const;
		void setMesh(std::string mesh);

		int getAmmo() const;
		void setAmmo(int ammo);

		void setForce(int force);
		int getForce() const;

		void setWeaponType(EN_WEAPON_TYPE type);
		EN_WEAPON_TYPE getWeaponType() const;

		void decAmmo( int dec = 1 );

		virtual void shoot(const int impulse = 0) {};

		Ogre::AnimationState* getShootAnimation();
		Ogre::AnimationState* getWalkAnimation();
		Ogre::AnimationState* getDownAnimation();

		void setShootAnimation(Ogre::AnimationState* amimation);
		void setWalkAnimation(Ogre::AnimationState* amimation);
		void setDownAnimation(Ogre::AnimationState* amimation);

	private:
		int 							_ammo;
		int								_force;
		Ogre::SceneNode*				_node;
		std::string 					_mesh;
		EN_WEAPON_TYPE					_weaponType;



		Ogre::AnimationState*	_downAnimation;
		Ogre::AnimationState*	_shootAnimation;
		Ogre::AnimationState*	_walkAnimation;

};

#endif
