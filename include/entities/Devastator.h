#ifndef DEVASTATOR_H
#define DEVASTATOR_H

#include <Ogre.h>
#include "Projectile.h"
#include "Weapon.h"


class Devastator : public Weapon
{
	public:
		Devastator();
		Devastator(const Devastator& D);
		Devastator& operator=(const Devastator&);
		~Devastator() {};

		void createProjectile(const int impulse);

		Ogre::SceneNode* getNodeProjectile();
		void setNodeProjectile(Ogre::SceneNode* nodeProjectile);
		void shoot(const int impulse);

		void setPositionNodeProjectile(Ogre::Vector3 vPos);

	private:
		Ogre::SceneNode* 	_scnNodeProjectile;
		Ogre::Vector3		_positionNodeProjectile;
};



#endif
