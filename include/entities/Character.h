#ifndef CHARACTER_H_
#define CHARACTER_H_


#include <Ogre.h>
#include <OIS/OIS.h>
#include "Grid.h"


#include <OgreBulletDynamicsRigidBody.h>
#include <Shapes/OgreBulletCollisionsStaticPlaneShape.h>
#include <Shapes/OgreBulletCollisionsBoxShape.h>

typedef enum
{
	EN_ALIVE	= 0,
	EN_WANDER,        	// Vagando
	EN_ATTACK,
	EN_RUNAWAY,
	EN_CHASE,			// perseguir
	EN_DEAD,
	EN_DEAD_VAPORIZE,
	EN_CHANGE_WEAPON_DEVASTATOR,
	EN_CHANGE_WEAPON_MACHINEGUN,
	EN_CLEAR			// para eliminarlo del vector en el framestarted

}EN_CURRENT_STATE;

class Character
{
	public:
		Character(Ogre::SceneNode* node = 0, float speed = 0.5f, std::string mesh="", int life=0);
		Character(const Character& C);
		Character& operator= (const Character &C);
		//~Character();
		virtual ~Character();

		void setPosition(Ogre::Vector3 pos);
		void setPosition(float x, float y,  float z);

		Ogre::Vector3 getPosition() const;

		Ogre::SceneNode* getNode();
		void setNode(Ogre::SceneNode*);

		void setSpeed(float s);
		float getSpeed() const;

		std::string getMesh() const;
		void setMesh(std::string mesh);

		static void setMove(bool M);
		static bool getMove(void);

		void setCellX(int x);
		void setCellY(int y);

		int getCellX(void) const;
		int getCellY(void) const;
		

		virtual void seekTarget() = 0;
		virtual void die(Ogre::Real deltaT) = 0;

		virtual void turn(Ogre::Real deltaT) = 0;


		void setRigidBody(OgreBulletDynamics::RigidBody* rigidBody);
		OgreBulletDynamics::RigidBody* getRigidBody() const;

		void CoordsToCellIndex();

		void setPath(const std::deque<Cell*>& path);
		std::deque<Cell*> getPath();

		int getLife() const;
		void setLife(int life);
		void decrementLife(int dec);

		int getOldTargetX() const;
		void setOldTargetX(int targetX);
		int getOldTargetY() const;
		void setOldTargetY(int targetY);

		void setTarget(Character* Ch);
		Character* getTarget() const;

		void setWalkingAnimation(Ogre::AnimationState* animation);
		Ogre::AnimationState* getWalkingAnimation();

		Ogre::AnimationState* getDeadAnimation();
		void setDeadAnimation(Ogre::AnimationState* animation);

		Ogre::AnimationState* getDeadVaporizeAnimation();
		void setDeadVaporizeAnimation(Ogre::AnimationState* animation);

		virtual void translate(Ogre::Real deltaT, Ogre::Vector3 vt=Ogre::Vector3::ZERO) = 0;

		void setState(EN_CURRENT_STATE state);
		EN_CURRENT_STATE getState() const;

	protected:
		Ogre::SceneNode*				_node;
		float 							_speed;
		std::string 					_mesh;
		int								_cellX;
		int								_cellY;
		
		std::deque<Cell*> 				_path;
		
		OgreBulletDynamics::RigidBody*	_rigidBody;

		static bool 					_move;

		int 							_life;
		int 							_oldTargetX;
		int 							_oldTargetY;
		Character*						_target;

		Ogre::AnimationState*			_walkingAnimation;
		Ogre::AnimationState* 			_deadAnimation;
		Ogre::AnimationState* 			_deadVaporize;

		EN_CURRENT_STATE				_currentState;

};

#endif /* CHARACTER_H_ */
