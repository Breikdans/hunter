#ifndef ELEMENT_H
#define ELEMENT_H

#include <Ogre.h>
#include <OIS/OIS.h>

class Element
{
	public:
		Element(std::string id = "", Ogre::SceneNode* node = 0, std::string mesh = "");

		void setPosition(Ogre::Vector3 pos);
		void setPosition(float x, float y,  float z);
		Ogre::Vector3 getPosition() const;

		void setId(std::string id);
		std::string getId() const;

		Ogre::SceneNode* getNode();
		void setNode(Ogre::SceneNode*);

		std::string getMesh() const;
		void setMesh(std::string mesh);

	protected:
		std::string			_id;
		Ogre::SceneNode*	_node;
		std::string 		_mesh;
};

#endif /* ELEMENT_H */
