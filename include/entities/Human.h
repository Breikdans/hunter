#ifndef HUMAN_H
#define HUMAN_H

#include <iostream>
#include <boost/asio.hpp>
#include <boost/bind.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>

#include "Character.h"

class Human : public Character
{
	public:
		Human(Ogre::SceneNode* node = 0, float speed = 0.5f, std::string mesh="", int life=5);
		Human(const Human& Z);
		Human& operator=(const Human& Z);
		~Human();

		void translate(Ogre::Real deltaT, Ogre::Vector3 vt=Ogre::Vector3::ZERO);

		void seekTarget();
		void setTarget(Character* Ch);
		Character* getTarget() const;

		void setState(EN_CURRENT_STATE state);
		EN_CURRENT_STATE getState() const;

		void turn(float deltaT);			// turno de juego
		void die(Ogre::Real deltaT);

	private:

		bool							_stop;
		Ogre::Vector3 					_vDestiny;
		Character*						_target;
};

#endif
