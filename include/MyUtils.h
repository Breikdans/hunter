#ifndef __MYUTILSH__
#define __MYUTILSH__

#include <OGRE/Ogre.h>
#include <OgreString.h>

#include "Shapes/OgreBulletCollisionsSphereShape.h"
#include "Shapes/OgreBulletCollisionsConvexHullShape.h"
#include "Shapes/OgreBulletCollisionsTrimeshShape.h"
#include "Utils/OgreBulletCollisionsMeshToShapeConverter.h"
#include <OgreBulletDynamicsRigidBody.h>
#include <Shapes/OgreBulletCollisionsStaticPlaneShape.h>
#include <Shapes/OgreBulletCollisionsBoxShape.h>
#include "OgreBulletCollisionsRay.h"

class MyUtils : public Ogre::Singleton<MyUtils>
{

	public:
		MyUtils () {};
		Ogre::Vector3 calculateTranslate( Ogre::Quaternion currentOrientation,  Ogre::Vector3 translation);
		Ogre::Quaternion multiply(Ogre::Quaternion q1, Ogre::Quaternion q2);
		int rangeRandomNumber (int min, int max);
		float rangeRandomNumber(float min, float max);
		// Heredados de Ogre::Singleton.
		static MyUtils& getSingleton ();
		static MyUtils* getSingletonPtr ();
};

#endif
