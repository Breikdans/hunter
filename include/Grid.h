#ifndef GRID_H
#define GRID_H

#include <iostream>
#include <stdexcept>
#include <cstdlib>
#include <String.h>
#include <vector>
#include <deque>
#include <set>
#include "control.h"

typedef enum
{
	EN_OPEN				= 0,
	EN_BLOCK,
	EN_ZOMBIE_CELL,
	EN_PLAYER_CELL
}EN_CELL_TYPE;

typedef enum{
	EN_FREE				= 0,
	EN_BUSY
}EN_CELL_TESTED;

class Cell
{
	public:
		Cell(float x = 0.0f, float y = 0.0f, float z = 0.0f, int type = 0, int indexX = 0, int indexY = 0) : _x(x), _y(y), _z(z), _type(type), _indexX(indexX), _indexY(indexY) {};
		int getType() const		{return _type;}
		void setType(int type)	{_type = type;}

		int getIndexX() const		{return _indexX;}
		void setIndexX(int indexX)	{_indexX = indexX;}
		int getIndexY() const		{return _indexY;}
		void setIndexY(int indexY)	{_indexY = indexY;}

		float getX() const		{return _x;}
		void setX(float x) 		{_x = x;}

		float getY() const		{return _y;}
		void setY(float y) 		{_y = y;}

		float getZ() const		{return _z;}
		void setZ(float z) 		{_z = z;}

	private:
		float 		_x;
		float		_y;
		float		_z;
		int			_type;
		int			_indexX;
		int			_indexY;
};

class CellSearch : public Cell
{
	public:
		CellSearch(CellSearch* cellParent = 0, int G = 0, int H = 0);
		CellSearch(const Cell& cell);						// ¡¡Ojo!! no es un constructor copia!!, es para iniciar el objeto con clase padre!!
		//bool operator<(const CellSearch & C) const;			// necesario para ordenacion interna del priority_queue
		
		void setG(int G)						{ _G = G; }
		void setH(int xDest, int yDest);
		void setParent(CellSearch* cellParent)	{ _cellParent = cellParent; }
		
		int getF() const						{ return _G + _H; }
		int getG() const						{ return _G; }
		int getH() const						{ return _H; }
		CellSearch* getParent() const			{ return _cellParent; }
		
	private:
		CellSearch* 		_cellParent;

		int 		_G;				// puntuacion desde celda de inicio hasta esta celda
		int			_H;				// puntuacion heuristica desde esta celda a la meta
};

//template<class T>
struct PointerComparator {
    bool operator()(const CellSearch *a, const CellSearch *b) const {
        return a->getF() < b->getF();
    }
};


class Grid
{
	public:
		Grid(int cols = 0, int rows = 0, Cell* tblGrid = 0);
		Grid(const Grid& G);
		Grid& operator=(const Grid & G);
		Cell& operator() (int X, int Y);				// para leer/escribir en posicion del grid
		Cell& operator() (int X);
		~Grid();

		void InitGrid(int rows, int cols);

		std::deque<Cell*> calculateAStarPath(const Cell& start, const Cell& end);

		void DebugGrid();
		void DebugGridCoordinate();

		void DebugCalculatedPath(const std::deque<Cell*>& path);
		void DebugOpenList(std::deque<CellSearch*>& openList);//, char *openListMap);
		void DebugClosedListMap(char *closedListMap);
		void DebugCellSearch(const CellSearch& cellSearch);
		void DebugCell(const Cell& cell);


	private:
		void Adjacents( CellSearch* actual, CellSearch* end, std::deque<CellSearch*>& openList, char *openListMap, char *closedListMap, std::vector<CellSearch*>& almacen);
		void addOpenList(CellSearch* actual, std::deque<CellSearch*>& openList, char *openListMap, std::vector<CellSearch*>& almacen);
		CellSearch* popTopOpenList(std::deque<CellSearch*>& openList, char *openListMap);
		
		std::deque<CellSearch*>::iterator getCellSearchOpenList(std::deque<CellSearch*>& openList, int X, int Y);
		void modifyCellSearchOpenList(std::deque<CellSearch*>::iterator it, std::deque<CellSearch*>& openList);
										
		void ConstructPath(std::deque<Cell*>&path, CellSearch* end);

		int									_cols;
		int									_rows;
		Cell* 								_tblGrid;
};

#endif
