#ifndef SCENE_H_
#define SCENE_H_





#include "Grid.h"
#include "Generator.h"

#include <OgreBulletDynamicsRigidBody.h>
#include <Shapes/OgreBulletCollisionsStaticPlaneShape.h>
#include <Shapes/OgreBulletCollisionsBoxShape.h>
#include <TimersManager.h>


class Scene
{
	public:
		Scene();
		Scene(const Scene&);
		Scene& operator=(const Scene&);
		~Scene();

		void setLevel(int level);
		int getLevel() const;

		void setDimensionX(int X);
		int getDimensionX() const;

		void setDimensionY(int Y);
		int getDimensionY() const;

		void setDimensionEdge(float dim);
		float getDimensionEdge() const;

		PathFindingEnemyTimer* getPathFindingEnemyTimer();
		PathFindingHumanTimer* getPathFindingHumanTimer();

		void initGrid();

		void addZombieGenerator(Generator* zombieGenerator);
		void addHumanGenerator(Generator* humanGenerator);

		std::vector<Generator*> getZombieGenerators() const 	{ return _zombieGenerators; }
		std::vector<Generator*> getHumanGenerators() const 	{ return _humanGenerators; }


		Cell& grid(int F, int C);
		Cell& grid(int F);
		//Cell grid(int F, int C) const;
		
		std::deque<Cell*> calculateAStarPath(const Cell& start, const Cell& end);

		void DebugGrid();
		void DebugCalculatedPath(const std::deque<Cell*>&path);
		void DebugCell(const Cell& cell);

	private:
		int 					_level;
		int						_dimensionX;
		int						_dimensionY;
		float					_dimensionEdge;

		std::vector<Generator*>	_zombieGenerators;
		std::vector<Generator*>	_humanGenerators;

		Grid 					_grid;

		PathFindingEnemyTimer*	_pathFindingEnemyTimer;
		PathFindingHumanTimer*	_pathFindingHumanTimer;
};

#endif /* SCENE_H_ */
