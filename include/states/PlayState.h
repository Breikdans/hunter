#ifndef PLAYSTATE_H
#define PLAYSTATE_H

//#include <iostream>
//#include <cstdio>

#include <Ogre.h>
#include <OIS/OIS.h>
#include <OgreOverlaySystem.h>
#include <OgreOverlayElement.h>
#include <OgreOverlayManager.h>

#include <CEGUI.h>
//#include <boost/asio.hpp>
//#include <boost/bind.hpp>
//#include <boost/date_time/posix_time/posix_time.hpp>

#include "GameState.h"
#include "IntroState.h"
#include "MenuEscState.h"
#include "PauseState.h"
#include "DeathState.h"
#include "EndLevelState.h"
#include "Scene.h"

#include "EntityManager.h"
#include "PhysicsManager.h"
#include "TimersManager.h"
#include "CharacterFactory.h"

class PlayState : public Ogre::Singleton<PlayState>, public GameState
{
	public:
		PlayState ();
		void enter ();
		void exit ();
		void pause ();
		void resume ();

		void keyPressed (const OIS::KeyEvent &e);
		void keyReleased (const OIS::KeyEvent &e);

		void mouseMoved (const OIS::MouseEvent &e);
		void mousePressed (const OIS::MouseEvent &e, OIS::MouseButtonID id);
		void mouseReleased (const OIS::MouseEvent &e, OIS::MouseButtonID id);

		bool frameStarted (const Ogre::FrameEvent& evt);
		bool frameEnded (const Ogre::FrameEvent& evt);

		// Heredados de Ogre::Singleton.
		static PlayState& getSingleton ();
		static PlayState* getSingletonPtr ();

		void rotatePlayer(float xAngle, float yAngle);

		void addScene(Scene* scn);

		int getLives() const;
		void setLives(int number);

		void showForce(int force);

		bool isGameOver() const;
		void decrementLive(int number);
		int getPointsPlayer() const;
		void setExitGame(bool status);

		Ogre::Camera* getMainCamera();
		void attachWeaponToCameraPitch();
		void DettachWeaponToCameraPitch();

		void saveVolumeInMuteOn(int vol);
		int getVolumeInMuteOff(void) const;

		void setMute(bool mute);
		bool getMute() const;

		void setStartLevel(bool startLevel);
		bool getStartLevel() const;
		
		Ogre::Real getDeltaTime() const;
		Scene* getScene();
		CEGUI::Window* getLevelCEGUI();

		void destroyProjectile(std::string projectileName);
		void killZombie(std::string zombieDead);
		void killHuman(std::string humanDead);
		void vaporizeZombie(std::string zombieDead);

		void destroyAll();
	protected:
		Ogre::Root* 			_root;
		Ogre::SceneManager* 	_sceneMgr;
		Ogre::RenderWindow* 	_renderWindow;
		Ogre::Viewport* 		_viewport;
		Ogre::Viewport* 		_viewportMiniMap;
		Ogre::Camera* 			_mainCamera;
		Ogre::Camera* 			_miniMapCamera;
		Ogre::OverlayManager* 	_overlayManager;
		//Ogre::Light* 			_light;
		Ogre::Real 				_deltaT;

		bool 					_mouseLeftPressed;

		Player*					_player;

		void fadeOutLevelNumber();
		void showCEGUILevelNumber();
		void initGame();
		void locateMainCamera();
		void locateDirectionalLightMinimapCamera();
		void locateDirectionalLight(Ogre::Vector3 lightDirection);

		void createScene();

		void createInGameCEGUI();
		void createMiniMap();
		void addMissionsCEGUI();
		void updateMiniMap();

		void createHudOverlay();
		void hideHudOverlay();
		void updateInfoHudOverlay();

		void createEnemies();
		void createHumans();

		CEGUI::MouseButton convertMouseButton(OIS::MouseButtonID id);
		void locateOverlayMousePointer(int x, int y);
		void locateCeguiMousePointer(int x, int y);

		void createProjectile(Projectile *projectile);
		void setPhysicsProjectile(Projectile *projectile, int impulsePercent);

		void updateProjectileCounter();

		void processCollisions();




		Ogre::SceneNode* 		_containerCamera;
		Ogre::SceneNode* 		_cameraPitchNode;


		Scene*					_scene;

		int						_lives;
		int 					_points;
		bool 					_gameOver;
		int						_volumeSavedOnMute;
		bool					_mute;

		CEGUI::Window* 			_levelCEGUI;

		bool 					_startLevel;
		bool 					_exitGame;

		MessageInitLevelTimer*	_messageInitLevelTimer;
		CEGUI::Window* 			_ceguiSheet;
		CEGUI::Window* 			_inGameCEGUI;
		CEGUI::Window* 			_RTTWindow;		// Donde se renderizara la textura del minimapa

		clock_t 				_impulseTime;
		clock_t 				_charsCreateTime;
		bool 					_fadedLevelNumber;
};

#endif
