#ifndef ENDLEVELSTATE_H
#define ENDLEVELSTATE_H

#include <Ogre.h>
#include <OIS/OIS.h>

#include "GameState.h"
#include "LoadLevelState.h"

class EndLevelState : public Ogre::Singleton<EndLevelState>, public GameState
{
	public:
		EndLevelState() {}

		void enter ();
		void exit ();
		void pause ();
		void resume ();

		void keyPressed (const OIS::KeyEvent &e);
		void keyReleased (const OIS::KeyEvent &e);

		void mouseMoved (const OIS::MouseEvent &e);
		void mousePressed (const OIS::MouseEvent &e, OIS::MouseButtonID id);
		void mouseReleased (const OIS::MouseEvent &e, OIS::MouseButtonID id);

		bool frameStarted (const Ogre::FrameEvent& evt);
		bool frameEnded (const Ogre::FrameEvent& evt);

		// Heredados de Ogre::Singleton.
		static EndLevelState& getSingleton ();
		static EndLevelState* getSingletonPtr ();

	protected:
		void locateOverlayMousePointer(int x,int y);
		void createOverlay();
		void hideOverlay();

		Ogre::OverlayManager* _overlayManager;
};

#endif
