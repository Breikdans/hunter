#ifndef NEWRECORDSTATE_H
#define NEWRECORDSTATE_H

#include <Ogre.h>
#include <OIS/OIS.h>
#include <CEGUI.h>

#include "GameState.h"
#include "RecordsState.h"

class NewRecordState : public Ogre::Singleton<NewRecordState>, public GameState
{
	public:
		NewRecordState() {}

		void enter ();
		void exit ();
		void pause ();
		void resume ();

		void keyPressed (const OIS::KeyEvent &e);
		void keyReleased (const OIS::KeyEvent &e);

		void mouseMoved (const OIS::MouseEvent &e);
		void mousePressed (const OIS::MouseEvent &e, OIS::MouseButtonID id);
		void mouseReleased (const OIS::MouseEvent &e, OIS::MouseButtonID id);

		bool frameStarted (const Ogre::FrameEvent& evt);
		bool frameEnded (const Ogre::FrameEvent& evt);

		// Heredados de Ogre::Singleton.
		static NewRecordState& getSingleton ();
		static NewRecordState* getSingletonPtr ();

		void locateCeguiMousePointer(int x, int y);
		void locateOverlayMousePointer(int x,int y);

	protected:
		CEGUI::MouseButton convertMouseButton(OIS::MouseButtonID id);
		void showEnterRecordNameCegui();
		bool BotonAceptar(const CEGUI::EventArgs &e);
		void saveRecords();

		CEGUI::Window* _newRecordWin;
		Ogre::OverlayManager* 	_overlayManager;
};

#endif
