#ifndef LoadLevelState_H
#define LoadLevelState_H

#include <Ogre.h>
#include <OIS/OIS.h>
#include <iterator>
#include <string>
#include <map>
#include "IntroState.h"
#include "GameState.h"
//#include "PlayState.h"
#include "Importer.h"
#include "Scene.h"

#define PATH_LEVEL		"./media/levels/level"
#define XML_LEVEL		"output.xml"

class LoadLevelState : public Ogre::Singleton<LoadLevelState>, public GameState
{
	public:
		LoadLevelState();

		void enter ();
		void exit ();
		void pause ();
		void resume ();

		void keyPressed (const OIS::KeyEvent &e);
		void keyReleased (const OIS::KeyEvent &e);

		void mouseMoved (const OIS::MouseEvent &e);
		void mousePressed (const OIS::MouseEvent &e, OIS::MouseButtonID id);
		void mouseReleased (const OIS::MouseEvent &e, OIS::MouseButtonID id);

		void locateOverlayMousePointer(const int, const int);

		bool frameStarted (const Ogre::FrameEvent& evt);
		bool frameEnded (const Ogre::FrameEvent& evt);

		// Heredados de Ogre::Singleton.
		static LoadLevelState& getSingleton ();
		static LoadLevelState* getSingletonPtr ();

		void loadLevel(int currentLevel = 0);
		void buildNodesScene();

		void setCurrentLevel(int number);
		int  getCurrentLevel() const;

		void createOverlay();

	protected:
		Ogre::Root* 			_root;
		Ogre::SceneManager* 	_sceneMgr;
		Ogre::RenderWindow* 	_renderWindow;
		Ogre::Viewport* 		_viewport;
		Ogre::Camera*			_mainCamera;
		Ogre::OverlayManager* 	_overlayManager;

		int 					_currentLevel;
		bool 					_exitGame;
};

#endif
