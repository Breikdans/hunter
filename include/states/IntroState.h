#ifndef INTROSTATE_H
#define INTROSTATE_H

#include <Ogre.h>
#include <OIS/OIS.h>
#include <iterator>
#include <map>
#include <SDL2/SDL.h>
#include <SDL2/SDL_mixer.h>

#include "TrackManager.h"
#include "SoundFXManager.h"

#include "GameState.h"
#include "MenuState.h"
#include "Fader.h"

typedef struct
{
	unsigned int iPuntos;
	std::string sJugador;
}STR_Record;

#define MAX_PLAYER_RECORDS		10

class IntroStateFaderCallback : public FaderCallback
{
	public:
		void fadeInCallback(void);
		void fadeOutCallback(void);
};

class IntroState : public Ogre::Singleton<IntroState>, public GameState
{
	public:
		IntroState() {}
		~IntroState() {delete _fader;}

		void enter ();
		void exit ();
		void pause ();
		void resume ();

		void keyPressed (const OIS::KeyEvent &e);
		void keyReleased (const OIS::KeyEvent &e);

		void mouseMoved (const OIS::MouseEvent &e);
		void mousePressed (const OIS::MouseEvent &e, OIS::MouseButtonID id);
		void mouseReleased (const OIS::MouseEvent &e, OIS::MouseButtonID id);

		bool frameStarted (const Ogre::FrameEvent& evt);
		bool frameEnded (const Ogre::FrameEvent& evt);

		Fader* getFader()						{ return _fader; }
		CEGUI::OgreRenderer* getRenderer()		{ return _renderer; }

		std::multimap<unsigned int, std::string> gameRecords;

		// Heredados de Ogre::Singleton.
		static IntroState& getSingleton ();
		static IntroState* getSingletonPtr ();

		TrackPtr 	getMenuTrackPtr () 			{ return _mainMenuTrack; }
		TrackPtr 	getMainThemeTrackPtr () 	{ return _gameThemeLoop; }

		SoundFXPtr 	getShotFXPtr () 			{ return _shotEffect; }
		SoundFXPtr	getPowerShotFXPtr()			{ return _powerShotEffect; }
		SoundFXPtr	getNoAmmoFXPtr()			{ return _noAmmoEffect; }
		SoundFXPtr	getZombie1FXPtr()			{ return _zombie1Effect; }
		SoundFXPtr	getMachineGunShotFXPtr()	{ return _machineGunShotEffect; }

	protected:
		Ogre::Root* 				_root;
		Ogre::SceneManager* 		_sceneMgr;
		Ogre::Viewport* 			_viewport;
		Ogre::Camera* 				_mainCamera;
		Ogre::OverlayManager* 		_overlayManager;
		CEGUI::OgreRenderer* 		_renderer;
		
		Fader*						_fader;
		IntroStateFaderCallback		_faderCBK;

		// Manejadores del sonido.
		TrackManager* 				_TrackManager;
		SoundFXManager* 			_SoundFXManager;

		TrackPtr 					_mainMenuTrack;			// puntero inteligente
		TrackPtr 					_gameThemeLoop;			// puntero inteligente

		SoundFXPtr 					_shotEffect;			// puntero inteligente
		SoundFXPtr 					_powerShotEffect;		// puntero inteligente
		SoundFXPtr 					_noAmmoEffect;			// puntero inteligente
		SoundFXPtr 					_zombie1Effect;			// puntero inteligente
		SoundFXPtr					_machineGunShotEffect;	// puntero inteligente

		void createIntroOverlay();
		void createOverlayMousePointer();
		void locateOverlayMousePointer(const int, const int);
		void loadResources();
		void createCegui();
		void initSDL();

		void loadRecordsFile();
		void fillRecordsFile();
		void showRecordsFile();

		bool _exitGame;
};

#endif
