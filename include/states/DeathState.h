#ifndef DEATHSTATE_H
#define DEATHSTATE_H

#include <Ogre.h>
#include <OIS/OIS.h>

#include "GameState.h"

#include "PlayState.h"
#include "EndGameState.h"
#include "Fader.h"

class DeathStateFaderCallback : public FaderCallback
{
	public:
		void fadeInCallback(void);
		void fadeOutCallback(void);
};

class DeathState : public Ogre::Singleton<DeathState>, public GameState
{
	public:
		DeathState() {}

		void enter ();
		void exit ();
		void pause ();
		void resume ();

		void keyPressed (const OIS::KeyEvent &e);
		void keyReleased (const OIS::KeyEvent &e);

		void mouseMoved (const OIS::MouseEvent &e);
		void mousePressed (const OIS::MouseEvent &e, OIS::MouseButtonID id);
		void mouseReleased (const OIS::MouseEvent &e, OIS::MouseButtonID id);

		bool frameStarted (const Ogre::FrameEvent& evt);
		bool frameEnded (const Ogre::FrameEvent& evt);

		// Heredados de Ogre::Singleton.
		static DeathState& getSingleton ();
		static DeathState* getSingletonPtr ();

		Fader* getFader()						{ return _fader; }

	protected:
		void locateOverlayMousePointer(int x,int y);
		void createOverlay();
		void hideOverlay();

		Fader*						_fader;
		DeathStateFaderCallback		_faderCBK;
		Ogre::OverlayManager* 		_overlayManager;
};

#endif
