#ifndef MENUESCSTATE_H
#define MENUESCSTATE_H

#include <Ogre.h>
#include <OIS/OIS.h>
#include <CEGUI.h>

#include "GameState.h"
#include "PlayState.h"
#include "MenuState.h"

class MenuEscState : public Ogre::Singleton<MenuEscState>, public GameState
{
	public:
		MenuEscState() {}
		void enter ();
		void exit ();
		void pause ();
		void resume ();

		void keyPressed (const OIS::KeyEvent &e);
		void keyReleased (const OIS::KeyEvent &e);

		void mouseMoved (const OIS::MouseEvent &e);
		void mousePressed (const OIS::MouseEvent &e, OIS::MouseButtonID id);
		void mouseReleased (const OIS::MouseEvent &e, OIS::MouseButtonID id);

		bool frameStarted (const Ogre::FrameEvent& evt);
		bool frameEnded (const Ogre::FrameEvent& evt);

		// Heredados de Ogre::Singleton.
		static MenuEscState& getSingleton ();
		static MenuEscState* getSingletonPtr ();

	protected:
		CEGUI::MouseButton convertMouseButton(OIS::MouseButtonID id);
		void showMenuEscMsgCegui();

		bool sliderMusic(const CEGUI::EventArgs &e);
		bool sliderMouse(const CEGUI::EventArgs &e);
		bool checkMute(const CEGUI::EventArgs &e);
		bool buttonResume(const CEGUI::EventArgs &e);
		bool buttonQuit(const CEGUI::EventArgs &e);

		int percentageFromValue(int value, int maxValue);
		int valueFromPercentage(int percent, int maxValue);

		void locateOverlayMousePointer(int x, int y);
		void locateCeguiMousePointer(int x, int y);

		CEGUI::Window* 			_MenuEscMsg;
		Ogre::OverlayManager* 	_overlayManager;
};

#endif
