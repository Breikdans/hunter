#ifndef CONTROL_H_
#define CONTROL_H_

using namespace std;

// ----- DEFINES DE DEPURACION -----
//#define DEBUG_TRACE_CONSOLE			// Comentar para que no escriba trazas por consola!!!
//#define DEBUG_SIN_SONIDO
//#define DEBUG_ESTADOS
//#define DEBUG_PARSER
//#define DEBUG_MODE_MOUSE
//#define DEBUG_A_STAR
//#define DEBUG_PHYSICS
// ----- FIN -----


#ifdef DEBUG_TRACE_CONSOLE
	#define DB_TRACE(x) x
#else
	#define DB_TRACE(x)
#endif

#ifdef DEBUG_SIN_SONIDO
	#define DB_SIN_SONIDO(x)
#else
	#define DB_SIN_SONIDO(x) x
#endif

#ifdef DEBUG_ESTADOS
	#define DB_ESTADOS(x) x
#else
	#define DB_ESTADOS(x)
#endif


#ifdef DEBUG_PARSER
	#define DB_PARSER(x) x
#else
	#define DB_PARSER(x)
#endif

#ifdef DEBUG_A_STAR
	#define DB_ASTAR(x) x
#else
	#define DB_ASTAR(x)
#endif

#ifdef DEBUG_PHYSICS
	#define DB_PHYSICS(x) x
#else
	#define DB_PHYSICS(x)
#endif

#endif /* CONTROL_H_ */
