#ifndef IMPORTER_H_
#define IMPORTER_H_

#include <xercesc/parsers/XercesDOMParser.hpp>
#include <xercesc/dom/DOM.hpp>
#include <xercesc/util/XMLString.hpp>
#include <xercesc/util/PlatformUtils.hpp>
#include <OGRE/Ogre.h>
#include <OgreVector3.h>
#include <OgreVector4.h>
#include <iostream>
#include <string>
#include "control.h"
#include "Scene.h"
#include "Generator.h"
#include "PlayState.h"
#include "PhysicsManager.h"
#include "EntityManager.h"
#include "MissionsManager.h"
#include "WeaponFactory.h"

typedef unsigned int uint32;

using namespace std;
using namespace xercesc;

class Importer : public Ogre::Singleton<Importer>
{
	public:
		// Unique public member function to parse an XML.
		void parseScene (const char * path, Scene *scn);
		string getAttribute(const DOMNode* node, const char* attr);

		// Inheritance from Ogre::Singleton.
		static Importer& getSingleton ();
		static Importer* getSingletonPtr ();

	private:
		void parseGrid(DOMNode* node, Scene* scn);
		void parsePlayer(DOMNode* node, Scene* scn);
		void getPlayerWeapons(DOMNode* node, Player& player);
		EN_WEAPON_TYPE getWeaponType(std::string type);
		void parseZombie(DOMNode* node);
		void parseHuman(DOMNode* node);

		void parseZombieGenerator(DOMNode* node, Scene* scn);
		void parseHumanGenerator(DOMNode* node, Scene* scn);

		void parseElement(DOMNode* node, Scene* scn);
		void parseCell(DOMNode* node, Scene* scn);
		void parseMissions(DOMNode* node);
		void parseGoal(DOMNode* node);
		void parseWeapon(DOMNode* node);
		void orderGrid(Scene *scn);
		void showCenterCells(Scene *scn);

		float getValueFromTag(DOMNode* node, const char* tag);
		std::string getStringFromTag(DOMNode* node, const char* tag);
		bool isNodeNamed(DOMNode* node,const char* name);


};
#endif /* IMPORTER_H_ */
