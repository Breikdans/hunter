#include <Ogre.h>
#include "EntityManager.h"
#include "PhysicsManager.h"

#ifndef WEAPONFACTORY_H_
#define WEAPONFACTORY_H_

// Define el creador principal
class WeaponFactory
{
public:
	virtual Weapon* createWeapon(const Ogre::Vector3 position) = 0;
	virtual ~WeaponFactory() {};
};


// Define el creador concreto
class DevastatorFactory : public WeaponFactory
{
public:
	Weapon* createWeapon(const Ogre::Vector3 position = Ogre::Vector3::ZERO );
};

// Define el creador concreto
class MachineGunFactory : public WeaponFactory
{
public:
	Weapon* createWeapon(const Ogre::Vector3 position = Ogre::Vector3::ZERO );
};

#endif /* WEAPONFACTORY_H_ */
