#include <Ogre.h>
#include "EntityManager.h"
#include "PhysicsManager.h"
#include "WeaponFactory.h"

#ifndef CHARACTERFACTORY_H_
#define CHARACTERFACTORY_H_

const int CHARACTER_ZOMBIE 		= 0;
const int CHARACTER_PLAYER		= 1;



// Define el creador principal
class CharacterFactory
{
public:
	virtual ~CharacterFactory();
	virtual Character* createCharacter(const Ogre::Vector3 position) = 0;
};



// Define el creador concreto
class ZombieFactory : public CharacterFactory
{
public:
	Character* createCharacter(const Ogre::Vector3 position);

};

class HumanFactory : public CharacterFactory
{
public:
	Character* createCharacter(const Ogre::Vector3 position);
};

// Define el creador concreto
class PlayerFactory : public CharacterFactory
{
public:
	Character* createCharacter(const Ogre::Vector3 position);
};

#endif /* CHARACTERFACTORY_H_ */
