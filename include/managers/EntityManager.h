#ifndef ENTITY_H
#define ENTITY_H


#include <Ogre.h>
#include "Element.h"
#include "Zombie.h"
#include "Human.h"
#include "Projectile.h"
#include "Player.h"
#include "Weapon.h"
#include "Devastator.h"
#include "MachineGun.h"

const int NOT_SHOWN_IN_MINIMAP = 0x01;

class EntityManager : public Ogre::Singleton<EntityManager>
{
public:
	EntityManager();
	~EntityManager();

	Ogre::Entity* createEntity(const std::string name, const std::string mesh);
	Ogre::SceneNode* createSceneNode(std::string name);

	void setZombieType(const Zombie& zombieType);
	const Zombie& getZombieType() const;

	int getInstanceNumber();

	void setHumanType(const Human& humanType);
	const Human& getHumanType() const;

	void addWeaponType(const Weapon& weaponType);
	Weapon* getWeaponType(EN_WEAPON_TYPE type) const;

	void setPlayerType(const Player& playerType);
	Player* getPlayerType() const;

	Ogre::SceneNode* getBaseNode();
	void setBaseNode(Ogre::SceneNode* baseNode);

	void addZombie(Character* zombie);
	void addHuman(Character* survivor);
	void addWeapon(Weapon* weapon);
	void addProjectile(Projectile* projectile);
	void addElement(Element* element);
	
	std::vector<Character*>& getZombies();
	std::vector<Character*>& getHumans();
	std::vector<Weapon*> getWeapons() const;
	std::deque<Projectile*> getProjectiles() const;
	std::vector<Element*> getElements() const;

	void removeProjectile(std::string projectileName);
	int getProjectileTotal() const;
	
	std::vector<Character*>::iterator getZombieIterator(std::string zombieDead);
	std::vector<Character*>::iterator getHumanIterator(std::string humanDead);
	int getZombiesAlive() const;
	int getHumansAlive() const;

	void setPlayer(Player* player);
	Player* getPlayer(void);

	void destroyAll();

private:
	void destroyAllZombies();
	void destroyAllHumans();
	void destroyAllWeaponsType();
	void destroyAllWeapons();
	void destroyAllProjectiles();
	void destroyAllElements();

	// ==== Objetos-Modelo de los cuales, copiar al crear un objeto nuevo.
	Zombie*					_zombieType;
	Human*					_humanType;
	std::vector<Weapon*>	_weaponsType;
	Player*					_playerType;

	// ==== Entidades y Personajes que formaran parte de la escena de juego
	std::vector<Character*>	_zombies;
	std::vector<Character*>	_humans;
	std::vector<Weapon*>	_weapons;
	std::deque<Projectile*>	_projectiles;
	std::vector<Element*>	_elements;

	Ogre::SceneNode*		_baseNode;

	Player*					_player;

	int _instanceNumber;
};

#endif

