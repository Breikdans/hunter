#ifndef MISSIONSMANAGER_H
#define MISSIONSMANAGER_H

#include <Ogre.h>
#include <vector>

class Mission;

typedef enum
{
	 EN_KILL_ENEMIES			= 1
	,EN_RESCUE_SURVIVORS
	,EN_GATHER_SPECIAL_WEAPON
	,EN_KILL_BOSS
}EN_MISSION_TYPE;

class MissionsManager : public Ogre::Singleton<MissionsManager>
{
	public:
		MissionsManager() {}

		// Heredados de Ogre::Singleton.
		static MissionsManager& getSingleton ();
		static MissionsManager* getSingletonPtr ();

		void addMission(Mission *mission);
		std::vector<Mission *> getMissions() const;

		void destroyAll();
		void destroyAllMissions();


	private:
		std::vector<Mission *> _missions;

};

class Mission
{
	public:
		Mission(std::string goal = "", bool complete = false, int totalUnits = 0,
				int actualUnits = 0, EN_MISSION_TYPE missionType = EN_KILL_ENEMIES) : _goal(goal),
																					  _complete(complete),
																					  _totalUnits(totalUnits),
																					  _actualUnits(actualUnits),
																					  _missionType(missionType) {}

		void setGoal(std::string goal) 		{ _goal = goal; }
		void setComplete(bool complete)		{ _complete = complete; }
		void setTotalUnits(int totalUnits)	{ _totalUnits = totalUnits; }
		void setActualUnits(int actualUnits){ _actualUnits = actualUnits; }

		std::string	getGoal() 			const { return _goal; }
		bool		getComplete()		const { return _complete; }
		int 		getTotalUnits()		const { return _totalUnits; }
		int			getActualUnits()	const { return _actualUnits; }

	private:
		std::string 		_goal;
		bool				_complete;
		int					_totalUnits;
		int					_actualUnits;
		EN_MISSION_TYPE 	_missionType;
};

#endif
