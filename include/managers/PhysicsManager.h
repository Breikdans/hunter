#ifndef PHYSICS_H
#define PHYSICS_H

#include <Ogre.h>

#include "control.h"
#include "Shapes/OgreBulletCollisionsSphereShape.h"
#include "Shapes/OgreBulletCollisionsConvexHullShape.h"
#include "Shapes/OgreBulletCollisionsTrimeshShape.h"
#include "Utils/OgreBulletCollisionsMeshToShapeConverter.h"
#include <OgreBulletDynamicsRigidBody.h>
#include <Shapes/OgreBulletCollisionsStaticPlaneShape.h>
#include <Shapes/OgreBulletCollisionsBoxShape.h>
#include "OgreBulletCollisionsRay.h"
#include <bullet/btBulletDynamicsCommon.h>
#ifdef DEBUG_PHYSICS
	#include "EntityManager.h"
#endif

#include "control.h"

const int COL_NONE 		= 0;
const int COL_FLOOR		= 1;
const int COL_ZOMBIE	= 2;
const int COL_PLAYER	= 4;
const int COL_OBJECT	= 8;
const int COL_STATIC	= 16;
const int COL_PROJECTILE= 32;
const int COL_SURVIVOR	= 64;
const int COL_ITEM		= 128;
const int COL_ALL		= COL_FLOOR | COL_ZOMBIE | COL_PLAYER | COL_OBJECT | COL_STATIC | COL_PROJECTILE | COL_SURVIVOR | COL_ITEM;

const int COLTYPE_NO_COLLISION 		= 0;
const int COLTYPE_ZOMBIE_PLAYER 	= 1;
const int COLTYPE_PLAYER_ZOMBIE 	= 2;
const int COLTYPE_ZOMBIE_PROJECTILE = 3;
const int COLTYPE_PROJECTILE_ZOMBIE = 4;
const int COLTYPE_PROJECTILE_HUMAN 	= 5;
const int COLTYPE_HUMAN_PROJECTILE 	= 6;
const int COLTYPE_ZOMBIE_HUMAN		= 7;
const int COLTYPE_HUMAN_ZOMBIE		= 8;


class PhysicsManager : public Ogre::Singleton<PhysicsManager>
{
	public:
		PhysicsManager();
		~PhysicsManager();
		int DetectCollision();
		void CreateWorld();
		void buildOgreBulletRayFromCamera(Ogre::Vector3 &v,
				Ogre::Vector3 &vEnd,
				OgreBulletDynamics::DynamicsWorld* world,
				 float xOrign, float yOrigin,
				Ogre::Camera *camera);

		 void buildOgreBulletRayFromCamera(	std::string &nameObject,		// salida del objeto colisionado
											OgreBulletDynamics::DynamicsWorld* world,
											float xOrign,								///< ENTRADA. Posicion en X de partida del rayo (valores de 0 a 1)
											float yOrigin,								///< ENTRADA. Posicion en Y de partida del rayo (valores de 0 a 1)
											Ogre::Camera *camera );



		OgreBulletDynamics::RigidBody* createRigidBody(Ogre::Entity* entity,
													  std::string type,
													  const Ogre::Vector3&	initialPosition,
													  const Ogre::Quaternion& initialOrientation);


		OgreBulletCollisions::BoxCollisionShape* getBoxCollisionShape(Ogre::Entity* entity, std::string type);
		OgreBulletCollisions::TriangleMeshCollisionShape* getStaticMeshShape(Ogre::Entity *entity, std::string type);
		OgreBulletCollisions::ConvexHullCollisionShape *getConvexHullShape(Ogre::Entity *entity, std::string type);
		OgreBulletCollisions::SphereCollisionShape *getSphereShape(Ogre::Entity *entity, std::string type);

		void addCollisionShape(std::string key, OgreBulletCollisions::CollisionShape* collisionShape);
		OgreBulletCollisions::CollisionShape* getCollisionShape(std::string key);

		OgreBulletCollisions::DebugDrawer* getDebugDrawer();
		void setDebugDrawer(OgreBulletCollisions::DebugDrawer* debugDrawer);

		OgreBulletDynamics::DynamicsWorld* getWorld();
		void setWorld(OgreBulletDynamics::DynamicsWorld* world);

		Ogre::Entity* getLastObjACollision();
		Ogre::Entity* getLastObjBCollision();

		bool isPairOf(const std::string txtObjA, const std::string txtObjB);
		bool isEnoughProjectileForce(const btCollisionObject* btObj);

		void destroyAll();
		void destroyAllShapes();

	private:
		std::map<std::string, OgreBulletCollisions::CollisionShape *>	_shapes;

		OgreBulletDynamics::DynamicsWorld * _world;
		OgreBulletCollisions::DebugDrawer * _debugDrawer;

		Ogre::Entity*	_lastObjACollision;
		Ogre::Entity*	_lastObjBCollision;


};
#endif
