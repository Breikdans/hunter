

#include <IceUtil/Thread.h>
#ifndef TIMERSMANAGER_H
#define TIMERSMANAGER_H

#include <IceUtil/Mutex.h>
#include <string.h>


class PathFindingEnemyTimer : public IceUtil::Thread
{
	private:
	 int _runningCustom;

	public:
	 	PathFindingEnemyTimer();
		~PathFindingEnemyTimer();
		void setSecondsLeft(int seconds);

		void stopTimer();
		void runTimer();
		void execTimer();
		void setZombieTargets();

		virtual void run ();
};

class PathFindingHumanTimer : public IceUtil::Thread
{
	private:
	 int _oldPlayerX;
	 int _oldPlayerY;
	 int _runningCustom;

	public:
	 	 PathFindingHumanTimer();
		~PathFindingHumanTimer();
		void setSecondsLeft(int seconds);

		void stopTimer();
		void runTimer();
		void execTimer();

		void setOldPlayerX(int x);
		void setOldPlayerY(int y);

		virtual void run ();
};

class MachineGunShootingTimer : public IceUtil::Thread
{
	private:
	 int _runningCustom;

	public:
	 	 MachineGunShootingTimer();
		~MachineGunShootingTimer();
		void setSecondsLeft(int seconds);

		void stopTimer();
		void runTimer();
		void execTimer();

		virtual void run ();
};

class MessageInitLevelTimer : public IceUtil::Thread
{
	private:
	public:
		MessageInitLevelTimer();
		~MessageInitLevelTimer();
		void setSecondsLeft(int seconds);

		void stopTimer();
		void runTimer();
		void execTimer();

		int _runningCustom;


		virtual void run ();
};
#endif /* TIMERSMANAGER_H */
