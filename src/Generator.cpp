#include "Generator.h"

Generator::Generator(Ogre::Vector3 position, ENEMY_TYPE typeGenerator, int numCharacters) : _position(position),
																						 _typeGenerator(typeGenerator),
																						 _numCharacters(numCharacters) {}

Generator::Generator(float x, float y, float z, ENEMY_TYPE typeGenerator, int numCharacters) : _position(x,y,z),
																						    _typeGenerator(typeGenerator),
																							_numCharacters(numCharacters) {}


void Generator::setPosition(Ogre::Vector3 pos)
{
	_position = pos;
}

void Generator::setPosition(float x, float y,  float z)
{
	_position.x = x;
	_position.y = y;
	_position.z = z;
}

Ogre::Vector3 Generator::getPosition() const
{
	return _position;
}

void Generator::setTypeGenerator(ENEMY_TYPE typeGenerator)
{
	_typeGenerator = typeGenerator;
}

ENEMY_TYPE Generator::getTypeGenerator() const
{
	return _typeGenerator;
}

void Generator::setNumCharacters(int numCharacters)
{
	_numCharacters = numCharacters;
}

int Generator::getNumCharacters() const
{
	return _numCharacters;
}
