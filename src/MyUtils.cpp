#include "MyUtils.h"

template<> MyUtils* Ogre::Singleton<MyUtils>::msSingleton = 0;

Ogre::Vector3 MyUtils::calculateTranslate(
		  Ogre::Quaternion currentOrientation,
		  Ogre::Vector3 translation)
{
		Ogre::Quaternion p = currentOrientation;
		Ogre::Quaternion q = Ogre::Quaternion(0, translation.x, translation.y, translation.z);
		Ogre::Quaternion pq = p * q * p.Inverse();
		Ogre::Vector3 pqv = Ogre::Vector3(pq.x,pq.y,pq.z);

		return pqv;
}

Ogre::Quaternion MyUtils::multiply(Ogre::Quaternion q1, Ogre::Quaternion q2)
{
	Ogre::Quaternion result = q1 * q2 * q1.Inverse();
	return result;
}

int MyUtils::rangeRandomNumber (int min, int max)
{
    int n = max - min + 1;
    int remainder = RAND_MAX % n;
    int x;
    do
    {
        x = rand();
    }while (x >= RAND_MAX - remainder);
    return min + x % n;
}

float MyUtils::rangeRandomNumber(float min, float max)
{
    float random = ((float) rand()) / (float) RAND_MAX;
    float diff = max - min;
    float r = random * diff;
    return min + r;
}


MyUtils* MyUtils::getSingletonPtr ()
{
	return msSingleton;
}

MyUtils& MyUtils::getSingleton ()
{
	assert(msSingleton);
	return *msSingleton;
}
