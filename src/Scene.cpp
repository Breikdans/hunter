#include "Scene.h"


Scene::Scene()
{
	_level		= 0;
	_dimensionX	= 0;
	_dimensionY	= 0;

	_zombieGenerators.reserve(20);

	_pathFindingEnemyTimer = new PathFindingEnemyTimer();
	_pathFindingHumanTimer = new PathFindingHumanTimer();

}

PathFindingHumanTimer* Scene::getPathFindingHumanTimer()
{
	return _pathFindingHumanTimer;
}

PathFindingEnemyTimer* Scene::getPathFindingEnemyTimer()
{
	return _pathFindingEnemyTimer;
}

Scene::Scene(const Scene& S)
{
//	*this = S;
}

//Scene& Scene::operator=(const Scene &S) : _cameras(S._cameras.size()), _balls(S._balls.size())	// inicializador constructor copia para C++11, reserva espacio para vectores
Scene& Scene::operator=(const Scene &S)
{
//	_level			= S._level;
//	_dimensionX		= S._dimensionX;
//	_dimensionY		= S._dimensionY;
//
//    std::vector<Element*>::const_iterator that_elements_it = S._elements.begin();	// iterador constante para el vector origen (vector recibido)
//
//	for(; that_elements_it != S._elements.end(); that_elements_it++)
//	{
//		_elements.push_back(new Element(**that_elements_it));
//	}
//
//    std::vector<Element*>::const_iterator that_enemyGenerator_it = S._enemyGenerator.begin();	// iterador constante para el vector origen (vector recibido)
//
//	for(; that_enemyGenerator_it != S._enemyGenerator.end(); that_enemyGenerator_it++)
//	{
//		_enemyGenerator.push_back(new Element(**that_enemyGenerator_it));
//	}
//
	return *this;
}

Scene::~Scene()
{
	if(_pathFindingEnemyTimer != 0)
	{
		_pathFindingEnemyTimer->stopTimer();
//		while(getPathFindingEnemyTimer()->isAlive());
		delete _pathFindingEnemyTimer;
		_pathFindingEnemyTimer = 0;
	}

	if(_pathFindingHumanTimer != 0)
		{
		_pathFindingHumanTimer->stopTimer();
	//		while(getPathFindingEnemyTimer()->isAlive());
			delete _pathFindingHumanTimer;
			_pathFindingHumanTimer = 0;
		}


	for (std::vector< Generator* >::iterator it = _zombieGenerators.begin() ; it != _zombieGenerators.end(); ++it)
	{
		delete (*it);
	}
	_zombieGenerators.clear();

	for (std::vector< Generator* >::iterator it = _humanGenerators.begin() ; it != _humanGenerators.end(); ++it)
	{
		delete (*it);
	}
	_humanGenerators.clear();
}

void Scene::initGrid()
{
	_grid.InitGrid(_dimensionY, _dimensionX);	// Y = Filas(rows). X = Columnas(cols)
}

void Scene::setLevel(int level)
{
	_level = level;
}

int Scene::getLevel() const
{
	return _level;
}


void Scene::setDimensionEdge(float dim) {
	_dimensionEdge = dim;
}

float Scene::getDimensionEdge() const {
	return _dimensionEdge;
}

void Scene::setDimensionX(int X)
{
	_dimensionX = X;
}

int Scene::getDimensionX() const
{
	return _dimensionX;
}

void Scene::setDimensionY(int Y)
{
	_dimensionY = Y;
}

int Scene::getDimensionY() const
{
	return _dimensionY;
}


void Scene::addZombieGenerator(Generator* zombieGenerator)
{
	_zombieGenerators.push_back(zombieGenerator);
}

void Scene::addHumanGenerator(Generator* humanGenerator)
{
	_humanGenerators.push_back(humanGenerator);
}



Cell& Scene::grid(int X, int Y)
{
	return _grid(X,Y);
}

Cell& Scene::grid(int X)
{
	return _grid(X);
}

//Cell Scene::grid(int X, int Y) const
//{
//	return _grid(X,Y);
//}

std::deque<Cell*> Scene::calculateAStarPath(const Cell& start, const Cell& end)
{
	return _grid.calculateAStarPath(start, end);
}

void Scene::DebugGrid()
{
	_grid.DebugGrid();
}

void Scene::DebugCalculatedPath(const std::deque<Cell*>&path)
{
	_grid.DebugCalculatedPath(path);
}

void Scene::DebugCell(const Cell& cell)
{
	_grid.DebugCell(cell);
}




