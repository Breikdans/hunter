#include <Ogre.h>
#include "Weapon.h"



//Weapon::Weapon(int ammo, Ogre::SceneNode* node, std::string mesh, EN_WEAPON_TYPE weaponType) :  _ammo(ammo),
//																								_node(node),
//																								_mesh(mesh),
//																								_weaponType(weaponType) {}

Weapon::Weapon()
{
	_ammo 		= 0;
	_node 		= 0;
	_force		= 0;
	_mesh		= "";
	_weaponType = EN_NONE;
}

Weapon::Weapon(const Weapon& C)
{
	*this = C;
}

Weapon& Weapon::operator= (const Weapon &C)
{
	_ammo		= C._ammo;
	_force		= C._force;
	_mesh		= C._mesh;
	_weaponType = C._weaponType;
//	if (_node)	delete _node;
//
//	_node		= new Ogre::SceneNode(*C._node);
//	*(_node) 	= *(C._node);

	return *this;
}

Weapon::~Weapon()
{

//	if(_node)
//		delete _node;
}

void Weapon::setPosition(Ogre::Vector3 pos)
{
	_node->setPosition(pos.x,pos.y,pos.z);
}

void Weapon::setPosition(float x=0, float y=0, float z=0)
{
	_node->setPosition(x,y,z);
}

Ogre::Vector3 Weapon::getPosition() const
{

	return _node->getPosition();

}

Ogre::SceneNode* Weapon::getNode()
{
	return _node;
}

void Weapon::setNode(Ogre::SceneNode* n)
{
	_node = n;
}

std::string Weapon::getMesh() const
{
	return _mesh;
}

void Weapon::setMesh(std::string mesh)
{
	_mesh = mesh;
}

int Weapon::getAmmo() const
{
	return _ammo;
}

void Weapon::setAmmo(int ammo)
{
	_ammo = ammo;
}

void Weapon::setForce(int force)
{
	_force = force;
}

int Weapon::getForce() const
{
	return _force;
}

void Weapon::decAmmo( int dec)
{
	if(_ammo > 0)
		_ammo -= dec;
}

EN_WEAPON_TYPE Weapon::getWeaponType() const
{
	return _weaponType;
}

void Weapon::setWeaponType(EN_WEAPON_TYPE type)
{
	_weaponType = type;
}

Ogre::AnimationState* Weapon::getShootAnimation()
{
	return _shootAnimation;
}

Ogre::AnimationState* Weapon::getWalkAnimation()
{
	return _walkAnimation;
}

void Weapon::setWalkAnimation(Ogre::AnimationState* animation)
{
	_walkAnimation = animation;
}

void Weapon::setShootAnimation(Ogre::AnimationState* animation)
{
	_shootAnimation = animation;
}

void Weapon::setDownAnimation(Ogre::AnimationState* animation)
{
	_downAnimation = animation;
}

Ogre::AnimationState* Weapon::getDownAnimation()
{
	return _downAnimation;
}
