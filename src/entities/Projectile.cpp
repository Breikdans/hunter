#include "Projectile.h"

Projectile::Projectile(Ogre::SceneNode* sceneNode, OgreBulletDynamics::RigidBody* rigidBody) : _sceneNode(sceneNode), _rigidBody(rigidBody) {}

Projectile::~Projectile()
{
	if(_rigidBody != 0)
		delete _rigidBody;
	
	_rigidBody = 0;

	Ogre::SceneManager* sceneMgr = Ogre::Root::getSingletonPtr()->getSceneManager("SceneManager");
	if (_sceneNode != 0)
	{
		sceneMgr->destroySceneNode(_sceneNode);
		_sceneNode = 0;
	}
}

void Projectile::setSceneNode(Ogre::SceneNode* sceneNode)
{
	_sceneNode = sceneNode;
}

Ogre::SceneNode* Projectile::getSceneNode()
{
	return _sceneNode;
}

void Projectile::setRigidBody(OgreBulletDynamics::RigidBody* rigidBody)
{
	_rigidBody = rigidBody;
}

OgreBulletDynamics::RigidBody* Projectile::getRigidBody()
{
	return _rigidBody;
}
