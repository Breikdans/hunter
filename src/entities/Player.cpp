#include "Player.h"
#include "PlayState.h"


Player::Player(Ogre::SceneNode* node, float speed, std::string mesh, float speedPitch, Weapon* weapon) : Character(node, speed, mesh), _speedPitch(speedPitch), _weapon(weapon){}

Player::Player(const Player& P)
{
	*this = P;
}

Player& Player::operator=(const Player& P)
{
	Character::operator=(P);
	_speedPitch = P._speedPitch;
	_initialOrientation = P._initialOrientation;
	_initialPosition = P._initialPosition;
	_initialWeapons = P._initialWeapons;
	return *this;
}

Player::~Player()
{
	_weapon = 0;
}

int Player::getSpeedPitch() const
{
	return _speedPitch;
}

void Player::setSpeedPitch(float speedPitch)
{
	_speedPitch = speedPitch;
}

Weapon*	Player::getWeapon() const
{
	return _weapon;
}

void Player::setWeapon(Weapon* weapon)
{
	_weapon = weapon;
}

void Player::addWeapon(Weapon* weapon)
{
	_inventaryWeapons.push_back(weapon);
}

Weapon* Player::getInventaryWeapon(EN_WEAPON_TYPE weaponType)
{
	Weapon* weaponResult = 0;
	std::vector<Weapon*>::iterator it = _inventaryWeapons.begin();

	for(; it != _inventaryWeapons.end(); it++)
	{
		if ((*it)->getWeaponType() == weaponType)
		{
			weaponResult = (*it);
			break;
		}
	}

	return weaponResult;
}

void Player::lookToTarget(Ogre::SceneManager *scn)
{
	Ogre::Vector3 v = Ogre::Vector3(0,0,0);
	Ogre::Vector3 vEnd = Ogre::Vector3(0,0,0);
	OgreBulletDynamics::DynamicsWorld* world = PhysicsManager::getSingleton().getWorld();

	Ogre::Camera* mainCamera = PlayState::getSingleton().getMainCamera();

	PhysicsManager::getSingleton().buildOgreBulletRayFromCamera(v, vEnd, world, 0.5, 0.5, mainCamera);

	scn->getSceneNode("projectileNode")->lookAt(vEnd,Ogre::SceneNode::TS_WORLD);
	if (v!=Ogre::Vector3::ZERO)
	{
		//std::cout << "Le doy a " << v.x << "," << v.y << "," << v.z << std::endl;
		scn->getSceneNode("projectileNode")->lookAt(v,Ogre::SceneNode::TS_WORLD);

		Ogre::Vector3 diff = getNode()->getPosition() - v;

		getWeapon()->getNode()->lookAt(vEnd,Ogre::SceneNode::TS_WORLD);
		if (v!=Ogre::Vector3::ZERO)
		{
			if (abs(diff.z)>4) {
				getWeapon()->getNode()->lookAt(v,Ogre::SceneNode::TS_WORLD);
			}
		}
	}
}

void Player::changeWeapon(EN_CURRENT_STATE state, Ogre::Real deltaT)
{
	if (state == EN_CHANGE_WEAPON_DEVASTATOR)
	{
		Ogre::AnimationState* anim = getWeapon()->getDownAnimation();
		Ogre::AnimationState* walk = getWeapon()->getWalkAnimation();

		walk->setEnabled(false);
		if (anim->getTimePosition() < anim->getLength())
		{
			anim->addTime(deltaT);
		}
		else
		{
			static bool initUpWeaponDevastator = false;
			Ogre::AnimationState* upWeapon = getWeapon()->getDownAnimation();

			if(initUpWeaponDevastator == false)
			{
				Weapon* weapon = 0;
				// desatachamos el nodo del arma
				PlayState::getSingleton().DettachWeaponToCameraPitch();

				// cambiamos arma actual al nuevo arma
				weapon = getInventaryWeapon(EN_DEVASTATOR);
				if (weapon != 0)
				{
					setWeapon(weapon);	// establecemos arma principal
					PlayState::getSingleton().attachWeaponToCameraPitch();

					upWeapon->setEnabled(true);
					upWeapon->setLoop(false);
					upWeapon->setTimePosition(upWeapon->getLength());
				}

				initUpWeaponDevastator = true;
			}

			if (upWeapon->getTimePosition() >= 0)
			{
//cout << "time position devastator UP: " << upWeapon->getTimePosition() << endl;
				upWeapon->addTime(-deltaT);
			}

			if (upWeapon->getTimePosition() >= 0)
			{
				upWeapon->setEnabled(false);
				setState(EN_ALIVE);
				initUpWeaponDevastator = false;
			}
		}
	}
	else if (state == EN_CHANGE_WEAPON_MACHINEGUN)
	{
		Ogre::AnimationState* anim = getWeapon()->getDownAnimation();
		Ogre::AnimationState* walk = getWeapon()->getWalkAnimation();

		walk->setEnabled(false);
		if (anim->getTimePosition() < anim->getLength())
		{
			anim->addTime(deltaT);
		}
		else
		{
			static bool initUpWeaponMachineGun = false;
			Ogre::AnimationState* upWeapon = getWeapon()->getDownAnimation();

			if(initUpWeaponMachineGun == false)
			{
				Weapon* weapon = 0;
				// desatachamos el nodo del arma
				PlayState::getSingleton().DettachWeaponToCameraPitch();

				// cambiamos arma actual al nuevo arma
				weapon = getInventaryWeapon(EN_MACHINEGUN);
				if (weapon != 0)
				{
					setWeapon(weapon);	// establecemos arma principal
					PlayState::getSingleton().attachWeaponToCameraPitch();

					upWeapon->setEnabled(true);
					upWeapon->setLoop(false);
					upWeapon->setTimePosition(upWeapon->getLength());
				}

				initUpWeaponMachineGun = true;
			}

			// vamos levantando el nuevo arma
			if (upWeapon->getTimePosition() >= 0)
			{
//cout << "time position machinegun UP: " << upWeapon->getTimePosition() << endl;
				upWeapon->addTime(-deltaT);
			}

			if (upWeapon->getTimePosition() >= 0)
			{
				upWeapon->setEnabled(false);
				setState(EN_ALIVE);
				initUpWeaponMachineGun = false;
			}
		}
	}
}

void Player::translate(Ogre::Real deltaT, Ogre::Vector3 vt)
{
	if (_currentState != EN_CHANGE_WEAPON_DEVASTATOR &&
		_currentState != EN_CHANGE_WEAPON_MACHINEGUN) {

		if (vt!=Ogre::Vector3::ZERO) {
			getWeapon()->getDownAnimation()->setEnabled(false);
			getWeapon()->getWalkAnimation()->setEnabled(true);
			getWeapon()->getWalkAnimation()->setLoop(true);
			getWeapon()->getWalkAnimation()->addTime(deltaT);

		} else {
			getWeapon()->getWalkAnimation()->setEnabled(false);
			getWeapon()->getWalkAnimation()->setLoop(false);
			getWeapon()->getWalkAnimation()->setTimePosition(0.0);
		}

		if (getWeapon()->getShootAnimation()->hasEnded()==false)
			getWeapon()->getShootAnimation()->addTime(deltaT);
	}

	Ogre::Vector3 pqv = MyUtils::getSingleton().calculateTranslate(getNode()->getOrientation(),vt);
	pqv = pqv * deltaT;
	getRigidBody()->getBulletRigidBody()->translate(btVector3(pqv.x,pqv.y,pqv.z));
}

void Player::setInitialPosition(Ogre::Vector3 pos) {
	_initialPosition = pos;
}

void Player::setInitialOrientation(Ogre::Quaternion rot) {
	_initialOrientation = rot;
}

Ogre::Vector3 Player::getInitialPosition() {
	return _initialPosition;
}

Ogre::Quaternion Player::getInitialOrientation() {
	return _initialOrientation;
}

void Player::addInitialWeapon(const EN_WEAPON_TYPE& weapon)
{
	_initialWeapons.push_back(weapon);
}

std::vector<EN_WEAPON_TYPE> Player::getInitialWeapons() const
{
	return _initialWeapons;
}












