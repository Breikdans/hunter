#include <math.h>
#include "PlayState.h"
#include "Character.h"
#include "MyUtils.h"



bool Character::_move = true;

Character::Character(Ogre::SceneNode* node, float speed, std::string mesh, int life) : _node(node), _speed(speed), _mesh(mesh), _life(life)
{
	_rigidBody 	= 0;
}

Character::Character(const Character& C)
{
	*this = C;
}

Character& Character::operator= (const Character &C)
{
	_speed		= C._speed;
	_mesh		= C._mesh;
	_life		= C._life;

//	if (_node)	delete _node;
//
//	_node		= new Ogre::SceneNode(*C._node);
//	*(_node) 	= *(C._node);

	return *this;
}

Character::~Character()
{
	if(_rigidBody != 0)
		delete _rigidBody;

	_rigidBody = 0;
	_path.clear();
}

void Character::setPosition(Ogre::Vector3 pos)
{
	_node->setPosition(pos.x,pos.y,pos.z);
}

void Character::setPosition(float x=0, float y=0, float z=0)
{
	_node->setPosition(x,y,z);
}

Ogre::Vector3 Character::getPosition() const
{

	return _node->getPosition();

}

Ogre::SceneNode* Character::getNode()
{
	return _node;
}

void Character::setState(EN_CURRENT_STATE state)
{
	_currentState = state;
}

EN_CURRENT_STATE Character::getState() const
{
	return _currentState;
}

void Character::setNode(Ogre::SceneNode* n)
{
	_node = n;
}

float Character::getSpeed() const
{
	return _speed;
}

void Character::setRigidBody(OgreBulletDynamics::RigidBody* rigidBody)
{
	_rigidBody = rigidBody;
}

OgreBulletDynamics::RigidBody* Character::getRigidBody() const {
	return _rigidBody;
}

void Character::setSpeed(float s)
{
	_speed=s;
}

std::string Character::getMesh() const
{
	return _mesh;
}

void Character::setMesh(std::string mesh)
{
	_mesh = mesh;
}



void Character::setMove(bool M)
{
	_move = M;
}

bool Character::getMove(void)
{
	return _move;
}

void Character::setCellX(int x)
{
	_cellX = x;
}

void Character::setCellY(int y)
{
	_cellY = y;
}

int Character::getCellX(void) const
{
	return _cellX;
}

int Character::getCellY(void) const
{
	return _cellY;
}

void Character::CoordsToCellIndex()
{
	// calculamos dimensiones, tomando de centro a centro de la celda, que es lo que tenemos
	float cell_width  = PlayState::getSingleton().getScene()->getDimensionEdge();
	float cell_height = PlayState::getSingleton().getScene()->getDimensionEdge();
	
	const Cell cell_ini = PlayState::getSingleton().getScene()->grid(0,0);

	// calculamos distancia desde posicion del caracter hasta el inicio del grid
	// le añadimos la mitad de la celda, porque lo estamos calculando respecto al centro de la primera celda
	float distanceX = abs(  getPosition().x - (cell_ini.getX() - (cell_width/2)) );
	float distanceY = abs( -getPosition().z - (cell_ini.getY() + (cell_height/2)) );
		
	// calculamos el indice del grid correspondiente
	// si la distancia / tamaño_celda es divisible (no da resto 0), esta justo en el limite de la celda, asignamos ese indice
	// si la distancia / tamaño_celda no es divisible (hay resto), significa que se ha pasado y su celda es la siguiente
	int indexX = (fmodf(distanceX, cell_width)  == 0) ? distanceX / cell_width  : (distanceX / cell_width) + 1;
	int indexY = (fmodf(distanceY, cell_height) == 0) ? distanceY / cell_height : (distanceY / cell_height) + 1;
	
	_cellX = indexX - 1;
	_cellY = indexY - 1;
}

void Character::setPath(const std::deque<Cell*>& path)
{
	_path = path;
}

std::deque<Cell*> Character::getPath()
{
	return _path;
}

int Character::getLife() const
{
	return _life;
}

void Character::setLife(int life)
{
	_life = life;
}

void Character::decrementLife(int dec)
{
	_life - dec < 0 ? _life = 0 : _life -= dec;
cout << _node->getName() << " life: " << _life << endl;
}

int Character::getOldTargetX() const
{
	return _oldTargetX;
}

void Character::setOldTargetX(int targetX)
{
	_oldTargetX = targetX;
}

int Character::getOldTargetY() const
{
	return _oldTargetY;
}

void Character::setOldTargetY(int targetY)
{
	_oldTargetY = targetY;
}

void Character::setTarget(Character* Ch)
{
	_target = Ch;
}

Character* Character::getTarget() const
{
	return _target;
}

Ogre::AnimationState* Character::getWalkingAnimation()
{
	return _walkingAnimation;
}

void Character::setWalkingAnimation(Ogre::AnimationState* animation)
{
	_walkingAnimation = animation;
}

Ogre::AnimationState* Character::getDeadAnimation()
{
	return _deadAnimation;
}

void Character::setDeadAnimation(Ogre::AnimationState* animation)
{
	_deadAnimation = animation;
}

Ogre::AnimationState* Character::getDeadVaporizeAnimation()
{
	return _deadVaporize;
}

void Character::setDeadVaporizeAnimation(Ogre::AnimationState* animation)
{
	_deadVaporize = animation;
}

