#include "Element.h"

Element::Element(std::string id, Ogre::SceneNode* node, std::string mesh) : _id(id), _node(node), _mesh(mesh) {}

void Element::setPosition(Ogre::Vector3 pos)
{
	_node->setPosition(pos.x,pos.y,pos.z);
}

void Element::setPosition(float x=0, float y=0, float z=0)
{
	_node->setPosition(x,y,z);
}

Ogre::Vector3 Element::getPosition() const
{
	return _node->getPosition();
}

void Element::setId(std::string id)
{
	_id = id;
}

std::string Element::getId() const
{
	return _id;
}

Ogre::SceneNode* Element::getNode()
{
	return _node;
}

void Element::setNode(Ogre::SceneNode* n)
{
	_node = n;
}

std::string Element::getMesh() const
{
	return _mesh;
}

void Element::setMesh(std::string mesh)
{
	_mesh = mesh;
}


