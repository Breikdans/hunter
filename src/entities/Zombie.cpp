#include "Zombie.h"
#include "PlayState.h"

Zombie::Zombie(Ogre::SceneNode* node, float speed, std::string mesh, int life) :
												Character(node, speed, mesh, life)
{
	_particleSystem = 0;
	_target			= 0;
	_currentState	= EN_CHASE;

	_deadVaporize 		= 0;
	_deadAnimation		= 0;
	_walkingAnimation	= 0;
}

Zombie::Zombie(const Zombie& Z)
{
	*this = Z;
}

Zombie& Zombie::operator=(const Zombie& Z)
{
	Character::operator=(Z);
	_life = Z._life;
	_currentState = Z._currentState;
	_particleSystem = 0;

	_deadVaporize 		= 0;
	_deadAnimation		= 0;
	_walkingAnimation	= 0;
	_target			= 0;

	return *this;
}

Zombie::~Zombie()
{
	if(_deadAnimation != 0)
		getDeadAnimation()->setEnabled(false);

	if(_deadVaporize != 0)
		getDeadVaporizeAnimation()->setEnabled(false);

	if(_walkingAnimation != 0)
		getWalkingAnimation()->setEnabled(false);

	_deadAnimation = 0;
	_walkingAnimation = 0;
	_deadVaporize = 0;

	_path.clear();

	delete _rigidBody;
	_rigidBody = 0;

	Ogre::SceneManager* sceneMgr = Ogre::Root::getSingletonPtr()->getSceneManager("SceneManager");
	if (_node != 0)
	{
		sceneMgr->destroySceneNode(_node);
		_node = 0;
	}
}

void Zombie::turn(float deltaT)
{
	if (_currentState != EN_DEAD && _currentState != EN_DEAD_VAPORIZE && _currentState != EN_CLEAR)
		CheckState();

	switch(_currentState)
	{
		case EN_ALIVE:
		case EN_RUNAWAY:
		case EN_CLEAR:
		case EN_CHANGE_WEAPON_DEVASTATOR:
		case EN_CHANGE_WEAPON_MACHINEGUN:
			break;
		case EN_CHASE:
			setDestiny();
			translate(deltaT);
			break;
		case EN_WANDER:
			break;
		case EN_ATTACK:
			attack();
			//setState(EN_CHASE);
			break;
		case EN_DEAD:
			die(deltaT);
			break;
		case EN_DEAD_VAPORIZE:
			vaporize(deltaT);
			break;
	}
}

void Zombie::CheckState()
{
	const float EPSILON_ATTACK = 2.0f;

	// si estamos a una distancia del jugador menor que el epsilon
	if ( fabs(getPosition().x - _target->getPosition().x ) 	<= EPSILON_ATTACK &&
		 fabs(getPosition().z - _target->getPosition().z )	<= EPSILON_ATTACK &&
		 _target->getState() != EN_DEAD)

	{
		setState(EN_ATTACK);
	}
	else
		setState(EN_CHASE);
}

void Zombie::setDestiny()
{
	const float EPSILON 					= (PlayState::getSingleton().getScene()->getDimensionEdge() / 2.0f);
	const float EPSILON_STRAIGHT_ATTACK 	=  PlayState::getSingleton().getScene()->getDimensionEdge() * 2.0f;

	// IMPORTANTE!! Refrescamos la posicion en el grid
	CoordsToCellIndex();

	// SI ESTAMOS en la MISMA CELDA... VAMOS A POR EL DIRECTAMENTE
//	if (getCellX() == _target->getCellX() && getCellY() == _target->getCellY())
//	{
	// si estamos a una distancia del jugador menor que el epsilon
	if ( fabs(getPosition().x - _target->getPosition().x ) 	<= EPSILON_STRAIGHT_ATTACK &&
		 fabs(getPosition().z - _target->getPosition().z )	<= EPSILON_STRAIGHT_ATTACK )
	{
		// Establecemos en coordenadas OGRE (x,z,-y)
		_vDestiny = _target->getPosition();
	}
	else
	{
		// si el zombie NO ESTA EN LA MISMA CELDA del TARGET Y queda camino... nuestro destino es la primera celda del camino
		if(_path.size() > 0)
		{
//cout << "path: X: " << (*(_path.begin()))->getX() << " Y: " << (*(_path.begin()))->getY() << " Z: " << (*(_path.begin()))->getZ() << endl;
			_vDestiny.x =   (*(_path.begin()))->getX();
			_vDestiny.z = - (*(_path.begin()))->getY();
		}

		// comprobamos si hemos llegado al siguiente paso del path
		if ( fabs(getPosition().x - _vDestiny.x ) 	<= EPSILON &&
			 fabs(getPosition().z - _vDestiny.z )	<= EPSILON )
		{
			if(_path.size() > 1)
			{
				_path.pop_front();
			}
			_vDestiny.x =   (*(_path.begin()))->getX();
			_vDestiny.z = - (*(_path.begin()))->getY();

		}
	}
	_vDestiny.y =   getPosition().y;



//cout << "vDestiny: X: " << _vDestiny.x << " Y: " << _vDestiny.y << " Z: " << _vDestiny.z << endl;
}

void Zombie::translate(Ogre::Real deltaT, Ogre::Vector3 vt)
{
	getWalkingAnimation()->addTime(deltaT);

	getNode()->lookAt(_vDestiny,Ogre::SceneNode::TS_WORLD);
	Ogre::Quaternion q = Ogre::Quaternion(0, 0, 0, - getSpeed());
	Ogre::Quaternion q1 = getNode()->getOrientation();

	Ogre::Quaternion pq = MyUtils::getSingleton().multiply(q1,q);
	Ogre::Vector3 pqv = Ogre::Vector3(pq.x,pq.y,pq.z);
	getRigidBody()->setLinearVelocity(pqv);
}



void Zombie::seekTarget()
{
	// posicion en pixels
	int closestHumanPosX = abs(_node->getPosition().x - EntityManager::getSingleton().getPlayer()->getPosition().x);
	int closestHumanPosZ = abs(_node->getPosition().z - EntityManager::getSingleton().getPlayer()->getPosition().z);

	setTarget(EntityManager::getSingleton().getPlayer());

	// guardamos la posicion en celdas del objetivo, para posteriormente comparar si se ha movido y calcular el camino hasta el
	setOldTargetX(EntityManager::getSingleton().getPlayer()->getCellX());
	setOldTargetY(EntityManager::getSingleton().getPlayer()->getCellY());

	std::vector<Character*> humans = EntityManager::getSingleton().getHumans();
	std::vector<Character*>::iterator it_humans = humans.begin();
	for (; it_humans != humans.end(); it_humans++)
	{
		if ((*it_humans)->getState() != EN_DEAD)
		{
			if ( (abs(_node->getPosition().x - (*it_humans)->getPosition().x) < closestHumanPosX) &&
				 (abs(_node->getPosition().z - (*it_humans)->getPosition().z) < closestHumanPosZ) )
			{
				closestHumanPosX = abs(_node->getPosition().x - (*it_humans)->getPosition().x);
				closestHumanPosZ = abs(_node->getPosition().z - (*it_humans)->getPosition().z);
				setTarget((*it_humans));
				setOldTargetX((*it_humans)->getCellX());
				setOldTargetY((*it_humans)->getCellY());
			}
		}
	}
}

void Zombie::die(Ogre::Real deltaT)
{
	getNode()->lookAt(_vDestiny,Ogre::SceneNode::TS_WORLD);

	if ( getDeadAnimation()->getTimePosition() < getDeadAnimation()->getLength() )
	{
		getDeadAnimation()->addTime(deltaT);
		if( getDeadAnimation()->getTimePosition() >= getDeadAnimation()->getLength())
		{
			delete _rigidBody;
			_rigidBody = 0;
		}
	}
}

void Zombie::attack()
{
	getNode()->lookAt(Ogre::Vector3(_target->getPosition().x, getPosition().y, _target->getPosition().z),
					  Ogre::SceneNode::TS_WORLD);
}

void Zombie::vaporize(Ogre::Real deltaT)
{
	getNode()->lookAt(_vDestiny,Ogre::SceneNode::TS_WORLD);
	if (_particleSystem==0)
	{
		startParticles();
	}

	if ( getDeadVaporizeAnimation()->getTimePosition() < getDeadVaporizeAnimation()->getLength() )
	{
		getDeadVaporizeAnimation()->addTime(deltaT);
		if( getDeadVaporizeAnimation()->getTimePosition() >= getDeadVaporizeAnimation()->getLength())
		{
			stopParticles();
		}
	}
}


void Zombie::startParticles()
{
	Ogre::SceneManager* sceneMgr = Ogre::Root::getSingletonPtr()->getSceneManager("SceneManager");

	_particleSystem = sceneMgr->createParticleSystem("Ps_"+_node->getName(),"vaporize");
	Ogre::SceneNode* psNode = _node->createChildSceneNode("PsNode_"+_node->getName());
	psNode->attachObject(_particleSystem);
	psNode->setScale(0.001, 0.001, 0.001);
}

//void Zombie::stopAsyncParticles(int milliseconds)
//{
//	_timer(_io, boost::posix_time::milliseconds(milliseconds));
//	_timer.async_wait(boost::bind(&Zombie::stopParticles, this));
//	_io.run_once();
//}

void Zombie::stopParticles()
{
	Ogre::SceneManager* sceneMgr = Ogre::Root::getSingletonPtr()->getSceneManager("SceneManager");
	sceneMgr->destroyParticleSystem(_particleSystem);
//	_node->removeAndDestroyAllChildren();
	_particleSystem = 0;

	setState(EN_CLEAR);
}

