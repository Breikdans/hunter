#include "Human.h"
#include "PlayState.h"

Human::Human(Ogre::SceneNode* node, float speed, std::string mesh, int life) : Character(node, speed, mesh, life)
{
	_target			= 0;
	_currentState	= EN_RUNAWAY;

	_deadVaporize 		= 0;
	_deadAnimation		= 0;
	_walkingAnimation	= 0;
}

Human::Human(const Human& Z)
{
	*this = Z;
}

Human& Human::operator=(const Human& Z)
{
	Character::operator=(Z);
	_life = Z._life;
	_currentState = Z._currentState;

	return *this;
}

Human::~Human()
{

}

void Human::turn(float deltaT)
{
	switch(_currentState)
	{
		case EN_RUNAWAY:
			translate(deltaT);
			break;
		case EN_DEAD:
		default:
			die(deltaT);
			break;
	}
}

void Human::translate(Ogre::Real deltaT, Ogre::Vector3 vt)
{

	CoordsToCellIndex();
	getWalkingAnimation()->addTime(deltaT);

	const float EPSILON = 3.11f;

	Cell *DestinyCell;
	Cell ultima;

	// cogemos la celda en la que esta el PLAYER
	int cellX = EntityManager::getSingleton().getPlayer()->getCellX();
	int cellY = EntityManager::getSingleton().getPlayer()->getCellY();

	// si el Human NO ESTA EN LA MISMA CELDA del player Y queda camino... nuestro destino es la primera celda del camino
	if ( (cellX != getCellX() || cellY != getCellY()) && _path.size() > 0)
		DestinyCell = *(_path.begin());
	else	// si no...
	{
		ultima = PlayState::getSingleton().getScene()->grid(cellX, cellY);	// cogemos como destino, la celda y posicion del PLAYER
		ultima.setX(EntityManager::getSingleton().getPlayer()->getNode()->getPosition().x);
		ultima.setY(EntityManager::getSingleton().getPlayer()->getNode()->getPosition().y);
		ultima.setZ(EntityManager::getSingleton().getPlayer()->getNode()->getPosition().z);
		DestinyCell = &ultima;
	}

//cout << "DESTINO X: " << actualCell->getIndexX() << " DESTINO Y: " << actualCell->getIndexY() << " actual X: " << getCellX() << " actual Y: " << getCellY() << endl;

	// comprobamos si hemos llegado al siguiente paso del path
	if ( fabs(getNode()->getPosition().x - DestinyCell->getX() ) 		<= EPSILON &&
		 fabs(getNode()->getPosition().z - (- DestinyCell->getY()) )	<= EPSILON )
	{

		// comprobamos si hay mas pasos en el path
		if (_path.size() > 1)
		{
			_path.pop_front();
			DestinyCell = *(_path.begin());
		}
		// sino, tomamos la posicion del jugador como objetivo
		else
		{
			ultima = PlayState::getSingleton().getScene()->grid(cellX, cellY);
			ultima.setX(EntityManager::getSingleton().getPlayer()->getNode()->getPosition().x);
			ultima.setY(EntityManager::getSingleton().getPlayer()->getNode()->getPosition().y);
			ultima.setZ(EntityManager::getSingleton().getPlayer()->getNode()->getPosition().z);
			DestinyCell = &ultima;
		}
	}

	// punto destino
	_vDestiny = Ogre::Vector3(DestinyCell->getX(), getPosition().y, - DestinyCell->getY());
	getNode()->lookAt(_vDestiny,Ogre::SceneNode::TS_WORLD);
//		btQuaternion bCurrent = btQuaternion(
//				getNode()->getOrientation().x,getNode()->getOrientation().y,
//				getNode()->getOrientation().z,getNode()->getOrientation().w);

	//getRigidBody()->setOrientation(bCurrent);
	Ogre::Quaternion q = Ogre::Quaternion(0, 0, 0, -1);
	Ogre::Quaternion q1 = getNode()->getOrientation();

	Ogre::Quaternion pq = MyUtils::getSingleton().multiply(q1,q);
	Ogre::Vector3 pqv = Ogre::Vector3(pq.x,pq.y,pq.z);
	getRigidBody()->setLinearVelocity(pqv);

}

void Human::setState(EN_CURRENT_STATE state)
{
	_currentState = state;
}

EN_CURRENT_STATE Human::getState() const
{
	return _currentState;
}

void Human::seekTarget()
{
	_target = EntityManager::getSingleton().getPlayer();
}

void Human::setTarget(Character* Ch)
{
	_target = Ch;
}

Character* Human::getTarget() const
{
	return _target;
}

void Human::die(Ogre::Real deltaT)
{
	getNode()->lookAt(_vDestiny,Ogre::SceneNode::TS_WORLD);

	if ( getDeadAnimation()->getTimePosition() < getDeadAnimation()->getLength() )
	{
		getDeadAnimation()->addTime(deltaT);
		if( getDeadAnimation()->getTimePosition() >= getDeadAnimation()->getLength())
		{
			delete _rigidBody;
			_rigidBody = 0;
		}
	}
}










