#include "EntityManager.h"
#include "PhysicsManager.h"
#include "Devastator.h"


Devastator::Devastator()
{
	_positionNodeProjectile = Ogre::Vector3(0, 0, -0.8);
}

void Devastator::shoot(const int impulse)
{
	createProjectile(impulse);
}

Devastator::Devastator(const Devastator& D)
{
	*this = D;
}

Devastator& Devastator::operator=(const Devastator& D)
{
	Weapon::operator=(D);

	return *this;
}

Ogre::SceneNode* Devastator::getNodeProjectile()
{
	return _scnNodeProjectile;
}

void Devastator::setNodeProjectile(Ogre::SceneNode* nodeProjectile)
{
	_scnNodeProjectile = nodeProjectile;
}

void Devastator::setPositionNodeProjectile(Ogre::Vector3 vPos)
{
	_positionNodeProjectile = vPos;
}

void Devastator::createProjectile(const int impulse) {

	const int MAX_IMPULSE 		= 30;
	const int INITIAL_IMPULSE 	= 15;

	// calculamos el tiro -----------
	 int impulsePercent = (impulse * MAX_IMPULSE) / 100;
	//std::cout << "PULSACION AÑADIDA: " << impulsePercent << endl;
	 int finalImpulse = INITIAL_IMPULSE + impulsePercent;

	Projectile *p = new Projectile;

	std::stringstream nameProjectileNode;

	EntityManager* entityManager = EntityManager::getSingletonPtr();

	// creamos el proyectil nuevo
	nameProjectileNode << "projectile_" << entityManager->getInstanceNumber();

	Ogre::Entity *entity = entityManager->createEntity(nameProjectileNode.str(), "projectile.mesh");
	p->setSceneNode(entityManager->createSceneNode(nameProjectileNode.str()));
	p->getSceneNode()->attachObject(entity);

	// lo adjuntamos al nodo base
	entityManager->getBaseNode()->addChild(p->getSceneNode());
	p->getSceneNode()->setPosition(getNodeProjectile()->_getDerivedPosition());
	decAmmo();

	OgreBulletDynamics::RigidBody* rigidBody = PhysicsManager::getSingleton().createRigidBody(entity,"PROJECTILE",getNodeProjectile()->_getDerivedPosition(),Ogre::Quaternion::IDENTITY);
	// añadimos el rigidBody al proyectil
	p->setRigidBody(rigidBody);
	// Se registra en el manager de las entidades
	entityManager->addProjectile(p);

	Ogre::Quaternion p1 =  getNodeProjectile()->_getDerivedOrientation();

	// - convertimos el vector de desplazamiento relativo a la pulsacion de teclado, en un cuaternio, añadiendole 0 a la parte escalar (w)
	Ogre::Quaternion q = Ogre::Quaternion(0, 0, 0, - finalImpulse);
	// - primero pre-multiplicamos el cuaternio (orientacion inicial) a girar por el cuaternio de giro, despues lo post-multiplicamos por la inversa del inicial.
	Ogre::Quaternion pq = MyUtils::getSingleton().multiply(p1,q);
	// lo pasamos a vector, para trasladarlo.
	Ogre::Vector3 pqv = Ogre::Vector3(pq.x,pq.y,pq.z);

	rigidBody->setLinearVelocity(pqv);

//	//setPhysicsProjectile(p, impulsePercent);
//	_player->getShoot()->setTimePosition(0.0);
//	_player->getShoot()->setEnabled(true);
//	_player->getShoot()->setLoop(false);
//
//	_player->getWeapon()->decAmmo();
//	updateProjectileCounter();
//	IntroState::getSingleton().getShotFXPtr()->play();
}
