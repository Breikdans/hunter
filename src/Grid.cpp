#include <string>
#include <sstream>
#include <math.h>
#include "Grid.h"

Grid::Grid(int cols, int rows, Cell* tblGrid) : _cols(cols), _rows(rows), _tblGrid(tblGrid) {}

Grid::~Grid()
{
	if (_tblGrid != 0)
		delete _tblGrid;
}

Grid::Grid(const Grid& G)
{
	*this = G;
}


Grid& Grid::operator=(const Grid & G)
{
	_cols = G._cols;
	_rows = G._rows;
	
	InitGrid(_rows, _cols);

	memcpy(_tblGrid, G._tblGrid, sizeof(*_tblGrid));
	
	DebugGrid();
	DebugGridCoordinate();
	
	return *this;
}

Cell& Grid::operator() (int X)
{
	return _tblGrid[X];
}

/* Para acceder a una posicion y DEVOLVER / ASIGNAR un valor */
Cell& Grid::operator() (int X, int Y)
{
	std::stringstream strExcepcion;

	if (_rows == 0 || _cols == 0)
	{
		strExcepcion << "Rejilla sin inicializar aun!!! o con una dimension = 0!!" << " rows: " << _rows << " cols: " << _cols << endl;
		throw std::out_of_range(strExcepcion.str());
	}

	//	LANZAMOS EXCEPCION
	if (X >= _cols || Y >= _rows)
	{
		strExcepcion << "Indice fuera de limites!" << " X: " << X << " Y: " << Y << endl;
		throw std::out_of_range(strExcepcion.str());
	}

	return _tblGrid[(Y*_cols)+X];
}

void Grid::InitGrid(int rows, int cols)
{
	std::stringstream strExcepcion;
	strExcepcion << "dimensiones invalidas!" << " Rows: " << rows << " Cols: " << cols << endl;

	if (rows <= 1 || cols <= 1)
		throw std::out_of_range(strExcepcion.str());
	
	_rows = rows;
	_cols = cols;
	
	if (_tblGrid)
		delete [] _tblGrid;
	
	_tblGrid = new Cell[rows*cols];
}

void Grid::DebugGrid()
{
	std::cout << std::endl;
	for(int y = 0; y < _rows; y++)
	{
		for(int x = 0; x < _cols; x++)
		{
			std::cout << " " << operator()(x,y).getType();
//			std::cout << " [" << operator()(x,y).getX() << "|" << operator()(x,y).getY() << "] ";
//			std::cout << " [" << operator()(x,y).getIndexX() << "|" << operator()(x,y).getIndexY() << "] ";
		}
		std::cout << std::endl;
	}
}

void Grid::DebugGridCoordinate()
{
	std::cout << std::endl;
	for(int y = 0; y < _rows; y++)
	{
		for(int x = 0; x < _cols; x++)
		{
			std::cout << "Col(x): " << x << " Fila(y): " << y 
			<< " === "
			<< " X: " << operator()(x,y).getX()
			<< " Y: " << operator()(x,y).getY()
			<< " Z: " << operator()(x,y).getZ() 
			<< " === "
			<< " Index X: " << operator()(x,y).getIndexX()
			<< " Index Y: " << operator()(x,y).getIndexY() << std::endl;
		}
		std::cout << std::endl;
	}
}

void Grid::DebugCalculatedPath(const std::deque<Cell*>& path)
{
	enum PATH_CELL
	{
		START_CELL	= 10,
		PATH_CELL,
		END_CELL
	};
	
	Cell* tblGrid = new Cell[_rows*_cols];
	memcpy(tblGrid, _tblGrid, sizeof(Cell) * (_rows * _cols));

	std::deque<Cell*>::const_iterator it = path.begin();
	
	for(; it != path.end() ; it++)
	{
		int x = (*it)->getIndexX();
		int y = (*it)->getIndexY();
		
		if (it == path.begin())
			tblGrid[x + y * _cols].setType(START_CELL);
		else if (it == path.end()-1)
			tblGrid[x + y * _cols].setType(END_CELL);
		else
			tblGrid[x + y * _cols].setType(PATH_CELL);
	}
	
	std::cout << std::endl;
	
	for(int y = 0; y < _rows; y++)
	{
		for(int x = 0; x < _cols; x++)
		{
			std::string strCelda;
			switch(tblGrid[x + y * _cols].getType())
			{
				case START_CELL:
					strCelda = "S";
					break;
				case PATH_CELL:
					strCelda = "*";
					break;
				case END_CELL:
					strCelda = "F";
					break;
				case EN_OPEN:
					strCelda = "0";
					break;
				case EN_BLOCK:
					strCelda = "1";
					break;
				case EN_ZOMBIE_CELL:
				case EN_PLAYER_CELL:
				default:
					break;
			}
			std::cout << strCelda;
		}
		std::cout << std::endl;
	}
	
	delete [] tblGrid;
}

void Grid::DebugCellSearch(const CellSearch& cellSearch)
{
		std::cout << " [" 			<< &cellSearch << "]"
				  << " F: " 		<< cellSearch.getF()
				  << " H: " 		<< cellSearch.getH()
				  << " G: " 		<< cellSearch.getG() 
				  << " Type: " 		<< cellSearch.getType() 
				  << " Index X: " 	<< cellSearch.getIndexX() 
				  << " Index Y: " 	<< cellSearch.getIndexY()
				  << " Parent: " 	<< cellSearch.getParent()
				  << " x: " 		<< cellSearch.getX()
				  << " y: " 		<< cellSearch.getY()
				  << " z: " 		<< cellSearch.getZ()
				  << endl;
}

void Grid::DebugCell(const Cell& cell)
{
	std::cout << " [" 			<< &cell << "]"
			  << " Type: " 		<< cell.getType()
			  << " Index X: " 	<< cell.getIndexX()
			  << " Index Y: " 	<< cell.getIndexY()
			  << " x: " 		<< cell.getX()
			  << " y: " 		<< cell.getY()
			  << " z: " 		<< cell.getZ()
			  << endl;

}

void Grid::DebugOpenList(std::deque<CellSearch*>& openList)//, char *openListMap)
{
	std::deque<CellSearch*>::iterator it = openList.begin();
	std::deque<CellSearch*>::iterator it_end = openList.end();

	std::cout << "OPEN LIST: " << openList.size() << " elems." << endl;
	//std::cout << "========================" << endl;
	for(; it != it_end; it++)
	{
		DebugCellSearch(*(*it));
	}
//	std::cout << "------------------------" << endl;
//	std::cout << "OpenListMap:" << endl;
//	std::cout << "------------------------" << endl;
//	for(int y = 0; y < _rows; y++)
//	{
//		for(int x = 0; x < _cols; x++)
//		{
//			cout << " [X: " << x << " Y: " << y << "] = " << static_cast<int>(openListMap[x + y * _cols]) << "\t";
//		}
//		cout << endl;
//	}
	//std::cout << "========================" << endl;
}

void Grid::DebugClosedListMap(char *closedListMap)
{
	std::cout << std::endl << "CLOSED LIST: COLS: " << _cols << " ROWS: " << _rows << "elems."<< endl;
	std::cout << "========================" << endl;
	for(int y = 0; y < _rows; y++)
	{
		for(int x = 0; x < _cols; x++)
		{
			cout << " [X: " << x << " Y: " << y << "] = " << static_cast<int>(closedListMap[x + y * _cols]) << "\t";
		}
		cout << endl;
	}
	std::cout << "========================" << endl;
}
CellSearch::CellSearch(CellSearch* cellParent, int G, int H) : Cell(), _cellParent(cellParent), _G(G), _H(H) {}

CellSearch::CellSearch(const Cell& cell) : Cell(cell)
{
	_cellParent = 0;
	_G			= 0;
	_H			= 0;

}

void CellSearch::setH(int endX, int endY)
{
	int distanciaX = endX - getIndexX();
	int distanciaY = endY - getIndexY();
	
	// Distancia Euclidea
	_H = sqrt(distanciaX*distanciaX + distanciaY*distanciaY);
	
	// Distancia Manhattan
	//_H = abs(distanciaX) + abs(distanciaY);
}

void Grid::addOpenList(CellSearch* actual, std::deque<CellSearch*>& openList, char *openListMap, std::vector<CellSearch*>& almacen)
{
	// NOTA!!!! Se trata de mantener ordenada la OPENLIST. Siendo el primer valor que tenga el MENOR F
	DB_ASTAR(cout << "==== METEMOS CELDA: " << actual << " ====";)
	DB_ASTAR(DebugCellSearch(*actual);)

	std::deque<CellSearch*>::iterator it = openList.begin();

	// comprobamos si es el primer elemento a añadir
	if (openList.size() > 0)
	{
		for (; it != openList.end(); it++)
		{
			if(actual->getF() <= (*it)->getF())
			{
				openList.insert(it, actual);
				break;
			}
		}

		if(it == openList.end())
			openList.push_back(actual);
//		// comprobamos si es menor o igual que el primero, sino lo añadimos por el final
//		if (actual->getF() <= openList.front()->getF())
//			openList.push_front(actual);
//		else
//			openList.push_back(actual);
	}
	else
		openList.push_back(actual);

	// guardamos en almacen
	almacen.push_back(actual);
	// marcamos en nuestro mapa que esta incluida en la lista
	openListMap[actual->getIndexX() + actual->getIndexY() * _cols] = EN_BUSY;
	
	DB_ASTAR(DebugOpenList(openList);)
	DB_ASTAR(cout << "==== FIN METEMOS CELDA: " << actual << " ====" << endl << endl;)
}

CellSearch* Grid::popTopOpenList(std::deque<CellSearch*>& openList, char *openListMap)
{
	CellSearch* actual = *(openList.begin());
DB_ASTAR(cout << "==== SACAMOS CELDA: " << actual << " ====";)
DB_ASTAR(DebugCellSearch(*actual);)

	openList.pop_front();
	openListMap[actual->getIndexX() + actual->getIndexY() * _cols] = EN_FREE;
	
DB_ASTAR(DebugOpenList(openList);)
DB_ASTAR(cout << "==== FIN SACAMOS CELDA: " << actual << " ====" << endl << endl;)
	return actual;
}

/**
 * Calcula una ruta mediante Pathfinding A* 
 *
 * @param: const Cell& start 		ENTRADA. Celda de inicio de la ruta
 * @param: const Cell& end			ENTRADA. Celda destino
 *
 * @return: std::deque<Cell*>		SALIDA. 
 *										-Si hay camino posible: el deque contendra punteros a las celdas del camino
 *										-No hay camino posible: el deque se retorna vacio.
 *
 * Note: comprobar siempre el retorno mediante size(), el cual indica el numero de elementos del deque.
 */
std::deque<Cell*> Grid::calculateAStarPath(const Cell& start, const Cell& end)
{
// * -. Añadir la celda inicial a la lista abierta
// * -. Sacar la celda inicial de la lista abierta
// * -. meter la celda inicial en la lista cerrada
// * -. calcular las celdas adyacentes que se puedan visitar
// * -. indicar en ellas que el padre es la inicial
// * -. calcular la puntuacion G de las adyacentes, añadimos 10 si es vertical/horizontal o 14 si diagonal, al camino desde la celda inicial a la actual
// * -. calcular la puntuacion H (heuristica, estimacion hasta el final, en horizontal y vertical) de las adyacentes
// * -. meter en la lista abierta las adyacentes.
// * -. calcular F de cada celda de la lista abierta (F = G + H)
// * -. coger la celda con menor puntuacion en F
// * -. sacamos la celda seleccionada de la lista abierta y lo metemos en la lista cerrada.
// * -. comprobamos los adyacentes igual que antes, comprobando tambien que no esten en la lista cerrada.
// * -. añadimos las celdas resultantes a la lista abierta si no estan ya en ella.

	std::deque<Cell*> path;

	// para guardar las celdas de la lista abierta. Aqui se guardan ordenadas siempre.
	std::deque<CellSearch*> openList;
	std::vector<CellSearch*> almacen;
	
	// mapeamos el grid para saber si la celda esta en la lista
	char* openListMap 	= new char [_rows * _cols]();
	char* closedListMap = new char [_rows * _cols]();

	CellSearch *actualSearch 	= new CellSearch(start);
	CellSearch *endSearch 		= new CellSearch(end);

	// 1-. Añadimos la celda inicial a la lista abierta
	addOpenList(actualSearch, openList, openListMap, almacen);

	do{

		// 2-. Cogemos la celda con la F menor y lo sacamos de la lista
		actualSearch = popTopOpenList(openList, openListMap);

		// 3-. Metemos la celda con la F menor en la lista cerrada
		closedListMap[actualSearch->getIndexX() + actualSearch->getIndexY() * _cols] = EN_BUSY;
//		DB_ASTAR(DebugClosedListMap(closedListMap);)

		// 4-. tratamos los adyacentes, añadimos/modificamos celdas a la lista abierta, con su correspondiente puntuacion
		Adjacents(actualSearch, endSearch, openList, openListMap, closedListMap, almacen);

	}while( (openList.size() > 0) &&			// mientras haya elemenos en la lista abierta, podremos seguir intentando el camino...
			 actualSearch != endSearch->getParent() );

	// ha podido encontrar el camino?
	if (openList.size() != 0)
		// crear el deque del path para devolverlo
		ConstructPath(path, endSearch);
	else
		// limpiamos por si acaso, para devolverlo vacio
		path.clear();

	delete [] closedListMap;
	delete [] openListMap;
	
	std::vector<CellSearch*>::iterator it_almacen = almacen.begin();
	for(;it_almacen != almacen.end(); it_almacen++)
		delete *it_almacen;
	
//	std::deque<CellSearch*>::iterator it = openList.begin();
//	for(;it != openList.end(); it++)
//		delete *it;

//	// liberamos memoria...
//	delete actualSearch;
//	delete endSearch;

	return path;
}

std::deque<CellSearch*>::iterator Grid::getCellSearchOpenList(std::deque<CellSearch*>& openList, int X, int Y)
{
	std::deque<CellSearch*>::iterator it 		= openList.begin();
	std::deque<CellSearch*>::iterator it_end 	= openList.end();

	for(;it != it_end; it++)
	{
		if ((*it)->getIndexX() == X && (*it)->getIndexY() == Y)
		{
			DB_ASTAR(cout << "ENCONTRADA! X: " << X << " Y: " << Y << endl;)
			DB_ASTAR(DebugCellSearch(*(*it));)
			break;
		}
	}

	return it;
}

void Grid::modifyCellSearchOpenList(std::deque<CellSearch*>::iterator it, std::deque<CellSearch*>& openList)
{
DB_ASTAR(cout << "Modificamos celda!" << endl;)
DB_ASTAR(DebugCellSearch(*(*it));)
DB_ASTAR(DebugOpenList(openList);)

	// recorremos el deque hacia atras, ya que el valor en G ha disminuido y F sera menor
	std::deque<CellSearch*>::iterator it_dec = it;
	for (;it_dec != openList.begin(); it_dec--)
	{
		if((*it)->getF() > (*it_dec)->getF())
		{
			CellSearch* temp = (*it);
			// borramos el objeto de su antigua posicion
			openList.erase(it);
			it_dec++;
			openList.insert(it_dec,temp);
			break;
		}
	}
	
DB_ASTAR(cout << "insertamos celda!" << endl;)
//DB_ASTAR(DebugCellSearch(*(*it));)
DB_ASTAR(DebugOpenList(openList);)
}

void Grid::Adjacents(CellSearch* actual, CellSearch* end, std::deque<CellSearch*>& openList, char *openListMap, char *closedListMap, std::vector<CellSearch*>& almacen)
{
	const int direcciones = 8;	// numero de direcciones posibles desde una celda

	// arrays para calcular los indices en el grid de las celdas de alrededor de la actual
	// Ejemplo:
	// dx = direccionX
	// dy = direccionY
	// derecha:	 | dx = 1		diagonal dcha-abajo: | dx = 1
	//		   	 | dy = 0							 | dy = 1
	//
	// abajo:  	 | dx = 0		diagonal abajo-izq:  | dx = -1
	//		   	 | dy = 1						     | dy =  1
	//
	// izquierda:| dx = -1		diagonal izq-arriba: | dx = -1
	// 			 | dy =  0							 | dy = -1
	//
	// 	arriba:	 | dx =  0		diagonal arriba-dcha:| dx =  1
	//		   	 | dy = -1							 | dy = -1
	//

	const int direccionX[direcciones]={1,  1,  0, -1, -1, -1,  0,  1};
	const int direccionY[direcciones]={0,  1,  1,  1,  0, -1, -1, -1};

	// generamos movimientos de indices a las adyacentes
	for (int i = 0; i < direcciones; i++)
	{
		// vamos estableciendo indices a las adyacentes para ir trabajando con ellas...
		int adyX = actual->getIndexX() + direccionX[i];
		int adyY = actual->getIndexY() + direccionY[i];

		if ( (adyX >= 0 && adyX < _cols) &&						// dentro de limites de X en el grid
			 (adyY >= 0 && adyY < _rows) &&						// dentro de limites de Y en el grid
			 (operator()(adyX, adyY).getType() == EN_OPEN) &&	// miramos en grid si es celda OPEN (visitable)
			 (closedListMap[adyX + adyY * _cols] == EN_FREE) )	// no esta en la lista cerrada
		{
			// si está en la lista abierta...
			if(openListMap[adyX + adyY * _cols] == EN_BUSY)
			{
				// comprobamos si la G de esa celda, es mas baja que la que estamos usando para ir alli.
				
				// Calculamos G total de la celda actual
				int g = 0;
				// si el indice de movimientos es par, es que la casilla es ortogonal (vertical u horizontal) (10) sino, es diagonal (14)
				g = (i % 2 == 0) ? 10 : 14;
				// añadimos el G calculado al G que tenia el padre
				g += actual->getG();

				// cogemos la celda adyacente de la lista abierta, para comparar su G
				std::deque<CellSearch*>::iterator it = getCellSearchOpenList(openList, adyX, adyY);

				// si el camino G de la adyacente es mayor que el G recorrido hasta ella a traves de la actual...
				if ((*it)->getG() > g)
				{
					DB_ASTAR(DebugOpenList(openList);)
					// modificamos la adyacente poniendo como padre la actual
					(*it)->setParent(actual);
					// y reasignamos la G recalculada
					(*it)->setG(g);

					// comprobamos si la F ha variado y reordenamos en ese caso el OpenList (la F inferior primero)
					modifyCellSearchOpenList(it, openList);
				}

			}
			else	// si no esta en la lista abierta...
			{
				// la creamos y haremos la celda actual su padre
				CellSearch *celda = new CellSearch(operator()(adyX, adyY));
				celda->setParent(actual);
				
				// Calculamos G
				int g = 0;
				// si el indice de movimientos es par, es que la casilla es ortogonal (vertical u horizontal) (10) sino, es diagonal (14)
				g = (i % 2 == 0) ? 10 : 14;
				// añadimos el G calculado al G que tenia el padre
				g += actual->getG();

				celda->setG(g);
				
				// calculamos H,la heuristica hasta el destino
				celda->setH(end->getIndexX(), end->getIndexY());
				
				// finalmente la añadimos
				addOpenList(celda, openList, openListMap, almacen);

				// si la celda añadida es la destino, la copiamos en la actual para saber que ha llegado al destino
				if (celda->getIndexX() == end->getIndexX() && celda->getIndexY() == end->getIndexY())
				{
					*end = *celda;
//					DB_ASTAR(DebugClosedListMap(closedListMap);)
					break;
				}
			}
		}
	}
}

void Grid::ConstructPath(std::deque<Cell*>&path, CellSearch* end)
{
	CellSearch* celdaActual = end;
	Cell* celda = &( operator()(end->getIndexX(), end->getIndexY()));
	path.push_front( celda );
	//DebugCellSearch(*end);

	while(celdaActual->getParent() != 0)
	{
		celdaActual = celdaActual->getParent();
		//DebugCellSearch(*celdaActual);
		celda = &( operator()(celdaActual->getIndexX(), celdaActual->getIndexY()));
		path.push_front( celda );

	}

//	std::deque<Cell*>::iterator it = path.begin();
//	for(;it != path.end(); it++)
//		DebugCell(*(*it));

	DB_ASTAR(DebugCalculatedPath(path);)
}

