#include "WeaponFactory.h"

Weapon * DevastatorFactory::createWeapon(Ogre::Vector3 position)
{
	// LA FABRICA HACE USO DE TODOS LOS MANAGERS NECESARIOS PARA CONSTRUIR EL OBJETO

	Ogre::SceneManager* sceneMgr = Ogre::Root::getSingleton().getSceneManager("SceneManager");

	// Se crea objeto de la clase Devastator
	Devastator* devastator = new Devastator(*(static_cast<Devastator*>(EntityManager::getSingleton().getWeaponType(EN_DEVASTATOR))));

	// Se calcula el nombre del nodo
	std::stringstream nameDevastatorNode;
	nameDevastatorNode << "devastator";

	// Se crea la entidad
	Ogre::Entity *entity = EntityManager::getSingleton().createEntity(	nameDevastatorNode.str(),
																		EntityManager::getSingleton().getWeaponType(EN_DEVASTATOR)->getMesh() );
	entity->setCastShadows(false);
	entity->setVisibilityFlags(NOT_SHOWN_IN_MINIMAP);

	// Se crea el nodo de escena
	devastator->setNode(sceneMgr->createSceneNode(nameDevastatorNode.str()));
	devastator->getNode()->attachObject(entity);

	// creamos nodo de proyectil, relativo al arma
	devastator->setNodeProjectile(EntityManager::getSingleton().createSceneNode("projectileNode"));
	devastator->getNode()->addChild(devastator->getNodeProjectile());
	devastator->getNodeProjectile()->setOrientation(Ogre::Quaternion::IDENTITY);

	// Se coloca el devastador en el escenario antes de meter la fisica
	devastator->setPosition(position);

	// Se añaden a la clase las animaciones
	devastator->setShootAnimation(sceneMgr->getEntity(nameDevastatorNode.str())->getAnimationState("retroceso"));
	devastator->setWalkAnimation(sceneMgr->getEntity(nameDevastatorNode.str())->getAnimationState("caminar"));
	devastator->setDownAnimation(sceneMgr->getEntity(nameDevastatorNode.str())->getAnimationState("bajar"));


	// Se registra en el manager de las entidades
	EntityManager::getSingleton().addWeapon(devastator);

	// Se devuelve
	return devastator;
}



Weapon * MachineGunFactory::createWeapon(Ogre::Vector3 position)
{
	// LA FABRICA HACE USO DE TODOS LOS MANAGERS NECESARIOS PARA CONSTRUIR EL OBJETO

	Ogre::SceneManager* sceneMgr = Ogre::Root::getSingleton().getSceneManager("SceneManager");

	// Se crea objeto de la clase MachineGun
	MachineGun* machineGun = new MachineGun(*(static_cast<MachineGun*>(EntityManager::getSingleton().getWeaponType(EN_MACHINEGUN))));

	// Se calcula el nombre del nodo
	std::stringstream nameMachineGunNode;
	nameMachineGunNode << "machineGun";

	// Se crea la entidad
	Ogre::Entity *entity = EntityManager::getSingleton().createEntity(	nameMachineGunNode.str(),
																		EntityManager::getSingleton().getWeaponType(EN_MACHINEGUN)->getMesh() );
	entity->setCastShadows(false);
	entity->setVisibilityFlags(NOT_SHOWN_IN_MINIMAP);

	// Se crea el nodo de escena
	machineGun->setNode(sceneMgr->createSceneNode(nameMachineGunNode.str()));
	machineGun->getNode()->attachObject(entity);

	// Se coloca la metralleta en el escenario antes de meter la fisica
	machineGun->setPosition(position);

	// Se añaden a la clase las animaciones
	machineGun->setShootAnimation(sceneMgr->getEntity(nameMachineGunNode.str())->getAnimationState("retroceso"));
	machineGun->setWalkAnimation(sceneMgr->getEntity(nameMachineGunNode.str())->getAnimationState("caminar"));
	machineGun->setDownAnimation(sceneMgr->getEntity(nameMachineGunNode.str())->getAnimationState("bajar"));


	// Se registra en el manager de las entidades
	EntityManager::getSingleton().addWeapon(machineGun);

	// Se devuelve
	return machineGun;
}
