#include "CharacterFactory.h"


CharacterFactory::~CharacterFactory() {}

Character* PlayerFactory::createCharacter(Ogre::Vector3 position)
{

	Player* playerType = EntityManager::getSingleton().getPlayerType();

	// Se crea la entidad
	Ogre::SceneNode* scnNode = EntityManager::getSingleton().createSceneNode("player");
	Ogre::Entity *entity = EntityManager::getSingleton().createEntity("player", playerType->getMesh() );
	entity->setVisibilityFlags(NOT_SHOWN_IN_MINIMAP);
	entity->setCastShadows(false);
	scnNode->attachObject(entity);

	EntityManager::getSingleton().getBaseNode()->addChild(scnNode);

	scnNode->setPosition(position);
	scnNode->setOrientation(playerType->getInitialOrientation());

	Player* player = new Player(scnNode, playerType->getSpeed(),
								playerType->getMesh(),
								playerType->getSpeedPitch());

	// ====== creamos inventario de armas
	std::vector<EN_WEAPON_TYPE> weapons = EntityManager::getSingleton().getPlayerType()->getInitialWeapons();
	std::vector<EN_WEAPON_TYPE>::iterator it_weapons = weapons.begin();

	for(int i = 0; it_weapons != weapons.end(); it_weapons++, i++)
	{
		switch(*it_weapons)
		{
			case EN_NONE:
				break;
			case EN_DEVASTATOR:
			{
				DevastatorFactory devastatorFactory;
				Weapon* devastator = devastatorFactory.createWeapon();

				if (i == 0)	// si es el primer arma, sera el activo
					player->setWeapon(devastator);

				player->addWeapon(devastator);
				break;
			}
			case EN_MACHINEGUN:
			{
				MachineGunFactory machineGunFactory;
				Weapon* machineGun = machineGunFactory.createWeapon();

				if (i == 0)	// si es el primer arma, sera el activo
					player->setWeapon(machineGun);

				player->addWeapon(machineGun);
				break;
			}
			case EN_GUN:
			case EN_SPECIAL_WEAPON:
				break;
		}
	}

	OgreBulletDynamics::RigidBody *rigidBody = PhysicsManager::getSingleton().createRigidBody(entity,
									"PLAYER",position,playerType->getInitialOrientation());

	player->setRigidBody(rigidBody);

	// ====== guardamos el player en el entity manager
	EntityManager::getSingleton().setPlayer(player);

	return player;
}

Character* ZombieFactory::createCharacter(Ogre::Vector3 position)
{
	// LA FABRICA HACE USO DE TODOS LOS MANAGERS NECESARIOS PARA CONSTRUIR EL OBJETO

	Ogre::SceneManager* sceneMgr = Ogre::Root::getSingleton().getSceneManager("SceneManager");

	// Se crea objeto de la clase Zombie

	Zombie* zombie = new Zombie(EntityManager::getSingleton().getZombieType());

	// Se calcula el nombre del nodo
	std::stringstream nameZombieNode;
	nameZombieNode << "zombie_" << EntityManager::getSingleton().getInstanceNumber();

	// Se crea la entidad
	Ogre::Entity *entity = EntityManager::getSingleton().createEntity(nameZombieNode.str(), "zombie1.mesh");
	//Ogre::Entity *entity = sceneMgr->createEntity(nameZombieNode.str(), "zombie1.mesh"); //_scene->getZombieType().getMesh());

	// Se crea el nodo de escena
	zombie->setNode(sceneMgr->createSceneNode(nameZombieNode.str()));
	zombie->getNode()->attachObject(entity);
	zombie->getNode()->setOrientation(Ogre::Quaternion::IDENTITY);

	// Se coloca el zombie en el escenario antes de meter la fisica
	zombie->setPosition(position);

	// establecemos coordenadas en el grid
	zombie->CoordsToCellIndex();

	// ===================== Se añaden a la clase las animaciones
	zombie->setWalkingAnimation(sceneMgr->getEntity(nameZombieNode.str())->getAnimationState("walk"));
	zombie->setDeadAnimation(sceneMgr->getEntity(nameZombieNode.str())->getAnimationState("dead"));
	zombie->setDeadVaporizeAnimation(sceneMgr->getEntity(nameZombieNode.str())->getAnimationState("deadVaporize"));

	zombie->getWalkingAnimation()->setEnabled(true);
	zombie->getWalkingAnimation()->setLoop(true);

	// calculamos una posicion aleatoria de inicio de la animacion de andar, para que no vayan todos al mismo paso
	int timePosition = MyUtils::getSingleton().rangeRandomNumber(0, (int)zombie->getWalkingAnimation()->getLength());
	zombie->getWalkingAnimation()->setTimePosition(timePosition);

	zombie->getDeadAnimation()->setEnabled(false);
	zombie->getDeadAnimation()->setLoop(false);
	zombie->getDeadVaporizeAnimation()->setEnabled(false);
	zombie->getDeadVaporizeAnimation()->setLoop(false);

	zombie->setState(EN_CHASE);

	zombie->seekTarget();

	// Lo adjuntamos al nodo base
	EntityManager::getSingleton().getBaseNode()->addChild(zombie->getNode());

	// ======================= Se crea la física
	std::string	type	= "ZOMBIE1";
	std::string id		= nameZombieNode.str();

	OgreBulletDynamics::RigidBody *rigidBody = PhysicsManager::getSingleton().createRigidBody(entity,
											type,
											Ogre::Vector3(zombie->getNode()->getPosition().x,
										   zombie->getNode()->getPosition().y,
										   zombie->getNode()->getPosition().z) /* Posicion inicial */,
										   Ogre::Quaternion::IDENTITY);

	zombie->setRigidBody(rigidBody);

	// ====== guardamos el personaje en el entity manager
	EntityManager::getSingleton().addZombie(zombie);

	// Se devuelve
	return zombie;
}

Character* HumanFactory::createCharacter(Ogre::Vector3 position)
{
	// LA FABRICA HACE USO DE TODOS LOS MANAGERS NECESARIOS PARA CONSTRUIR EL OBJETO

	Ogre::SceneManager* sceneMgr = Ogre::Root::getSingleton().getSceneManager("SceneManager");

	// Se crea objeto de la clase Human

	Human* human = new Human(EntityManager::getSingleton().getHumanType());

	// Se calcula el nombre del nodo
	std::stringstream nameHumanNode;
	nameHumanNode << "human_" << EntityManager::getSingleton().getInstanceNumber();

	// Se crea la entidad
	Ogre::Entity *entity = EntityManager::getSingleton().createEntity(nameHumanNode.str(), "human1.mesh");
	//Ogre::Entity *entity = sceneMgr->createEntity(nameZombieNode.str(), "zombie1.mesh"); //_scene->getZombieType().getMesh());

	// Se crea el nodo de escena
	human->setNode(sceneMgr->createSceneNode(nameHumanNode.str()));
	human->getNode()->attachObject(entity);
	human->getNode()->setOrientation(Ogre::Quaternion::IDENTITY);

	// Se coloca el zombie en el escenario antes de meter la fisica
	human->setPosition(position);

	// establecemos coordenadas en el grid
	human->CoordsToCellIndex();

	// ===================== Se añaden a la clase las animaciones
	human->setWalkingAnimation(sceneMgr->getEntity(nameHumanNode.str())->getAnimationState("walk"));
	human->setDeadAnimation(sceneMgr->getEntity(nameHumanNode.str())->getAnimationState("dead"));

	human->getWalkingAnimation()->setEnabled(true);
	human->getWalkingAnimation()->setLoop(true);

	// calculamos una posicion aleatoria de inicio de la animacion de andar, para que no vayan todos al mismo paso
	int timePosition = MyUtils::getSingleton().rangeRandomNumber(0, (int)human->getWalkingAnimation()->getLength());
	human->getWalkingAnimation()->setTimePosition(timePosition);

	human->getDeadAnimation()->setEnabled(false);
	human->getDeadAnimation()->setLoop(false);

	human->setState(EN_RUNAWAY);

	human->seekTarget();

	// Lo adjuntamos al nodo base
	EntityManager::getSingleton().getBaseNode()->addChild(human->getNode());

	// ======================= Se crea la física
	std::string	type	= "HUMAN1";
	std::string id		= nameHumanNode.str();

	OgreBulletDynamics::RigidBody *rigidBody = PhysicsManager::getSingleton().createRigidBody(entity,
											type,
											Ogre::Vector3(human->getNode()->getPosition().x,
											human->getNode()->getPosition().y,
											human->getNode()->getPosition().z) /* Posicion inicial */,
										   Ogre::Quaternion::IDENTITY);

	human->setRigidBody(rigidBody);

	// ====== guardamos el personaje en el entity manager
	EntityManager::getSingleton().addHuman(human);


	// Se devuelve
	return human;
}
