#include "MissionsManager.h"

template<> MissionsManager* Ogre::Singleton<MissionsManager>::msSingleton = 0;

MissionsManager* MissionsManager::getSingletonPtr ()
{
	return msSingleton;
}

MissionsManager& MissionsManager::getSingleton ()
{
	assert(msSingleton);
	return *msSingleton;
}


void MissionsManager::addMission(Mission *mission)
{
	_missions.push_back(mission);
}

std::vector<Mission *> MissionsManager::getMissions() const
{
	return _missions;
}

void MissionsManager::destroyAll()
{
	destroyAllMissions();
}

void MissionsManager::destroyAllMissions()
{
	std::vector<Mission*>::iterator it;
	it = _missions.begin();
	for(; it != _missions.end(); it++)
		delete *it;

	_missions.clear();
}
