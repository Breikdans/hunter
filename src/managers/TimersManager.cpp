/*
 * PathFindingEnemyTimer.cpp
 *
 *  Created on: 07/02/2016
 *      Author: jose
 */
#include <CEGUI.h>
#include <TimersManager.h>
#include "PlayState.h"
#include "MachineGun.h"

// ===========================================================================================================================================
//		PATHFINDINGENEMYTIMER (comprobara cada cierto tiempo si el jugador se ha movido y calculara la ruta desde el enemigo hasta el)
// ===========================================================================================================================================
PathFindingEnemyTimer::PathFindingEnemyTimer()
{
	_running=false;
	_runningCustom = 0;
}

PathFindingEnemyTimer::~PathFindingEnemyTimer() {}

void PathFindingEnemyTimer::run()
{
	while(true)
	{
		IceUtil::ThreadControl::sleep( IceUtil::Time::milliSeconds(1000));
		if (Character::getMove()==true)
		{
			if(_runningCustom==1)
			{
				setZombieTargets();
				execTimer();
			}
		}
	}
}

void PathFindingEnemyTimer::setZombieTargets()
{
	std::vector<Character*>& zombies = EntityManager::getSingleton().getZombies();
	std::vector<Character*>::iterator it = zombies.begin();

	for (; it != zombies.end(); it++)
	{
		if((*it)->getState() == EN_CHASE || (*it)->getState() == EN_ATTACK)
			(*it)->seekTarget();
	}
}

void PathFindingEnemyTimer::execTimer()
{
	// si el objetivo ha cambiado de celda en el grid, recalculamos PATH
	std::vector<Character*> zombies = EntityManager::getSingleton().getZombies();
	std::vector<Character*>::iterator it_zombies = zombies.begin();

	for(;it_zombies != zombies.end() && _runningCustom==1; it_zombies++)
	{
		if ((*it_zombies)->getState() == EN_CHASE)
		{
			// si el objetivo ha cambiado de celda en el grid, recalculamos PATH
//			if( (*it_zombies)->getTarget()->getCellX() != (*it_zombies)->getOldTargetX() ||
//				(*it_zombies)->getTarget()->getCellY() != (*it_zombies)->getOldTargetY() )
//			{
				// cogemos la celda del zombie y la del target para calcular el camino
				Cell zombieCell = PlayState::getSingleton().getScene()->grid((*it_zombies)->getCellX(), (*it_zombies)->getCellY());
				Cell TargetCell = PlayState::getSingleton().getScene()->grid((*it_zombies)->getTarget()->getCellX(),
																			 (*it_zombies)->getTarget()->getCellY());

				// actualizamos las celdas del target
				(*it_zombies)->setOldTargetX((*it_zombies)->getTarget()->getCellX());
				(*it_zombies)->setOldTargetY((*it_zombies)->getTarget()->getCellY());

				// recalculamos camino
				std::deque<Cell*> path = PlayState::getSingleton().getScene()->calculateAStarPath(zombieCell, TargetCell);
				(*it_zombies)->setPath(path);
			//}
		}
	}
}

void PathFindingEnemyTimer::stopTimer()
{
	_running=false;
	_runningCustom = 0;
}

void PathFindingEnemyTimer::runTimer()
{
	_running=true;
	_runningCustom = 1;
}

// ===========================================================================================================================================
//		PATHFINDINGHUMANTIMER (comprobara cada cierto tiempo si el jugador se ha movido y calculara la ruta desde el enemigo hasta el)
// ===========================================================================================================================================
PathFindingHumanTimer::PathFindingHumanTimer()
{
	_running=false;
	_runningCustom = 0;
}

PathFindingHumanTimer::~PathFindingHumanTimer() {}

void PathFindingHumanTimer::run()
{
	while(true)
	{
		IceUtil::ThreadControl::sleep( IceUtil::Time::milliSeconds(1000));
		if (Character::getMove()==true)
		{
			if(_runningCustom==1)
				execTimer();
		}
	}
}


void PathFindingHumanTimer::execTimer()
{
	// si el jugador ha cambiado de celda en el grid, recalculamos PATH del human
	if(EntityManager::getSingleton().getPlayer()->getCellX() != _oldPlayerX ||
	   EntityManager::getSingleton().getPlayer()->getCellY() != _oldPlayerY)
	{
		std::vector<Character*>& humans = EntityManager::getSingleton().getHumans();
		std::vector<Character*>::iterator it_humans = humans.begin();

		for(;it_humans != humans.end() && _runningCustom==1; it_humans++)
		{
			Cell humanCell = PlayState::getSingleton().getScene()->grid((*it_humans)->getCellX(), (*it_humans)->getCellY());
			Cell playerCell = PlayState::getSingleton().getScene()->grid(EntityManager::getSingleton().getPlayer()->getCellX(),
																		 EntityManager::getSingleton().getPlayer()->getCellY());

			std::deque<Cell*> path = PlayState::getSingleton().getScene()->calculateAStarPath(humanCell, playerCell);
			(*it_humans)->setPath(path);
		}

		_oldPlayerX = EntityManager::getSingleton().getPlayer()->getCellX();
		_oldPlayerY = EntityManager::getSingleton().getPlayer()->getCellY();
	}
}



void PathFindingHumanTimer::setOldPlayerX(int x)
{
	_oldPlayerX = x;
}

void PathFindingHumanTimer::setOldPlayerY(int y)
{
	_oldPlayerY = y;
}


void PathFindingHumanTimer::stopTimer()
{
	_running=false;
	_runningCustom = 0;
}

void PathFindingHumanTimer::runTimer()
{
	_running=true;
	_runningCustom = 1;
}


// =============================================================================================
//		MACHINEGUNSHOOTINGTIMER (realizará disparos de metralleta mientras tengamos el boton apretado)
// =============================================================================================
MachineGunShootingTimer::MachineGunShootingTimer()
{
	_running=false;
	_runningCustom = 0;
}

MachineGunShootingTimer::~MachineGunShootingTimer() {}

void MachineGunShootingTimer::run()
{
	while(true)
	{
		IceUtil::ThreadControl::sleep( IceUtil::Time::milliSeconds(200));
		if(_runningCustom==1)
			execTimer();
	}
}

void MachineGunShootingTimer::execTimer()
{
	stopTimer();
	PlayState::getSingleton().setStartLevel(true);
}

void MachineGunShootingTimer::stopTimer()
{
	_running=false;
	_runningCustom = 0;
//	cout << "--------PARAMOS TIMER" << endl;
}

void MachineGunShootingTimer::runTimer()
{
	_running=true;
	_runningCustom = 1;
//	cout << "--------INICIAMOS TIMER" << endl;
}

// =============================================================================================
//		MESSAGEINITLEVELTIMER (marcará el inicio de la desaparicion del cartel de nivel)
// =============================================================================================
MessageInitLevelTimer::MessageInitLevelTimer()
{
	_running=false;
	_runningCustom = 0;
}

MessageInitLevelTimer::~MessageInitLevelTimer() {}

void MessageInitLevelTimer::run()
{
	while(true) 
	{
		IceUtil::ThreadControl::sleep( IceUtil::Time::milliSeconds(2000));
		if(_runningCustom==1)
			execTimer();
	}
}

void MessageInitLevelTimer::execTimer()
{
	stopTimer();
	PlayState::getSingleton().setStartLevel(true);
}

void MessageInitLevelTimer::stopTimer()
{
	_running=false;
	_runningCustom = 0;
//	cout << "--------PARAMOS TIMER" << endl;
}

void MessageInitLevelTimer::runTimer()
{
	_running=true;
	_runningCustom = 1;
//	cout << "--------INICIAMOS TIMER" << endl;
}




