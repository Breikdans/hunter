#include "PhysicsManager.h"



template<> PhysicsManager* Ogre::Singleton<PhysicsManager>::msSingleton = 0;

PhysicsManager::PhysicsManager() 
{
	_world 			= 0;
	_debugDrawer		= 0;
	_lastObjACollision	= 0;
	_lastObjBCollision	= 0;
}

PhysicsManager::~PhysicsManager()
{
	destroyAll();
}


void PhysicsManager::CreateWorld() 
{
	Ogre::SceneManager* sceneMgr = Ogre::Root::getSingletonPtr()->getSceneManager("SceneManager");

		// Creacion del mundo (definicion de los limites y la gravedad) ---
		Ogre::AxisAlignedBox worldBounds = Ogre::AxisAlignedBox (
		Ogre::Vector3 (-10000, -10000, -10000),
		Ogre::Vector3 (10000,  10000,  10000));
		Ogre::Vector3 gravity = Ogre::Vector3(0, -9.8, 0);

		setWorld(new OgreBulletDynamics::DynamicsWorld(
				sceneMgr,
				worldBounds,
				gravity));

#ifdef DEBUG_PHYSICS
		// Creacion del modulo de debug visual de Bullet ------------------
		getWorld()->setDebugDrawer(new OgreBulletCollisions::DebugDrawer());
		getWorld()->getDebugDrawer()->setDrawWireframe(true);
		Ogre::SceneNode *node = EntityManager::getSingleton().getBaseNode()->createChildSceneNode("debugNode", Ogre::Vector3::ZERO);
		node->attachObject(static_cast <Ogre::SimpleRenderable *>(getWorld()->getDebugDrawer()));
		getWorld()->setShowDebugShapes (true);  // Muestra los collision shapes
#endif
}

bool PhysicsManager::isEnoughProjectileForce(const btCollisionObject* btObj)
{
	const float MINIMAL_KILLING_FORCE = 20.0f;
	const btRigidBody* btRgBody = static_cast<const btRigidBody *>(btObj);
	bool collision 		= false;
	btVector3 velocity 	= btRgBody->getLinearVelocity();
	btScalar speed 		= velocity.length();

	if (speed > MINIMAL_KILLING_FORCE)
	{
		std::cout << "SPEED HIT!!: " << speed << std::endl;
		collision = true;
	}

	return collision;
}

int PhysicsManager::DetectCollision()
{
	// cogemos un puntero al mundo dinamico de bullet...
	OgreBulletDynamics::DynamicsWorld* world 	= getWorld();

	// cogemos un puntero al mundo de colisiones fisicas de bullet...
	btCollisionWorld *bulletWorld 				= world->getBulletCollisionWorld();

	// recogemos el numero de contenedores(manifolds) de objetos detectados en colision. los manifolds son contenedores que tienen pares de objetos colisionados
	int numManifolds							= bulletWorld->getDispatcher()->getNumManifolds();

	for (int i=0 ; i < numManifolds ; i++)
	{
		// cogemos manifold
		btPersistentManifold* contactManifold =	bulletWorld->getDispatcher()->getManifoldByIndexInternal(i);

		// recogemos los 2 objetos colisionantes
		const btCollisionObject* obA = contactManifold->getBody0();
		const btCollisionObject* obB = contactManifold->getBody1();

		void *obAUserPtr = obA->getUserPointer();
		void *obBUserPtr = obB->getUserPointer();

		if (obAUserPtr) {
			_lastObjACollision = static_cast<Ogre::Entity*>(obAUserPtr);
		}

		if (obBUserPtr) {
			_lastObjBCollision = static_cast<Ogre::Entity*>(obBUserPtr);
		}

		// Buscamos zombie, projectile
		if (isPairOf("zombie","player")) {
			return COLTYPE_ZOMBIE_PLAYER;
		}
		else if (isPairOf("player","zombie"))
		{
			return COLTYPE_PLAYER_ZOMBIE;
		}
		else if (isPairOf("zombie","projectile"))
		{
			if(isEnoughProjectileForce(obB))
				return COLTYPE_ZOMBIE_PROJECTILE;
		}
		else if (isPairOf("projectile","zombie"))
		{
			if(isEnoughProjectileForce(obA))
				return COLTYPE_PROJECTILE_ZOMBIE;
		}
		else if (isPairOf("human","projectile"))
		{
			if(isEnoughProjectileForce(obB))
				return COLTYPE_HUMAN_PROJECTILE;
		}
		else if (isPairOf("projectile","human"))
		{
			if(isEnoughProjectileForce(obA))
				return COLTYPE_PROJECTILE_HUMAN;
		}
		else if (isPairOf("zombie","human"))
		{
			return COLTYPE_ZOMBIE_HUMAN;
		}
		else if (isPairOf("human","zombie"))
		{
			return COLTYPE_HUMAN_ZOMBIE;
		}
	}
	return COLTYPE_NO_COLLISION;

}

bool PhysicsManager::isPairOf(const std::string txtObjA, const std::string txtObjB)
{

	std::size_t foundA = _lastObjACollision->getName().find(txtObjA,0);
	if (foundA!=std::string::npos)
	{
		std::size_t foundB = _lastObjBCollision->getName().find(txtObjB,0);
		if (foundB!=std::string::npos)
		{
			return true;
		}
	}

	return false;
}

OgreBulletDynamics::RigidBody* PhysicsManager::createRigidBody(Ogre::Entity* entity,
											  std::string type,
											  const Ogre::Vector3&	initialPosition,
											  const Ogre::Quaternion& initialOrientation)
{
	OgreBulletDynamics::RigidBody *rigidBody = 0;
	OgreBulletCollisions::CollisionShape *shape = 0;

	float restitution = 0;
	float friction = 0;
	float mass = 0;
	int collisionGroup = COL_NONE;
	int setAngularFactor=false;


	// Establecer parámetros de los objetos físicos
	if (type=="PLAYER")
	{
		shape = getBoxCollisionShape(entity, type);
		restitution = 1; friction = 10; mass = 70;		collisionGroup = COL_PLAYER; setAngularFactor = true;
	}
	if (type=="HUMAN1")
	{
		shape = getConvexHullShape(entity, type);
		restitution = 0.6; friction = 0.6; mass = 10;	collisionGroup = COL_ZOMBIE; setAngularFactor = true;
	}
	else if (type=="BOX1")
	{
		shape = getBoxCollisionShape(entity, type);
		restitution = 0.6; friction = 0.6; mass = 0.5;	collisionGroup = COL_OBJECT;
	}
	else if(type=="FENCEWOOD1")
	{
		shape = getStaticMeshShape(entity, type);
		restitution = 0.6; friction = 0.6; mass = 0;	collisionGroup = COL_OBJECT;
	}
	else if (type=="WAGENRAEDER1")
	{
		shape = getConvexHullShape(entity,type);
		restitution = 0.6; friction = 0.6; mass = 20;	collisionGroup = COL_OBJECT;
	}
	else if(type=="HOUSE1" || type=="HOUSE2" || type=="HOUSE3")
	{
		shape = getStaticMeshShape(entity, type);
		restitution = 0.6; friction = 0.6; mass = 0;	collisionGroup = COL_STATIC;
	}
	else if (type=="TERRAIN")
	{
		shape = getStaticMeshShape(entity, type);
		restitution = 0.1; friction = 1.0; mass = 0;	collisionGroup = COL_STATIC;
	}
	else if (type=="ZOMBIE1")
	{
		shape = getConvexHullShape(entity, type);
		restitution = 0.6; friction = 0.6; mass = 10;	collisionGroup = COL_ZOMBIE; setAngularFactor = true;
	}
	else if (type=="PROJECTILE")
	{
		shape = getSphereShape(entity, type);
		restitution = 1; friction = 1; mass = 2;		collisionGroup = COL_PROJECTILE;
	}

	// Crear el cuerpo rígido
	rigidBody = new OgreBulletDynamics::RigidBody(entity->getName(), getWorld(), collisionGroup, COL_ALL);
	if (mass==0)
	{
		rigidBody->setStaticShape(entity->getParentSceneNode(), shape,
									restitution /* Restitucion */,
									friction /* Friccion */,
									initialPosition /* Posicion inicial */,
									initialOrientation /* Orientacion */);
	}
	else
	{
		rigidBody->setShape(entity->getParentSceneNode(), shape,
									restitution,
									friction,
									mass,
									initialPosition /* Posicion inicial */,
									initialOrientation /* Orientacion */);
	}

	// Establecer siempre vertical si procede
	if (setAngularFactor==true)
	{
		rigidBody->getBulletRigidBody()->setAngularFactor(0.0);
		rigidBody->getBulletRigidBody()->setSleepingThresholds(0.0,0.0);
		rigidBody->getBulletRigidBody()->setActivationState( DISABLE_DEACTIVATION );
	}

	rigidBody->getBulletRigidBody()->setUserPointer(entity);
	
	return rigidBody;
}

OgreBulletCollisions::BoxCollisionShape *PhysicsManager::getBoxCollisionShape(Ogre::Entity* entity, std::string type) {

	OgreBulletCollisions::BoxCollisionShape *shape = static_cast<OgreBulletCollisions::BoxCollisionShape *>(getCollisionShape(type));

	if (shape == 0)
	{
		Ogre::Vector3 size = Ogre::Vector3::ZERO;	// size of the box

		// Obtenemos la bounding box de la entidad creada... ------------
		Ogre::AxisAlignedBox boundingB = entity->getBoundingBox();
		size = boundingB.getSize();
		size /= 2.0f;   // El tamano en Bullet se indica desde el centro

		// after that create the Bullet shape with the calculated size
		shape = new OgreBulletCollisions::BoxCollisionShape(size);
		addCollisionShape(type, shape);

	}
	return shape;

}

OgreBulletCollisions::TriangleMeshCollisionShape *PhysicsManager::getStaticMeshShape(Ogre::Entity *entity, std::string type) {

	OgreBulletCollisions::TriangleMeshCollisionShape *shape = static_cast<OgreBulletCollisions::TriangleMeshCollisionShape*>(getCollisionShape(type));
	if (shape == 0)
	{
		shape = OgreBulletCollisions::StaticMeshToShapeConverter(entity).createTrimesh();
		addCollisionShape(type, shape);
	}
	return shape;
}


OgreBulletCollisions::ConvexHullCollisionShape *PhysicsManager::getConvexHullShape(Ogre::Entity *entity, std::string type) {

	OgreBulletCollisions::ConvexHullCollisionShape *shape = static_cast<OgreBulletCollisions::ConvexHullCollisionShape*>(getCollisionShape(type));
	if (shape == 0)
	{
		shape = OgreBulletCollisions::StaticMeshToShapeConverter(entity).createConvex();
		addCollisionShape(type, shape);
	}
	return shape;
}

OgreBulletCollisions::SphereCollisionShape *PhysicsManager::getSphereShape(Ogre::Entity *entity, std::string type) {
	OgreBulletCollisions::SphereCollisionShape *shape =	static_cast<OgreBulletCollisions::SphereCollisionShape*>(getCollisionShape(type));
	if (shape == 0)
	{
		 shape = OgreBulletCollisions::StaticMeshToShapeConverter(entity).createSphere();
		 addCollisionShape(type, shape);
	}
	return shape;
}

/**
 * Dado un nodo y el atributo que queremos recoger, nos devuelve su valor en un string
 *
 * @param: Ogre::Vector3 &v,							///< SALIDA. Primer punto de colision detectado
 * @param: Ogre::Vector3 &vEnd,							///< SALIDA. Punto final del rayo lanzado
 * @param: OgreBulletDynamics::DynamicsWorld* world,
 * @param: float xOrign,								///< ENTRADA. Posicion en X de partida del rayo (valores de 0 a 1)
 * @param: float yOrigin,								///< ENTRADA. Posicion en Y de partida del rayo (valores de 0 a 1)
 * @param: Ogre::Camera *camera *
 *
 * @return: Ogre::Vector3			SALIDA. v = posicion del primer punto de colision detectado. v = 0 si no detecta colision
 */
void PhysicsManager::buildOgreBulletRayFromCamera(	Ogre::Vector3 &v,							///< SALIDA. Primer punto de colision detectado
													Ogre::Vector3 &vEnd,						///< SALIDA. Punto final del rayo lanzado
													OgreBulletDynamics::DynamicsWorld* world,
													float xOrign,								///< ENTRADA. Posicion en X de partida del rayo (valores de 0 a 1)
													float yOrigin,								///< ENTRADA. Posicion en Y de partida del rayo (valores de 0 a 1)
													Ogre::Camera *camera )
{
	Ogre::Ray ray;
	ray = camera->getCameraToViewportRay (xOrign, yOrigin);
	OgreBulletCollisions::CollisionClosestRayResultCallback cQuery =
			OgreBulletCollisions::CollisionClosestRayResultCallback (ray, world, 10000);
	world->launchRay(cQuery);
	vEnd = cQuery.getRayEndPoint();
	if (cQuery.doesCollide())
	{
	   v = cQuery.getCollisionPoint();
	}
	else
	{
		v = Ogre::Vector3::ZERO;
	}
}

void PhysicsManager::buildOgreBulletRayFromCamera(	std::string &nameObject,		// salida del objeto colisionado
													OgreBulletDynamics::DynamicsWorld* world,
													float xOrign,								///< ENTRADA. Posicion en X de partida del rayo (valores de 0 a 1)
													float yOrigin,								///< ENTRADA. Posicion en Y de partida del rayo (valores de 0 a 1)
													Ogre::Camera *camera )
{
	btCollisionObject* btObject = 0;
	Ogre::Ray ray;
	ray = camera->getCameraToViewportRay (xOrign, yOrigin);
	OgreBulletCollisions::CollisionClosestRayResultCallback cQuery =
			OgreBulletCollisions::CollisionClosestRayResultCallback (ray, world, 10000);
	world->launchRay(cQuery);
	if (cQuery.doesCollide())
	{
	   btObject = cQuery.getCollidedObject()->getBulletObject();
	   nameObject = cQuery.getCollidedObject()->getName();
	}
	else
	{
		nameObject = "";
	}
}

/**
 * Dado un par <string, CollisionShape*>  (key, valor)  si no se encuentra en el mapa se añade.
 *
 * @param: std::string key 		ENTRADA. Key a buscar en el mapa. Ej: "BOX"
 * @param: CollisionShape*		ENTRADA. Puntero al collisionShape creado
 *
 * @return: void
 */
void PhysicsManager::addCollisionShape(std::string key, OgreBulletCollisions::CollisionShape* collisionShape)
{
	_shapes.insert(std::pair<std::string, OgreBulletCollisions::CollisionShape*>(key, collisionShape));
}

/**
 * Dada una key para buscar en el mapa su valor asociado, nos retorna el valor (en este caso un puntero) o NULL si no lo ha encontrado
 *
 * @param: std::string key 		ENTRADA. Key a buscar en el mapa. Ej: "BOX"
 *
 * @return: CollisionShape*		SALIDA. puntero al collisionShape asociado o NULL si no se ha asignado aun ningun par de este tipo
 */
OgreBulletCollisions::CollisionShape* PhysicsManager::getCollisionShape(std::string key)
{
	OgreBulletCollisions::CollisionShape* retColShape = 0;

	std::map<std::string, OgreBulletCollisions::CollisionShape*>::iterator it;
	it = _shapes.find(key);

	// si no lo ha encontrado, retorna un puntero a end
	if(it != _shapes.end())
	{
		retColShape = it->second;
	}

	return retColShape;
}


OgreBulletDynamics::DynamicsWorld* PhysicsManager::getWorld() {
	return _world;
}

void PhysicsManager::setWorld(OgreBulletDynamics::DynamicsWorld* world) {
	_world = world;
}

Ogre::Entity* PhysicsManager::getLastObjACollision()
{
	return _lastObjACollision;
}

Ogre::Entity* PhysicsManager::getLastObjBCollision()
{
	return _lastObjBCollision;
}

void PhysicsManager::destroyAll()
{
	destroyAllShapes();

	_lastObjACollision = 0;
	_lastObjBCollision = 0;

	if (_world != 0)
	{
#ifdef DEBUG_PHYSICS
		if (_world->getDebugDrawer() != 0)
		{
			delete _world->getDebugDrawer();
			_debugDrawer = 0;
		}
#endif
		delete _world;
		_world = 0;
	}
}

void PhysicsManager::destroyAllShapes()
{
	std::map<std::string, OgreBulletCollisions::CollisionShape *>::iterator it;

	it = _shapes.begin();
	for(; it != _shapes.end(); it++)
		delete it->second;

	_shapes.clear();
}
