#include "EntityManager.h"


template<> EntityManager* Ogre::Singleton<EntityManager>::msSingleton = 0;

EntityManager::EntityManager()
{
	_zombieType = 0;
	_elements.reserve(100);
	_baseNode 	= 0;
	_player = 0;
	_instanceNumber = 0;

}

EntityManager::~EntityManager()
{
	destroyAll();
}

Ogre::Entity* EntityManager::createEntity(const std::string name, const std::string mesh) {
	Ogre::Entity* entity =
			Ogre::Root::getSingleton().getSceneManager("SceneManager")->createEntity(name,mesh);
	return entity;
}

Ogre::SceneNode* EntityManager::createSceneNode(std::string name) {
	Ogre::SceneNode* node =
				Ogre::Root::getSingleton().getSceneManager("SceneManager")->createSceneNode(name);
	return node;
}

void EntityManager::addElement(Element* element)
{
	_elements.push_back(element);
}

int EntityManager::getInstanceNumber() {
	_instanceNumber++;
	return _instanceNumber;
}

void EntityManager::setZombieType(const Zombie& zombieType)
{
	_zombieType = new Zombie(zombieType);
}

const Zombie& EntityManager::getZombieType() const
{
	return *_zombieType;
}

void EntityManager::setHumanType(const Human& humanType)
{
	_humanType = new Human(humanType);
}

const Human& EntityManager::getHumanType() const
{
	return *_humanType;
}

void EntityManager::addWeaponType(const Weapon& weaponType)
{
	_weaponsType.push_back(new Weapon(weaponType));
}

Weapon* EntityManager::getWeaponType(EN_WEAPON_TYPE type) const
{
	Weapon* resultWeapon = 0;
	std::vector<Weapon*>::const_iterator c_it;
	c_it = _weaponsType.begin();

	for(;c_it != _weaponsType.end(); c_it++)
	{
		if ((*c_it)->getWeaponType() == type)
		{
			resultWeapon = (*c_it);
			break;
		}
	}

	return resultWeapon;
}

void EntityManager::setPlayerType(const Player& playerType)
{
	_playerType = new Player(playerType);

}
Player* EntityManager::getPlayerType() const
{
	return _playerType;
}

void EntityManager::addProjectile(Projectile* projectile)
{
	_projectiles.push_front(projectile);
}

void EntityManager::removeProjectile(std::string projectileName)
{
	std::deque<Projectile*>::iterator it;
	it = _projectiles.begin();

	for(; it != _projectiles.end(); it++)
	{
		if( (*it)->getSceneNode()->getName() == projectileName )
		{
			delete (*it);
			_projectiles.erase(it);
			break;
		}
	}
}

void EntityManager::destroyAll()
{
	if (_zombieType != 0)
		delete _zombieType;

	if (_playerType != 0)
		delete _playerType;

	if (_player != 0)
		delete _player;

	_humanType		= 0;
	_zombieType 	= 0;
	_playerType		= 0;
	_player			= 0;

	destroyAllZombies();
	destroyAllHumans();
	destroyAllWeaponsType();
	destroyAllWeapons();
	destroyAllProjectiles();
	destroyAllElements();

}

void EntityManager::destroyAllZombies()
{
	std::vector<Character*>::iterator it;
	it = _zombies.begin();
	for(; it != _zombies.end(); it++)
		delete *it;

	_zombies.clear();
}

void EntityManager::destroyAllHumans()
{
	std::vector<Character*>::iterator it;
	it = _humans.begin();
	for(; it != _humans.end(); it++)
		delete *it;

	_humans.clear();

}

void EntityManager::destroyAllWeaponsType()
{
	std::vector<Weapon*>::iterator it;
	it = _weaponsType.begin();
	for(; it != _weaponsType.end(); it++)
		delete *it;

	_weaponsType.clear();
}

void EntityManager::destroyAllWeapons()
{
	std::vector<Weapon*>::iterator it;
	it = _weapons.begin();
	for(; it != _weapons.end(); it++)
		delete *it;

	_weapons.clear();
}

void EntityManager::destroyAllProjectiles()
{
	std::deque<Projectile*>::iterator it;
	it = _projectiles.begin();
	for(; it != _projectiles.end(); it++)
		delete *it;

	_projectiles.clear();
}

void EntityManager::destroyAllElements()
{
	std::vector<Element*>::iterator it;
	it = _elements.begin();
	for(; it != _elements.end(); it++)
		delete *it;

	_elements.clear();
}

int EntityManager::getProjectileTotal() const
{
	return _projectiles.size();
}

Ogre::SceneNode* EntityManager::getBaseNode()
{
	return _baseNode;
}

void EntityManager::setBaseNode(Ogre::SceneNode* baseNode) {
	_baseNode = baseNode;
}

int EntityManager::getZombiesAlive() const
{
	return _zombies.size();
}

int EntityManager::getHumansAlive() const
{
	return _humans.size();
}


std::vector<Character*>& EntityManager::getZombies()
{
	return _zombies;
}

std::vector<Character*>& EntityManager::getHumans()
{
	return _humans;
}

std::vector<Weapon*> EntityManager::getWeapons() const
{
	return _weapons;
}

std::deque<Projectile*> EntityManager::getProjectiles() const
{
	return _projectiles;
}

std::vector<Element*> EntityManager::getElements() const
{
	return _elements;
}

void EntityManager::addZombie(Character* zombie)
{
	_zombies.push_back(zombie);
}

void EntityManager::addHuman(Character* human)
{
	_humans.push_back(human);
}

void EntityManager::addWeapon(Weapon* weapon)
{
	_weapons.push_back(weapon);
}

std::vector<Character*>::iterator EntityManager::getZombieIterator(std::string zombieDead)
{
	std::vector<Character*>::iterator it;
	it = _zombies.begin();

	for(; it != _zombies.end(); it++)
	{
		if( (*it)->getNode()->getName() == zombieDead )
			break;
	}

	return it;
}

std::vector<Character*>::iterator EntityManager::getHumanIterator(std::string humanDead)
{
	std::vector<Character*>::iterator it;
	it = _humans.begin();

	for(; it != _humans.end(); it++)
	{
		if( (*it)->getNode()->getName() == humanDead )
			break;
	}

	return it;
}

void EntityManager::setPlayer(Player* player)
{
	if (_player!=0) {
		delete(_player);
	}
	_player = player;
}

Player* EntityManager::getPlayer(void)
{
	return _player;
}
