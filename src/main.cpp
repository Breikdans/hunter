
#define UNUSED_VARIABLE(x) (void)x


#include "GameManager.h"
#include "IntroState.h"
#include "MenuState.h"
#include "MenuEscState.h"
#include "PlayState.h"
#include "PauseState.h"
#include "DeathState.h"
#include "EndLevelState.h"
#include "RecordsState.h"
#include "CreditsState.h"
#include "LoadLevelState.h"
#include "Importer.h"
#include "EndGameState.h"
#include "NewRecordState.h"
#include "PhysicsManager.h"
#include "EntityManager.h"
#include "MissionsManager.h"
#include "MyUtils.h"


#include <iostream>

using namespace std;

int main ()
{
	GameManager* game 				= new GameManager();
	IntroState* introState 			= new IntroState();

	MenuState* menuState 			= new MenuState();
	MenuEscState* menuEscState 		= new MenuEscState();
	PlayState* playState			= new PlayState();
	PauseState* pauseState			= new PauseState();
	DeathState* deathState			= new DeathState();
	EndLevelState* endLevelState	= new EndLevelState();
	RecordsState* recordsState		= new RecordsState();
	CreditsState* creditsState		= new CreditsState();
	LoadLevelState* loadLevelState 	= new LoadLevelState();
	Importer* importer				= new Importer();
	EndGameState* endState			= new EndGameState();
	NewRecordState* newRecordState	= new NewRecordState();
	PhysicsManager* physicsManager	= new PhysicsManager();
	MyUtils* myUtils				= new MyUtils();
	EntityManager* entityManager	= new EntityManager();
	MissionsManager* missionsManager= new MissionsManager();

	UNUSED_VARIABLE(game);
	UNUSED_VARIABLE(introState);
	UNUSED_VARIABLE(menuState);
	UNUSED_VARIABLE(menuEscState);
	UNUSED_VARIABLE(playState);
	UNUSED_VARIABLE(pauseState);
	UNUSED_VARIABLE(deathState);
	UNUSED_VARIABLE(endLevelState);
	UNUSED_VARIABLE(recordsState);
	UNUSED_VARIABLE(creditsState);
	UNUSED_VARIABLE(loadLevelState);
	UNUSED_VARIABLE(importer);
	UNUSED_VARIABLE(endState);
	UNUSED_VARIABLE(newRecordState);
	UNUSED_VARIABLE(physicsManager);
	UNUSED_VARIABLE(myUtils);
	UNUSED_VARIABLE(entityManager);
	UNUSED_VARIABLE(missionsManager);

	try
	{
		// Inicializa el juego y transición al primer estado.
		game->start(IntroState::getSingletonPtr());
	}
	catch (Ogre::Exception& e)
	{
		std::cerr << "Excepción detectada: " << e.getFullDescription();
	}

	delete game;

	return 0;
}
