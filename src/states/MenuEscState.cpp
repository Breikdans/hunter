#include "MenuEscState.h"

template<> MenuEscState* Ogre::Singleton<MenuEscState>::msSingleton = 0;

const int MIN_SPEED_MOUSE = 5;
const int MAX_SPEED_MOUSE = 30 + MIN_SPEED_MOUSE;

void MenuEscState::enter ()
{
	// musica del menu
	DB_SIN_SONIDO(IntroState::getSingleton().getMenuTrackPtr()->play();)
	_overlayManager = Ogre::OverlayManager::getSingletonPtr();

	CEGUI::System::getSingleton().getDefaultGUIContext().getMouseCursor().show();
	showMenuEscMsgCegui();
}

void MenuEscState::exit ()
{
	DB_SIN_SONIDO(IntroState::getSingleton().getMenuTrackPtr()->stop();)
	CEGUI::System::getSingleton().getDefaultGUIContext().getMouseCursor().hide();
	CEGUI::System::getSingleton().getDefaultGUIContext().getRootWindow()->hide();
}

void MenuEscState::pause () {}

void MenuEscState::resume () {}

bool MenuEscState::frameStarted(const Ogre::FrameEvent& evt)
{
	CEGUI::System::getSingleton().getDefaultGUIContext().injectTimePulse(evt.timeSinceLastFrame);
	return true;
}

bool MenuEscState::frameEnded(const Ogre::FrameEvent& evt)
{
	return true;
}

void MenuEscState::keyPressed(const OIS::KeyEvent &e)
{
	// Tecla p --> PauseState.
//	DB_ESTADOS(if (e.key == OIS::KC_E){)
//	DB_ESTADOS(	PlayState::getSingleton().setExitGame(true);)
//	DB_ESTADOS(	popState();})
//	DB_ESTADOS(else if(e.key == OIS::KC_F){)
//	DB_ESTADOS(	popState();})

	if (e.key == OIS::KC_ESCAPE)
		popState();
}

void MenuEscState::keyReleased(const OIS::KeyEvent &e) {}

void MenuEscState::mouseMoved(const OIS::MouseEvent &e)
{
	// Gestion del overlay (CURSOR)-----------------------------
	// posiciones del puntero del raton en pixeles
	int posx = e.state.X.abs;
	int posy = e.state.Y.abs;

	locateOverlayMousePointer(posx,posy);
	locateCeguiMousePointer(posx,posy);
}

void MenuEscState::locateOverlayMousePointer(int x,int y)
{
	Ogre::OverlayElement *oe;
	oe = _overlayManager->getOverlayElement("panelMousePointer");
	oe->setLeft(x); oe->setTop(y);

}

void MenuEscState::locateCeguiMousePointer(int x, int y)
{
	int width = InputManager::getSingleton().getMouse()->getMouseState().width;
	int height = InputManager::getSingleton().getMouse()->getMouseState().height;
	CEGUI::System::getSingleton().getDefaultGUIContext().getMouseCursor().setPosition(CEGUI::Vector2f(x,y));
	CEGUI::System::getSingleton().getDefaultGUIContext().injectMouseMove(x/float(width),y/float(height));
}

void MenuEscState::mousePressed(const OIS::MouseEvent &e, OIS::MouseButtonID id)
{
	CEGUI::System::getSingleton().getDefaultGUIContext().injectMouseButtonDown(convertMouseButton(id));
}

void MenuEscState::mouseReleased(const OIS::MouseEvent &e, OIS::MouseButtonID id)
{
	CEGUI::System::getSingleton().getDefaultGUIContext().injectMouseButtonUp(convertMouseButton(id));
}

MenuEscState* MenuEscState::getSingletonPtr ()
{
	return msSingleton;
}

MenuEscState& MenuEscState::getSingleton ()
{ 
	assert(msSingleton);
	return *msSingleton;
}

void MenuEscState::showMenuEscMsgCegui()
{
	//Sheet
	CEGUI::Window* ceguiSheet = CEGUI::WindowManager::getSingleton().createWindow("DefaultWindow","menuEsc");

	//Config Window
	_MenuEscMsg = CEGUI::WindowManager::getSingleton().loadLayoutFromFile("menuEsc.layout");

DB_TRACE(cout << "volumen: " << Mix_VolumeMusic(-1) << " porcentage: " << percentageFromValue(Mix_VolumeMusic(-1), MIX_MAX_VOLUME) << endl;)

	CEGUI::Slider* sliderMusic = static_cast<CEGUI::Slider*>(_MenuEscMsg->getChild("sliderMusic"));
	CEGUI::Slider* sliderMouse = static_cast<CEGUI::Slider*>(_MenuEscMsg->getChild("sliderMouse"));

	int speedMouse = EntityManager::getSingleton().getPlayer()->getSpeedPitch();
//cout << "Velocidad Real Raton Inicial: " << speedMouse;
	if (speedMouse - MIN_SPEED_MOUSE < MIN_SPEED_MOUSE) speedMouse = MIN_SPEED_MOUSE;
	if (speedMouse > MAX_SPEED_MOUSE) speedMouse = MAX_SPEED_MOUSE;

	speedMouse -= MIN_SPEED_MOUSE;
//cout << " Velocidad Slider Raton Inicial: " << speedMouse << endl;
	sliderMouse->setCurrentValue(speedMouse);

	// cogemos el volumen actual, para representarlo en el slider
	int volumeMusic = percentageFromValue(Mix_VolumeMusic(-1), MIX_MAX_VOLUME);
	stringstream stringVolumeMusic;
	stringVolumeMusic << volumeMusic;
	_MenuEscMsg->getChild("textMusic")->setText(stringVolumeMusic.str());
	sliderMusic->setCurrentValue(volumeMusic);
	sliderMusic->setMaxValue(100.0f);

	// comprobamos como debemos pintar el mute, dependiendo del estado en el que se quedo
	CEGUI::ToggleButton* checkbox = static_cast<CEGUI::ToggleButton*>(_MenuEscMsg->getChild("checkMute"));
	checkbox->setSelected(PlayState::getSingleton().getMute());

	// SLIDER VOLUME MUSIC
	CEGUI::Window* sldMusic = _MenuEscMsg->getChild("sliderMusic");
	sldMusic->subscribeEvent( CEGUI::Slider::EventValueChanged,
							  CEGUI::Event::Subscriber(&MenuEscState::sliderMusic, this));
	// CHECKBOX MUTE
	CEGUI::Window* chkMute = _MenuEscMsg->getChild("checkMute");
	chkMute->subscribeEvent( CEGUI::ToggleButton::EventSelectStateChanged,
							 CEGUI::Event::Subscriber(&MenuEscState::checkMute, this));

	// SLIDER MOUSE SPEED
	CEGUI::Window* sldMouse = _MenuEscMsg->getChild("sliderMouse");
	sldMouse->subscribeEvent( CEGUI::Slider::EventValueChanged,
							  CEGUI::Event::Subscriber(&MenuEscState::sliderMouse, this));

	// RESUME
	CEGUI::Window* btnResume = _MenuEscMsg->getChild("buttonResume");
	btnResume->subscribeEvent( CEGUI::PushButton::EventClicked,
							  CEGUI::Event::Subscriber(&MenuEscState::buttonResume, this));

	// QUIT
	CEGUI::Window* btnQuit = _MenuEscMsg->getChild("buttonQuit");
	btnQuit->subscribeEvent( CEGUI::PushButton::EventClicked,
							  CEGUI::Event::Subscriber(&MenuEscState::buttonQuit, this));

	//Attaching buttons
	ceguiSheet->addChild(_MenuEscMsg);
	CEGUI::System::getSingleton().getDefaultGUIContext().setRootWindow(ceguiSheet);

}

int MenuEscState::percentageFromValue(int value, int maxValue)
{
	return (int) ( (value * 100) / maxValue );
}

int MenuEscState::valueFromPercentage(int percent, int maxValue)
{
	return (int) ( (percent * maxValue) / 100 );
}


bool MenuEscState::sliderMusic(const CEGUI::EventArgs &e)
{
	stringstream stringVolumeMusic;
	CEGUI::Slider* sliderMusic 	= static_cast<CEGUI::Slider*>(_MenuEscMsg->getChild("sliderMusic"));
	CEGUI::ToggleButton* checkbox = static_cast<CEGUI::ToggleButton*>(_MenuEscMsg->getChild("checkMute"));

	int volumeMusic 			= static_cast<int>(sliderMusic->getCurrentValue());
DB_TRACE(cout << "Porcentaje: " << volumeMusic;)

	stringVolumeMusic << volumeMusic;
	_MenuEscMsg->getChild("textMusic")->setText(stringVolumeMusic.str());

DB_TRACE(cout << " Valor volumen: " << valueFromPercentage(volumeMusic, MIX_MAX_VOLUME) << endl;)
	Mix_VolumeMusic(valueFromPercentage(volumeMusic, MIX_MAX_VOLUME));
	Mix_VolumeChunk(IntroState::getSingleton().getShotFXPtr()->getChunk(), valueFromPercentage(volumeMusic, MIX_MAX_VOLUME));
	Mix_VolumeChunk(IntroState::getSingleton().getPowerShotFXPtr()->getChunk(), valueFromPercentage(volumeMusic, MIX_MAX_VOLUME));
	Mix_VolumeChunk(IntroState::getSingleton().getNoAmmoFXPtr()->getChunk(), valueFromPercentage(volumeMusic, MIX_MAX_VOLUME));
	Mix_VolumeChunk(IntroState::getSingleton().getZombie1FXPtr()->getChunk(), valueFromPercentage(volumeMusic, MIX_MAX_VOLUME));
	Mix_VolumeChunk(IntroState::getSingleton().getMachineGunShotFXPtr()->getChunk(), valueFromPercentage(volumeMusic, MIX_MAX_VOLUME));

	if(checkbox->isSelected() && volumeMusic != 0)
	{
		checkbox->setSelected(false);
		PlayState::getSingleton().setMute(false);
	}

	return true;
}

bool MenuEscState::sliderMouse(const CEGUI::EventArgs &e)
{
	stringstream stringVolumeMusic;
	CEGUI::Slider* sliderMouse 	= static_cast<CEGUI::Slider*>(_MenuEscMsg->getChild("sliderMouse"));

	int speedMouse		= static_cast<int>(sliderMouse->getCurrentValue());
//cout << "Velocidad Raton Slider: " << speedMouse;
	if (speedMouse + MIN_SPEED_MOUSE > MAX_SPEED_MOUSE) speedMouse = MAX_SPEED_MOUSE - MIN_SPEED_MOUSE;
	speedMouse += MIN_SPEED_MOUSE;
//cout << " Velocidad Real Raton: " << speedMouse << endl;
	EntityManager::getSingleton().getPlayer()->setSpeedPitch(speedMouse);
	return true;
}

bool MenuEscState::checkMute(const CEGUI::EventArgs &e)
{
	CEGUI::ToggleButton* checkbox = static_cast<CEGUI::ToggleButton*>(_MenuEscMsg->getChild("checkMute"));

	if(checkbox->isSelected())
	{
		PlayState::getSingleton().saveVolumeInMuteOn(Mix_VolumeMusic(-1));
		PlayState::getSingleton().setMute(true);
		Mix_VolumeMusic(0);
	}
	else
	{
		Mix_VolumeMusic(PlayState::getSingleton().getVolumeInMuteOff());
		PlayState::getSingleton().setMute(false);
	}

	stringstream stringVolumeMusic;
	int volumeMusic = percentageFromValue(Mix_VolumeMusic(-1), MIX_MAX_VOLUME);
DB_TRACE(cout << "Porcentaje recuperado: " << volumeMusic << " Valor volumen: " << Mix_VolumeMusic(-1) << endl;)
	stringVolumeMusic << volumeMusic;
	CEGUI::Slider* sliderMusic 	= static_cast<CEGUI::Slider*>(_MenuEscMsg->getChild("sliderMusic"));
	sliderMusic->setCurrentValue(volumeMusic);
	_MenuEscMsg->getChild("textMusic")->setText(stringVolumeMusic.str());

	return true;
}

bool MenuEscState::buttonResume(const CEGUI::EventArgs &e)
{
	popState();
	return true;
}

bool MenuEscState::buttonQuit(const CEGUI::EventArgs &e)
{
	PlayState::getSingleton().setExitGame(true);
	popState();
	return true;
}

CEGUI::MouseButton MenuEscState::convertMouseButton(OIS::MouseButtonID id)
{
	CEGUI::MouseButton ceguiId;
	switch(id)
	{
		case OIS::MB_Left:
			ceguiId = CEGUI::LeftButton;
			break;
		case OIS::MB_Right:
			ceguiId = CEGUI::RightButton;
			break;
		case OIS::MB_Middle:
			ceguiId = CEGUI::MiddleButton;
			break;
		default:
			ceguiId = CEGUI::LeftButton;
	}
	return ceguiId;
}


