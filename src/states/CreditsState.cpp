#include "CreditsState.h"

template<> CreditsState* Ogre::Singleton<CreditsState>::msSingleton = 0;

void CreditsState::enter ()
{
	_overlayManager = Ogre::OverlayManager::getSingletonPtr();
	createOverlay();

}

void CreditsState::createOverlay()
{
	unsigned int width, height, depth;
	int left, top;

	Ogre::Overlay *overlay = _overlayManager->getByName("credits");
	Ogre::Root::getSingletonPtr()->getAutoCreatedWindow()->getMetrics(width, height, depth, left, top);

	overlay->setScale(((float(width) / 100) / 1024) * 100, ((float(height) / 100) / 768) * 100);
	overlay->show();
}

void CreditsState::exit ()
{
	Ogre::Overlay *creditsOverlay = _overlayManager->getByName("credits");
	creditsOverlay->hide();
}

void CreditsState::pause () {}

void CreditsState::resume () {}

bool CreditsState::frameStarted(const Ogre::FrameEvent& evt)
{
	return true;
}

void CreditsState::locateOverlayMousePointer(int x,int y)
{
	Ogre::OverlayElement *oe;
	oe = _overlayManager->getOverlayElement("panelMousePointer");
	oe->setLeft(x); oe->setTop(y);
}

bool CreditsState::frameEnded(const Ogre::FrameEvent& evt)
{
	return true;
}

void CreditsState::keyPressed(const OIS::KeyEvent &e) {}

void CreditsState::keyReleased(const OIS::KeyEvent &e) {}

void CreditsState::mouseMoved(const OIS::MouseEvent &e)
{
	locateOverlayMousePointer(e.state.X.abs, e.state.Y.abs);
}

void CreditsState::mousePressed(const OIS::MouseEvent &e, OIS::MouseButtonID id)
{
	popState();
}

void CreditsState::mouseReleased(const OIS::MouseEvent &e, OIS::MouseButtonID id) {}

CreditsState* CreditsState::getSingletonPtr ()
{
	return msSingleton;
}

CreditsState& CreditsState::getSingleton ()
{ 
	assert(msSingleton);
	return *msSingleton;
}


