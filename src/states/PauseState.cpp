#include "PauseState.h"

template<> PauseState* Ogre::Singleton<PauseState>::msSingleton = 0;

void PauseState::enter ()
{
	// musica del menu
	DB_SIN_SONIDO(IntroState::getSingleton().getMenuTrackPtr()->play();)

	_overlayManager = Ogre::OverlayManager::getSingletonPtr();
	_overlayManager->getByName("pause")->show();
}

void PauseState::exit ()
{
	// paramos musica del menu
	DB_SIN_SONIDO(IntroState::getSingleton().getMenuTrackPtr()->stop();)

	_overlayManager->getByName("pause")->hide();
}

void PauseState::pause () {}

void PauseState::resume () {}

bool PauseState::frameStarted(const Ogre::FrameEvent& evt)
{
	return true;
}

bool PauseState::frameEnded(const Ogre::FrameEvent& evt)
{
	return true;
}

void PauseState::keyPressed(const OIS::KeyEvent &e)
{
	// Tecla p --> Estado anterior.
	if (e.key == OIS::KC_P)
	{
		popState();
	}
}

void PauseState::keyReleased(const OIS::KeyEvent &e) {}

void PauseState::mouseMoved(const OIS::MouseEvent &e)
{
	// Gestion del overlay (CURSOR)-----------------------------
	// posiciones del puntero del raton en pixeles
	int posx = e.state.X.abs;
	int posy = e.state.Y.abs;

	locateOverlayMousePointer(posx,posy);
}

void PauseState::locateOverlayMousePointer(int x,int y)
{
	Ogre::OverlayElement *oe;
	oe = _overlayManager->getOverlayElement("panelMousePointer");
	oe->setLeft(x); oe->setTop(y);
}

void PauseState::mousePressed(const OIS::MouseEvent &e, OIS::MouseButtonID id) {}

void PauseState::mouseReleased(const OIS::MouseEvent &e, OIS::MouseButtonID id) {}

PauseState* PauseState::getSingletonPtr ()
{
	return msSingleton;
}

PauseState& PauseState::getSingleton ()
{ 
	assert(msSingleton);
	return *msSingleton;
}

