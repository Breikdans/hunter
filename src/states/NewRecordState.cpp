#include "NewRecordState.h"

template<> NewRecordState* Ogre::Singleton<NewRecordState>::msSingleton = 0;

void NewRecordState::enter ()
{
	showEnterRecordNameCegui();
	_overlayManager = Ogre::OverlayManager::getSingletonPtr();
}

void NewRecordState::exit () {}

void NewRecordState::pause () {}

void NewRecordState::resume () {}

bool NewRecordState::frameStarted(const Ogre::FrameEvent& evt)
{
	CEGUI::System::getSingleton().getDefaultGUIContext().injectTimePulse(evt.timeSinceLastFrame);
	return true;
}

bool NewRecordState::frameEnded(const Ogre::FrameEvent& evt)
{
	return true;
}

void NewRecordState::locateOverlayMousePointer(int x,int y)
{
	Ogre::OverlayElement *oe;
	oe = _overlayManager->getOverlayElement("panelMousePointer");
	oe->setLeft(x); oe->setTop(y);
}

void NewRecordState::locateCeguiMousePointer(int x, int y)
{
	int width = InputManager::getSingleton().getMouse()->getMouseState().width;
	int height = InputManager::getSingleton().getMouse()->getMouseState().height;
	CEGUI::System::getSingleton().getDefaultGUIContext().getMouseCursor().setPosition(CEGUI::Vector2f(x,y));
	CEGUI::System::getSingleton().getDefaultGUIContext().injectMouseMove(x/float(width),y/float(height));
}

void NewRecordState::keyPressed(const OIS::KeyEvent &e)
{
	CEGUI::System::getSingleton().getDefaultGUIContext().injectKeyDown(static_cast<CEGUI::Key::Scan>(e.key));
	CEGUI::System::getSingleton().getDefaultGUIContext().injectChar(e.text);
}

void NewRecordState::keyReleased(const OIS::KeyEvent &e) {}

void NewRecordState::mouseMoved(const OIS::MouseEvent &e)
{
	locateOverlayMousePointer(e.state.X.abs, e.state.Y.abs);
	locateCeguiMousePointer(e.state.X.abs, e.state.Y.abs);
}

void NewRecordState::mousePressed(const OIS::MouseEvent &e, OIS::MouseButtonID id)
{
	CEGUI::System::getSingleton().getDefaultGUIContext().injectMouseButtonDown(convertMouseButton(id));
}

void NewRecordState::mouseReleased(const OIS::MouseEvent &e, OIS::MouseButtonID id)
{
	CEGUI::System::getSingleton().getDefaultGUIContext().injectMouseButtonUp(convertMouseButton(id));
}

NewRecordState* NewRecordState::getSingletonPtr ()
{
	return msSingleton;
}

NewRecordState& NewRecordState::getSingleton ()
{ 
	assert(msSingleton);
	return *msSingleton;
}

void NewRecordState::showEnterRecordNameCegui()
{
	//Sheet
	CEGUI::Window* ceguiSheet = CEGUI::WindowManager::getSingleton().createWindow("DefaultWindow","newrecord");

	//Config Window
	_newRecordWin = CEGUI::WindowManager::getSingleton().loadLayoutFromFile("ceguiNewRecord.layout");

	// OK
	CEGUI::Window* okButton = _newRecordWin->getChild("btn_Aceptar");
	okButton->subscribeEvent( CEGUI::PushButton::EventClicked,
							  CEGUI::Event::Subscriber(&NewRecordState::BotonAceptar, this));

	//Attaching buttons
	ceguiSheet->addChild(_newRecordWin);
	CEGUI::System::getSingleton().getDefaultGUIContext().setRootWindow(ceguiSheet);

}

bool NewRecordState::BotonAceptar(const CEGUI::EventArgs &e)
{
	CEGUI::String Cegui_sCadena;
	std::string sCadena;
	int Puntos = 0;

	Cegui_sCadena = _newRecordWin->getChild("txt_Name")->getText();

	sCadena = Cegui_sCadena.c_str();
	Puntos = PlayState::getSingleton().getPointsPlayer();

	IntroState::getSingleton().gameRecords.insert(std::make_pair(Puntos, sCadena));
	saveRecords();

	changeState(RecordsState::getSingletonPtr());
	return true;
}

void NewRecordState::saveRecords()
{
	std::multimap<unsigned int, std::string>::reverse_iterator rit;
	STR_Record str_record;

	std::ofstream file;

	file.open("records.txt");

	rit = IntroState::getSingleton().gameRecords.rbegin();
	for (int i = 0; rit != IntroState::getSingleton().gameRecords.rend() && i < MAX_PLAYER_RECORDS; rit++,i++)
	{
		file << (*rit).first << "_" << (*rit).second << std::endl;
	}

	file.close();
}

CEGUI::MouseButton NewRecordState::convertMouseButton(OIS::MouseButtonID id)
{
	CEGUI::MouseButton ceguiId;
	switch(id)
	{
		case OIS::MB_Left:
			ceguiId = CEGUI::LeftButton;
			break;
		case OIS::MB_Right:
			ceguiId = CEGUI::RightButton;
			break;
		case OIS::MB_Middle:
			ceguiId = CEGUI::MiddleButton;
			break;
		default:
			ceguiId = CEGUI::LeftButton;
	}
	return ceguiId;
}


