#include "LoadLevelState.h"

template<> LoadLevelState* Ogre::Singleton<LoadLevelState>::msSingleton = 0;

LoadLevelState::LoadLevelState()
{
	_root			= 0;
	_sceneMgr		= 0;
	_renderWindow	= 0;
	_viewport		= 0;
	_mainCamera		= 0;
	_overlayManager	= 0;

	_currentLevel	= 1;
	_exitGame		= false;;
}

void LoadLevelState::setCurrentLevel(int number) {
	_currentLevel = number;
}

int LoadLevelState::getCurrentLevel() const {
	return _currentLevel;
}

void LoadLevelState::enter()
{
	_root = Ogre::Root::getSingletonPtr();

	// Se recupera el gestor de escena y la cámara.
	_sceneMgr 		= _root->getSceneManager("SceneManager");

	_sceneMgr->clearScene();
	_root->getAutoCreatedWindow()->removeAllViewports();

	_mainCamera 	= _sceneMgr->getCamera("mainCamera");
	_renderWindow 	= _root->getAutoCreatedWindow();
	_viewport 		= _renderWindow->addViewport(_mainCamera);

	// Metemos una luz ambiental, una luz que no tiene fuente de origen. Ilumina a todos los objetos
	_sceneMgr->setAmbientLight(Ogre::ColourValue(1, 1, 1));

	_mainCamera->setPosition(Ogre::Vector3(0, 15, 10));	// posicionamos...
	_mainCamera->lookAt(Ogre::Vector3(0, 0, 0));		// enfocamos a 0,0,0
	_mainCamera->setNearClipDistance(5);				// establecemos plano cercano del frustum
	_mainCamera->setFarClipDistance(10000);				// establecemos plano lejano del frustum

	// Creamos el plano de imagen (lienzo) asociado a la camara
	double width 	= _viewport->getActualWidth();		// recogemos ancho del viewport actual
	double height 	= _viewport->getActualHeight();		// recogemos alto del viewport actual
	_mainCamera->setAspectRatio(width / height);		// calculamos ratio (4:3 = 1,333 16:9 1,777)

	_overlayManager = Ogre::OverlayManager::getSingletonPtr();

	createOverlay();

	loadLevel(_currentLevel);

	_exitGame = false;
}

void LoadLevelState::createOverlay()
{
	unsigned int width, height, depth;
	int left, top;

	Ogre::Overlay *overlay = _overlayManager->getByName("loadLevel");
	_root->getAutoCreatedWindow()->getMetrics(width, height, depth, left, top);

	overlay->setScale(((float(width) / 100) / 1024) * 100, ((float(height) / 100) / 768) * 100);
	overlay->show();
}

void LoadLevelState::loadLevel(int currentLevel)
{
	std::stringstream fileXML;
	std::string pathFileXML;
	Scene* newScene = new Scene();

	fileXML << PATH_LEVEL << currentLevel << "/" << XML_LEVEL;	//==> ./media/levels/level1/output.xml
	pathFileXML = fileXML.str();

	// Importamos el fichero XML de nivel. hay que comprobar si hay nivel!!
	Importer::getSingleton().parseScene(pathFileXML.c_str(), newScene);
	//EntityManager::getSingleton().getPlayer()->CoordsToCellIndex();
//int x = PlayState::getSingleton().getPlayer()->getCellX();
//int y = PlayState::getSingleton().getPlayer()->getCellY();
//newScene->grid(x, y).setType(EN_PLAYER_CELL);
//	newScene->DebugGrid();
	PlayState::getSingleton().addScene(newScene);

	changeState(PlayState::getSingletonPtr());
}

void LoadLevelState::exit() {

	Ogre::Overlay *overlay = _overlayManager->getByName("loadLevel");
	overlay->hide();

}

void LoadLevelState::pause() {}

void LoadLevelState::resume() {}

bool LoadLevelState::frameStarted(const Ogre::FrameEvent& evt)
{
	return true;
}

bool LoadLevelState::frameEnded(const Ogre::FrameEvent& evt)
{
	if (_exitGame)
		return false;

	return true;
}

void LoadLevelState::keyPressed(const OIS::KeyEvent &e) {
	// Tecla p --> PauseState.
	if (e.key == OIS::KC_1)
	{
		if (_currentLevel==0) {
			PlayState::getSingletonPtr()->setLives(3);
		}
		changeState(PlayState::getSingletonPtr());
	}

	if (e.key == OIS::KC_2)
	{
		changeState(MenuState::getSingletonPtr());
	}
}

void LoadLevelState::keyReleased(const OIS::KeyEvent &e ) {}

void LoadLevelState::mouseMoved(const OIS::MouseEvent &e) {
	// Gestion del overlay (CURSOR)-----------------------------
	// posiciones del puntero del raton en pixeles
	int posx = e.state.X.abs;
	int posy = e.state.Y.abs;

	locateOverlayMousePointer(posx,posy);
}

void LoadLevelState::locateOverlayMousePointer(int x, int y) {
	Ogre::OverlayElement *oe;
	oe = _overlayManager->getOverlayElement("panelMousePointer");
	oe->setLeft(x); oe->setTop(y);
}

void LoadLevelState::mousePressed(const OIS::MouseEvent &e, OIS::MouseButtonID id) {}

void LoadLevelState::mouseReleased(const OIS::MouseEvent &e, OIS::MouseButtonID id) {}

LoadLevelState* LoadLevelState::getSingletonPtr()
{
	return msSingleton;
}

LoadLevelState & LoadLevelState::getSingleton ()
{
	assert(msSingleton);
	return *msSingleton;
}

