#include "EndGameState.h"

template<> EndGameState* Ogre::Singleton<EndGameState>::msSingleton = 0;

void EndGameState::enter ()
{
	showEndMsgCegui();
	_overlayManager = Ogre::OverlayManager::getSingletonPtr();
}

void EndGameState::exit ()
{
//	CEGUI::System::getSingleton().getDefaultGUIContext().getRootWindow()->hide();
}

void EndGameState::pause () {}

void EndGameState::resume () {}

bool EndGameState::frameStarted(const Ogre::FrameEvent& evt)
{
	CEGUI::System::getSingleton().getDefaultGUIContext().injectTimePulse(evt.timeSinceLastFrame);
	return true;
}

bool EndGameState::frameEnded(const Ogre::FrameEvent& evt)
{
	return true;
}

void EndGameState::keyPressed(const OIS::KeyEvent &e)
{
	DB_ESTADOS(if (e.key == OIS::KC_P))
		DB_ESTADOS(changeState(NewRecordState::getSingletonPtr());)
	DB_ESTADOS(else if(e.key == OIS::KC_M))
		DB_ESTADOS(changeState(MenuState::getSingletonPtr());)
}

void EndGameState::keyReleased(const OIS::KeyEvent &e) {}

void EndGameState::mouseMoved(const OIS::MouseEvent &e)
{
	locateOverlayMousePointer(e.state.X.abs, e.state.Y.abs);
	locateCeguiMousePointer(e.state.X.abs, e.state.Y.abs);
}

void EndGameState::mousePressed(const OIS::MouseEvent &e, OIS::MouseButtonID id)
{
	CEGUI::System::getSingleton().getDefaultGUIContext().injectMouseButtonDown(convertMouseButton(id));
}

void EndGameState::mouseReleased(const OIS::MouseEvent &e, OIS::MouseButtonID id)
{
	CEGUI::System::getSingleton().getDefaultGUIContext().injectMouseButtonUp(convertMouseButton(id));
}

void EndGameState::locateOverlayMousePointer(int x,int y)
{
	Ogre::OverlayElement *oe;
	oe = _overlayManager->getOverlayElement("panelMousePointer");
	oe->setLeft(x); oe->setTop(y);
}

void EndGameState::locateCeguiMousePointer(int x, int y)
{
	int width = InputManager::getSingleton().getMouse()->getMouseState().width;
	int height = InputManager::getSingleton().getMouse()->getMouseState().height;
	CEGUI::System::getSingleton().getDefaultGUIContext().getMouseCursor().setPosition(CEGUI::Vector2f(x,y));
	CEGUI::System::getSingleton().getDefaultGUIContext().injectMouseMove(x/float(width),y/float(height));
}

EndGameState* EndGameState::getSingletonPtr ()
{
	return msSingleton;
}

EndGameState& EndGameState::getSingleton ()
{ 
	assert(msSingleton);
	return *msSingleton;
}

void EndGameState::showEndMsgCegui()
{
	int iPuntosPlayer = PlayState::getSingleton().getPointsPlayer();

	std::stringstream sCadena;

	//Sheet
	CEGUI::Window* _ceguiSheet = CEGUI::WindowManager::getSingleton().createWindow("DefaultWindow","endgame");

	//Config Window
	CEGUI::Window* endMsgWin = CEGUI::WindowManager::getSingleton().loadLayoutFromFile("endgame.layout");

	sCadena.str("");
	sCadena << "[font='DejaVuSans-12'][colour='FF0000FF']Puntos JUGADOR: [colour='FFFFFFFF'] " << iPuntosPlayer;
	endMsgWin->getChild("Text4")->setText(sCadena.str());

	// OK
	CEGUI::Window* okButton = endMsgWin->getChild("btn_ok");
	okButton->subscribeEvent( CEGUI::PushButton::EventClicked,
							  CEGUI::Event::Subscriber(&EndGameState::BotonOk, this));

	//Attaching buttons
	_ceguiSheet->addChild(endMsgWin);
	CEGUI::System::getSingleton().getDefaultGUIContext().setRootWindow(_ceguiSheet);

	CEGUI::System::getSingleton().getDefaultGUIContext().getMouseCursor().show();
}

bool EndGameState::BotonOk(const CEGUI::EventArgs &e)
{
	int iPuntosPlayer = PlayState::getSingleton().getPointsPlayer();

	if(isNewRecord(iPuntosPlayer))
		changeState(NewRecordState::getSingletonPtr());
	else
		changeState(MenuState::getSingletonPtr());

	return true;
}

bool EndGameState::isNewRecord(unsigned int puntos)
{
	bool sw_result = false;
	std::multimap<unsigned int, std::string>::reverse_iterator rit;

	rit = IntroState::getSingleton().gameRecords.rbegin();
	for (int i = 0; rit != IntroState::getSingleton().gameRecords.rend() && sw_result != true; rit++,i++)
	{
		if (puntos > (*rit).first && i < 10)
			sw_result = true;
	}

	return sw_result;
}

CEGUI::MouseButton EndGameState::convertMouseButton(OIS::MouseButtonID id)
{
	CEGUI::MouseButton ceguiId;
	switch(id)
	{
		case OIS::MB_Left:
			ceguiId = CEGUI::LeftButton;
			break;
		case OIS::MB_Right:
			ceguiId = CEGUI::RightButton;
			break;
		case OIS::MB_Middle:
			ceguiId = CEGUI::MiddleButton;
			break;
		default:
			ceguiId = CEGUI::LeftButton;
	}
	return ceguiId;
}


