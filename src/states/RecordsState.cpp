#include "RecordsState.h"

template<> RecordsState* Ogre::Singleton<RecordsState>::msSingleton = 0;

void RecordsState::enter ()
{
	CEGUI::System::getSingleton().getDefaultGUIContext().getMouseCursor().show();
	showRecordsMsgCegui();

	_overlayManager = Ogre::OverlayManager::getSingletonPtr();
}

void RecordsState::exit () {}

void RecordsState::pause () {}

void RecordsState::resume () {}

bool RecordsState::frameStarted(const Ogre::FrameEvent& evt)
{
	CEGUI::System::getSingleton().getDefaultGUIContext().injectTimePulse(evt.timeSinceLastFrame);
	return true;
}

void RecordsState::locateOverlayMousePointer(int x,int y)
{
	Ogre::OverlayElement *oe;
	oe = _overlayManager->getOverlayElement("panelMousePointer");
	oe->setLeft(x); oe->setTop(y);
}

void RecordsState::locateCeguiMousePointer(int x, int y)
{
	int width = InputManager::getSingleton().getMouse()->getMouseState().width;
	int height = InputManager::getSingleton().getMouse()->getMouseState().height;
	CEGUI::System::getSingleton().getDefaultGUIContext().getMouseCursor().setPosition(CEGUI::Vector2f(x,y));
	CEGUI::System::getSingleton().getDefaultGUIContext().injectMouseMove(x/float(width),y/float(height));
}

bool RecordsState::frameEnded(const Ogre::FrameEvent& evt)
{
	return true;
}

void RecordsState::keyPressed(const OIS::KeyEvent &e) {}

void RecordsState::keyReleased(const OIS::KeyEvent &e) {}

void RecordsState::mouseMoved(const OIS::MouseEvent &e)
{

	locateOverlayMousePointer(e.state.X.abs, e.state.Y.abs);
	locateCeguiMousePointer(e.state.X.abs, e.state.Y.abs);
}

void RecordsState::mousePressed(const OIS::MouseEvent &e, OIS::MouseButtonID id)
{
	CEGUI::System::getSingleton().getDefaultGUIContext().injectMouseButtonDown(convertMouseButton(id));
}

void RecordsState::mouseReleased(const OIS::MouseEvent &e, OIS::MouseButtonID id)
{
	CEGUI::System::getSingleton().getDefaultGUIContext().injectMouseButtonUp(convertMouseButton(id));
}

RecordsState* RecordsState::getSingletonPtr ()
{
	return msSingleton;
}

RecordsState& RecordsState::getSingleton ()
{ 
	assert(msSingleton);
	return *msSingleton;
}

void RecordsState::fillRecordsTable(CEGUI::Window* win)
{
	std::stringstream sLabelName;
	std::stringstream sText;

	std::multimap<unsigned int, std::string>::reverse_iterator rit;

	rit = IntroState::getSingleton().gameRecords.rbegin();
	for (int i = 0; rit != IntroState::getSingleton().gameRecords.rend() && i < MAX_PLAYER_RECORDS; rit++,i++)
	{
		// POSICION
		sLabelName.str(""); sText.str("");
		sLabelName << "lbl_Pos" << i+1;
		sText << "[font='major_shift-18']" << i+1;
		win->getChild(sLabelName.str())->setText(sText.str());

		// NOMBRE
		sLabelName.str(""); sText.str("");
		sLabelName << "lbl_Name" << i+1;
		sText << "[font='major_shift-18']" << (*rit).second;
		win->getChild(sLabelName.str())->setText(sText.str());

		// PUNTOS
		sLabelName.str(""); sText.str("");
		sLabelName << "lbl_Puntos" << i+1;
		sText << "[font='major_shift-18']" << (*rit).first;
		win->getChild(sLabelName.str())->setText(sText.str());
	}
}

void RecordsState::showRecordsMsgCegui()
{
	//Sheet
	CEGUI::Window* _ceguiSheet = CEGUI::WindowManager::getSingleton().createWindow("DefaultWindow","records");

	//Config Window
	CEGUI::Window* recordsMsg = CEGUI::WindowManager::getSingleton().loadLayoutFromFile("ceguiRecords.layout");

	fillRecordsTable(recordsMsg);

	// OK
	CEGUI::Window* okButton = recordsMsg->getChild("btn_Volver");
	okButton->subscribeEvent( CEGUI::PushButton::EventClicked,
							  CEGUI::Event::Subscriber(&RecordsState::BotonVolver, this));

	//Attaching buttons
	_ceguiSheet->addChild(recordsMsg);
	CEGUI::System::getSingleton().getDefaultGUIContext().setRootWindow(_ceguiSheet);

}

bool RecordsState::BotonVolver(const CEGUI::EventArgs &e)
{
	changeState(MenuState::getSingletonPtr());
	return true;
}

CEGUI::MouseButton RecordsState::convertMouseButton(OIS::MouseButtonID id)
{
	CEGUI::MouseButton ceguiId;
	switch(id)
	{
		case OIS::MB_Left:
			ceguiId = CEGUI::LeftButton;
			break;
		case OIS::MB_Right:
			ceguiId = CEGUI::RightButton;
			break;
		case OIS::MB_Middle:
			ceguiId = CEGUI::MiddleButton;
			break;
		default:
			ceguiId = CEGUI::LeftButton;
	}
	return ceguiId;
}


