#include "PlayState.h"
#include "MyUtils.h"
#include <ctime>
#include <RendererModules/Ogre/Renderer.h>
#include <RendererModules/Ogre/Texture.h>


template<> PlayState* Ogre::Singleton<PlayState>::msSingleton = 0;

PlayState::PlayState()
{
	_player 				= 0;
	_containerCamera		= 0;
	_cameraPitchNode		= 0;

	_scene					= 0;
	_levelCEGUI				= 0;

	_messageInitLevelTimer	= 0;
	_ceguiSheet				= 0;
	_inGameCEGUI			= 0;
	_RTTWindow				= 0;
}

void PlayState::enter ()
{
	PlayerFactory playerFactory;
	_player	= (Player*)playerFactory.createCharacter(EntityManager::getSingleton().getPlayerType()->getInitialPosition());
	EntityManager::getSingleton().getPlayer()->CoordsToCellIndex();

	_startLevel 			= false;
	_fadedLevelNumber		= false;
	
	// creamos timer para ir decrementando alpha de letras del cegui y ocultarlo cuando sea transparente
	_messageInitLevelTimer 	= new MessageInitLevelTimer();
	_messageInitLevelTimer->runTimer();
	_messageInitLevelTimer->start();
	
	initGame();
	showCEGUILevelNumber();

	createScene();		// creamos la escena
	
	_exitGame 	= false;
	_gameOver	= false;
	
	_cameraPitchNode->setOrientation(Ogre::Quaternion::IDENTITY);
}

void PlayState::initGame()
{
	_root = Ogre::Root::getSingletonPtr();

	// Se recupera el gestor de escena y la cámara.
	_sceneMgr 		= _root->getSceneManager("SceneManager");

	//_sceneMgr->clearScene();
	_root->getAutoCreatedWindow()->removeAllViewports();

	_renderWindow 	= _root->getAutoCreatedWindow();
	_mainCamera		= _sceneMgr->getCamera("mainCamera");
	_viewport 		= _renderWindow->addViewport(_mainCamera);
	_overlayManager = Ogre::OverlayManager::getSingletonPtr();

	// Metemos una luz ambiental, una luz que no tiene fuente de origen. Ilumina a todos los objetos
	_sceneMgr->setAmbientLight(Ogre::ColourValue(0.3, 0.3, 0.3));

	// Establecemos sombra
	_sceneMgr->setShadowTechnique(Ogre::SHADOWTYPE_STENCIL_ADDITIVE);

	// Creamos el plano de imagen (lienzo) asociado a la camara
	Ogre::ColourValue fadeColour(0.3, 0.3, 0.3);
	_viewport->setBackgroundColour(fadeColour);	// color de fondo del viewport(negro)
	
	// establecemos neblina
	//_sceneMgr->setFog(Ogre::FOG_EXP2, Ogre::ColourValue(0.2,0.2,0.2), 0.05);
	
	// establecemos SkyDome
	_sceneMgr->setSkyDome(true, "nightskycloud",
						  25,						// curvatura de la boveda, valores entre 2 y 65. menores=mejor efecto en distancias lejanas
						  20);						// numero de veces que se repite la imagen
													// distancia entre la camara y el skydome
													// true / false el skydome se renderiza antes que el resto de la escena

	locateDirectionalLight(Ogre::Vector3(1, -1, -3));
	locateMainCamera();

	_sceneMgr->getRootSceneNode()->addChild(EntityManager::getSingleton().getBaseNode());

	Ogre::Overlay *overlayMouse = _overlayManager->getByName("mousePointer");
	overlayMouse->hide();

	// musica del juego
	DB_SIN_SONIDO(IntroState::getSingleton().getMainThemeTrackPtr()->play();)

}

void PlayState::showCEGUILevelNumber()
{
	//Sheet
	CEGUI::Window* ceguiSheet = CEGUI::WindowManager::getSingleton().createWindow("DefaultWindow","beginLevel");

	//Config Window
	_levelCEGUI = CEGUI::WindowManager::getSingleton().loadLayoutFromFile("beginLevel.layout");

	std::stringstream levelNumber;

	levelNumber << "[font='DoubleFeature100']" << getScene()->getLevel();
	_levelCEGUI->getChild("labelLevel")->setText("[font='DoubleFeature40'] LEVEL: ");
	_levelCEGUI->getChild("labelNumLevel")->setText(levelNumber.str());

	//Attaching buttons
	ceguiSheet->addChild(_levelCEGUI);
	CEGUI::System::getSingleton().getDefaultGUIContext().setRootWindow(ceguiSheet);
}

void PlayState::createEnemies()
{
	// recorremos los generadores, comprobando si quedan enemigos por crear y si quedan, creamos uno...
	std::vector<Generator*> g = _scene->getZombieGenerators();
	std::vector<Generator*>::iterator it = g.begin();

	ZombieFactory zombieFactory;

	for (; it != g.end(); it++)
	{
		int numChars = (*it)->getNumCharacters();
		if (numChars > 0)
		{
			Zombie* zombie = static_cast<Zombie*>(zombieFactory.createCharacter((*it)->getPosition()));

			(*it)->setNumCharacters(--numChars);

			// establecemos camino al target
			Cell zombieCell = _scene->grid(zombie->getCellX(), zombie->getCellY());
			Cell targetCell = _scene->grid(zombie->getTarget()->getCellX(),  zombie->getTarget()->getCellY());

			std::deque<Cell*> path = _scene->calculateAStarPath(zombieCell, targetCell);
			zombie->setPath(path);
//_scene->DebugCalculatedPath(path);
		}
	}
}

void PlayState::createHumans()
{
	// recorremos los generadores, comprobando si quedan enemigos por crear y si quedan, creamos uno...
		std::vector<Generator*> g = _scene->getHumanGenerators();
		std::vector<Generator*>::iterator it = g.begin();

		HumanFactory humanFactory;

		for (; it != g.end(); it++)
		{
			int numChars = (*it)->getNumCharacters();
			if (numChars > 0)
			{
				Character* human = humanFactory.createCharacter((*it)->getPosition());

				(*it)->setNumCharacters(--numChars);

				// establecemos camino al player
				Cell humanCell = _scene->grid(human->getCellX(), human->getCellY());
				Cell playerCell = _scene->grid(_player->getCellX(),  _player->getCellY());

				std::deque<Cell*> path = _scene->calculateAStarPath(humanCell, playerCell);
				human->setPath(path);
//_scene->DebugCalculatedPath(path);
			}
		}
}

void PlayState::locateMainCamera()
{
	_mainCamera->setNearClipDistance(0.5);				// establecemos plano cercano del frustum
	_mainCamera->setFarClipDistance(300);				// establecemos plano lejano del frustum
							
	double width	= _viewport->getActualWidth();		// recogemos ancho del viewport actual
	double height	= _viewport->getActualHeight();		// recogemos alto del viewport actual
	_mainCamera->setAspectRatio(width / height);		// calculamos ratio (4:3 = 1,333 16:9 1,777)


	// Crear los nodos de movimiento
	_containerCamera = _player->getNode()->createChildSceneNode();
	_cameraPitchNode = _containerCamera->createChildSceneNode();
	_cameraPitchNode->attachObject(_mainCamera);
	_cameraPitchNode->setOrientation(Ogre::Quaternion::IDENTITY);

	attachWeaponToCameraPitch();

	// el player debe mirar hacia delante
	_containerCamera->setPosition(0,0.8,-0.2);

	_mainCamera->setPosition(Ogre::Vector3(0, 0, 0));	// posicionamos...
	_mainCamera->setOrientation(Ogre::Quaternion::IDENTITY);

	std::cout << _player->getNode()->getOrientation().w << ","
			  << _player->getNode()->getOrientation().x << ","
			  << _player->getNode()->getOrientation().y << ","
			  << _player->getNode()->getOrientation().z << std::endl;

	std::cout << _mainCamera->getOrientation().w << ","
			  << _mainCamera->getOrientation().x << ","
			  << _mainCamera->getOrientation().y << ","
			  << _mainCamera->getOrientation().z << std::endl;

}

void PlayState::DettachWeaponToCameraPitch()
{
	_cameraPitchNode->removeChild(_player->getWeapon()->getNode());
}

void PlayState::attachWeaponToCameraPitch()
{
	_cameraPitchNode->addChild(_player->getWeapon()->getNode());

	_player->getWeapon()->getNode()->setPosition(0.4,-0.2,-0.9);

}

void PlayState::addScene(Scene* scn)
{
	_scene = scn;
}



int PlayState::getLives() const {
	return _lives;
}

void PlayState::setLives(int number) {
	_lives = number;
}



void PlayState::decrementLive(int number) {
	_lives = _lives - number;
	if (_lives <= 0) {
		_lives = 0;
		_gameOver = true;
	}
}

void PlayState::saveVolumeInMuteOn(int vol)
{
	_volumeSavedOnMute = vol;
DB_TRACE(std::cout << "Volumen Salvado: " << _volumeSavedOnMute << std::endl;)
}

int PlayState::getVolumeInMuteOff(void) const
{
	return _volumeSavedOnMute;
}

void PlayState::setMute(bool mute)
{
	_mute = mute;
}

bool PlayState::getMute() const
{
	return _mute;
}

void PlayState::locateDirectionalLight(Ogre::Vector3 lightDirection)
{
	Ogre::Light* light;

	// Creamos la luz
	light = _sceneMgr->createLight("LightingNode");
	light->setType(Ogre::Light::LT_DIRECTIONAL);
	light->setDirection(lightDirection);
	_sceneMgr->getRootSceneNode()->attachObject(light);
	light->setDiffuseColour(0.5, 0.5, 0.7);
	light->setSpecularColour(0.5, 0.5, 0.7);
}

void PlayState::exit ()
{
	destroyAll();

	// paramos musica del juego
	DB_SIN_SONIDO(IntroState::getSingleton().getMainThemeTrackPtr()->stop();)

	// ocultamos los overlays
	hideHudOverlay();
}

void PlayState::pause()
{
	// PAUSAR MP3 no funciona con SDL2, asi que lo PARAMOS
	DB_SIN_SONIDO(IntroState::getSingleton().getMainThemeTrackPtr()->stop();)
}

void PlayState::resume()
{
	if (_exitGame)
	{
		changeState(MenuState::getSingletonPtr());
	}
	else if (isGameOver())
	{
		changeState(EndGameState::getSingletonPtr());
	}
	else
	{
		CEGUI::System::getSingleton().getDefaultGUIContext().setRootWindow(_ceguiSheet);
		// continuamos musica del juego
		DB_SIN_SONIDO(IntroState::getSingleton().getMainThemeTrackPtr()->play();)
	}
}


bool PlayState::frameEnded(const Ogre::FrameEvent& evt)
{
	_deltaT = evt.timeSinceLastFrame;
	PhysicsManager::getSingleton().getWorld()->stepSimulation(_deltaT,10); // Actualizar simulacion Bullet
	if (_exitGame)
		changeState(MenuState::getSingletonPtr());

	return true;
}

void PlayState::setExitGame(bool status) {
	_exitGame = status;
}

void PlayState::keyPressed(const OIS::KeyEvent &e)
{
DB_TRACE(std::cout << __FILE__ << " " << __func__ << " KEY PRESSED: " << e.key << std::endl;)

	// Tecla p --> PauseState.
	if (e.key == OIS::KC_P)
	{
		pushState(PauseState::getSingletonPtr());
	}
	else if(e.key == OIS::KC_ESCAPE)
	{
		pushState(MenuEscState::getSingletonPtr());

	}
	else if(e.key == OIS::KC_F)
	{
		if (_sceneMgr->getFogMode() == Ogre::FOG_NONE)
			_sceneMgr->setFog(Ogre::FOG_EXP2, Ogre::ColourValue(0.2,0.2,0.2), 0.05);
		else
			_sceneMgr->setFog(Ogre::FOG_NONE);
	}
}

void PlayState::keyReleased(const OIS::KeyEvent &e)
{
DB_TRACE(std::cout << __FILE__ << " " << __func__ << " KEY RELEASED: " << e.key << std::endl;)
}


bool PlayState::frameStarted(const Ogre::FrameEvent& evt)
{

	_deltaT = evt.timeSinceLastFrame;
	PhysicsManager::getSingleton().getWorld()->stepSimulation(_deltaT,10); // Actualizar simulacion Bullet

	Ogre::Vector3 vt(0,0,0);
	Ogre::Real tSpeed = _player->getSpeed();
	if(	InputManager::getSingleton().getKeyboard()->isKeyDown(OIS::KC_RSHIFT) ||
		InputManager::getSingleton().getKeyboard()->isKeyDown(OIS::KC_LSHIFT) )		tSpeed *= 2;

	if(	InputManager::getSingleton().getKeyboard()->isKeyDown(OIS::KC_UP) ||
		InputManager::getSingleton().getKeyboard()->isKeyDown(OIS::KC_W) )			vt+=Ogre::Vector3(0,0,-tSpeed);

	if(	InputManager::getSingleton().getKeyboard()->isKeyDown(OIS::KC_DOWN) ||
		InputManager::getSingleton().getKeyboard()->isKeyDown(OIS::KC_S) )			vt+=Ogre::Vector3(0,0,tSpeed);

	if(	InputManager::getSingleton().getKeyboard()->isKeyDown(OIS::KC_LEFT)	||
		InputManager::getSingleton().getKeyboard()->isKeyDown(OIS::KC_A) )			vt+=Ogre::Vector3(-tSpeed,0,0);

	if(	InputManager::getSingleton().getKeyboard()->isKeyDown(OIS::KC_RIGHT) ||
		InputManager::getSingleton().getKeyboard()->isKeyDown(OIS::KC_D) )			vt+=Ogre::Vector3(tSpeed,0,0);

	if(	InputManager::getSingleton().getKeyboard()->isKeyDown(OIS::KC_1) )
	{
		if (_player->getInventaryWeapon(EN_MACHINEGUN) && _player->getWeapon()->getWeaponType()!=EN_MACHINEGUN)
		{
			if (_player->getState() != EN_CHANGE_WEAPON_DEVASTATOR &&
				_player->getState() != EN_CHANGE_WEAPON_MACHINEGUN)
			{
				_player->setState(EN_CHANGE_WEAPON_MACHINEGUN);
				Ogre::AnimationState* down = _player->getWeapon()->getDownAnimation();
				Ogre::AnimationState* shoot = _player->getWeapon()->getShootAnimation();
				shoot->setEnabled(false);
				down->setEnabled(true);
				down->setLoop(false);
				down->setTimePosition(0);
			}
		}
	}

	if(	InputManager::getSingleton().getKeyboard()->isKeyDown(OIS::KC_2) ) 
	{
		if (_player->getInventaryWeapon(EN_DEVASTATOR) && _player->getWeapon()->getWeaponType()!=EN_DEVASTATOR)
		{

			if (_player->getState() != EN_CHANGE_WEAPON_DEVASTATOR &&
				_player->getState() != EN_CHANGE_WEAPON_MACHINEGUN) {
				_player->setState(EN_CHANGE_WEAPON_DEVASTATOR);
				Ogre::AnimationState* down = _player->getWeapon()->getDownAnimation();
				Ogre::AnimationState* shoot = _player->getWeapon()->getShootAnimation();
				shoot->setEnabled(false);
				down->setEnabled(true);
				down->setLoop(false);
				down->setTimePosition(0);
			}
		}
	}

	_player->changeWeapon(_player->getState(),_deltaT);

	_player->translate(_deltaT, vt);
	_player->lookToTarget(_sceneMgr);
	_player->CoordsToCellIndex();

	float charsCreateTimeClocks = ( clock() - _charsCreateTime );
	// si ha pasado 1 segundo (en ticks de reloj)
	if (charsCreateTimeClocks > CLOCKS_PER_SEC)
	{
		// comprobamos si ya podemos empezar a sacar enemigos....
		if(_startLevel == true && _fadedLevelNumber == true)
		{
			createEnemies();
			createHumans();
		}
		_charsCreateTime = clock();
	}

	// si ya ha terminado el tiempo de mostrar el cartel de nivel y aun no se ha hecho todo el fundido... fundimos a transparente
	if(_startLevel == true && _fadedLevelNumber == false)
	{
		fadeOutLevelNumber();
		if (_fadedLevelNumber==true)
		{
			createHudOverlay();	// creamos el overlay
			createInGameCEGUI();
		}
	}

	std::vector<Character*>& zombies = EntityManager::getSingleton().getZombies();
	std::vector<Character*>::iterator it_zombies;

	// recorremos arrays de zombies, comprobando si esta vivo o muerto, para desplazarle o mostrar la animacion de muerte
	for(it_zombies = zombies.begin(); it_zombies != zombies.end(); it_zombies++)
		(*it_zombies)->turn(_deltaT);

	std::vector<Character*>::iterator it_z;
	for(it_z = zombies.begin(); it_z != zombies.end(); it_z++)
	{
		if ((*it_z)->getState() == EN_CLEAR)
		{
			delete (*it_z);
			it_z = zombies.erase(it_z);
			break;
		}
	}

	std::vector<Character*>& humans = EntityManager::getSingleton().getHumans();
	std::vector<Character*>::iterator it_humans;

	// recorremos arrays de humanos, comprobando si esta vivo o muerto, para desplazarle o mostrar la animacion de muerte
	for(it_humans = humans.begin(); it_humans != humans.end(); it_humans++)
		(*it_humans)->turn(_deltaT);

	processCollisions();

	if (_mouseLeftPressed)
	{
		if (_player->getState() != EN_CHANGE_WEAPON_DEVASTATOR &&
			_player->getState() != EN_CHANGE_WEAPON_MACHINEGUN)
		{
			float impulseDuration = ( clock() - _impulseTime );
			int impulsePercent = 0;

			// limitamos la pulsacion de raton a 1 segundo (en ticks de reloj)
			if (impulseDuration > CLOCKS_PER_SEC) impulseDuration = CLOCKS_PER_SEC;
			// calculamos el porcentaje de fuerza a añadir al tiro
			impulsePercent = ((int)(impulseDuration * 100) / (CLOCKS_PER_SEC));
			showForce(impulsePercent);
		}
	}

	if (_fadedLevelNumber == true)
		updateMiniMap();

	return true;
}

void PlayState::processCollisions()
{
	int collisionType = PhysicsManager::getSingleton().DetectCollision();


		switch(collisionType)
		{
			case COLTYPE_NO_COLLISION:
				break;
			case COLTYPE_ZOMBIE_PLAYER:
			case COLTYPE_PLAYER_ZOMBIE:
				break;

			case COLTYPE_ZOMBIE_PROJECTILE:
				vaporizeZombie(PhysicsManager::getSingleton().getLastObjACollision()->getName());
				destroyProjectile(PhysicsManager::getSingleton().getLastObjBCollision()->getName());
				break;
			case COLTYPE_PROJECTILE_ZOMBIE:
				vaporizeZombie(PhysicsManager::getSingleton().getLastObjBCollision()->getName());
				destroyProjectile(PhysicsManager::getSingleton().getLastObjACollision()->getName());
				break;
			case COLTYPE_ZOMBIE_HUMAN:
				killHuman(PhysicsManager::getSingleton().getLastObjBCollision()->getName());
				break;
			case COLTYPE_HUMAN_ZOMBIE:
				killHuman(PhysicsManager::getSingleton().getLastObjACollision()->getName());
				break;
		}

}

void PlayState::destroyProjectile(std::string projectileName)
{
	EntityManager::getSingleton().removeProjectile(projectileName);
}

void PlayState::killHuman(std::string humanDead)
{
	std::vector<Character*>::iterator it;
	it = EntityManager::getSingleton().getHumanIterator(humanDead);

	if ( (*it)->getState() != EN_DEAD )
	{
		(*it)->decrementLife(1);
		if((*it)->getLife() <= 0)
		{
			(*it)->setState(EN_DEAD);
			(*it)->getDeadAnimation()->setEnabled(true);
			(*it)->getDeadAnimation()->setTimePosition(0);
			(*it)->getWalkingAnimation()->setEnabled(false);
		}
	}
}

void PlayState::killZombie(std::string zombieDead)
{
	std::vector<Character*>::iterator it;
	it = EntityManager::getSingleton().getZombieIterator(zombieDead);

	if ( (*it)->getState() != EN_DEAD && (*it)->getState() != EN_DEAD_VAPORIZE)
	{
		(*it)->decrementLife(1);
		if((*it)->getLife() <= 0)
		{
			(*it)->setState(EN_DEAD);
			(*it)->getDeadAnimation()->setEnabled(true);
			(*it)->getDeadAnimation()->setTimePosition(0);
			(*it)->getWalkingAnimation()->setEnabled(false);
		}
	}
}


void PlayState::vaporizeZombie(std::string zombieDead)
{
	std::vector<Character*>::iterator it;
	it = EntityManager::getSingleton().getZombieIterator(zombieDead);

	if ( (*it)->getState() != EN_CLEAR && (*it)->getState() != EN_DEAD && (*it)->getState() != EN_DEAD_VAPORIZE)
	{
		(*it)->decrementLife(EntityManager::getSingleton().getPlayer()->getWeapon()->getForce());

		if((*it)->getLife() <= 0)
		{
			(*it)->setState(EN_DEAD_VAPORIZE);
			(*it)->getDeadVaporizeAnimation()->setEnabled(true);
			(*it)->getDeadVaporizeAnimation()->setTimePosition(0);
			(*it)->getWalkingAnimation()->setEnabled(false);
		}
	}
}


Ogre::Camera* PlayState::getMainCamera()
{
	return _mainCamera;
}

void PlayState::mouseMoved(const OIS::MouseEvent &e)
{
	// Gestion del overlay (CURSOR)-----------------------------
	// posiciones del puntero del raton en pixeles
	int posx = e.state.X.abs;
	int posy = e.state.Y.abs;
	static int posz = e.state.Z.abs;

	float speedPitch = _player->getSpeedPitch();

	float rotx = e.state.X.rel * _deltaT * -1;
	float roty = e.state.Y.rel * _deltaT * -1;

	// comprobamos si ha girado la rueda del raton, para cambiar de arma
	if (posz != e.state.Z.abs)
	{
//		if (posz < e.state.Z.abs)
//			_player->nextWeapon();
//		if (posz < e.state.Z.abs)
//			_player->prevWeapon();

		_player->getWeapon()->getNode()->pitch(Ogre::Degree(-90));
		posz = e.state.Z.abs;
	}

	_player->getNode()->yaw(Ogre::Radian(rotx/speedPitch),Ogre::SceneNode::TS_LOCAL);
	_cameraPitchNode->pitch(Ogre::Radian(roty/speedPitch),Ogre::SceneNode::TS_LOCAL);

	_player->getRigidBody()->getBulletRigidBody()->getWorldTransform().setRotation(
			btQuaternion(0,
			_player->getNode()->getOrientation().y,
			 0,
			_player->getNode()->getOrientation().w)
	);

     // Angle of rotation around the X-axis.
	 float pitchAngle = (2 * Ogre::Degree(Ogre::Math::ACos(this->_cameraPitchNode->getOrientation().w)).valueDegrees());

	 // Just to determine the sign of the angle we pick up above, the
	 // value itself does not interest us.
	 float pitchAngleSign = this->_cameraPitchNode->getOrientation().x;

	 // Limit the pitch between -90 degress and +90 degrees, Quake3-style.
	 if (pitchAngle > 90.0f)
	  {
	      if (pitchAngleSign > 0)
	          // Set orientation to 90 degrees on X-axis.
	          this->_cameraPitchNode->setOrientation(Ogre::Quaternion(Ogre::Math::Sqrt(0.5f),
	                                                                 Ogre::Math::Sqrt(0.5f), 0, 0));
	      else if (pitchAngleSign < 0)
	          // Sets orientation to -90 degrees on X-axis.
	          this->_cameraPitchNode->setOrientation(Ogre::Quaternion(Ogre::Math::Sqrt(0.5f),
	                                                                 -Ogre::Math::Sqrt(0.5f), 0, 0));
	  }
	//std::cout << pitchAngle << "-" << pitchAngleSign << std::endl;

	locateOverlayMousePointer(posx,posy);
	locateCeguiMousePointer(posx,posy);
}

void PlayState::locateOverlayMousePointer(int x,int y)
{
	Ogre::OverlayElement *oe;
	oe = _overlayManager->getOverlayElement("panelMousePointer");
	oe->setLeft(x); oe->setTop(y);

}

void PlayState::locateCeguiMousePointer(int x, int y)
{
	int width = InputManager::getSingleton().getMouse()->getMouseState().width;
	int height = InputManager::getSingleton().getMouse()->getMouseState().height;
	CEGUI::System::getSingleton().getDefaultGUIContext().getMouseCursor().setPosition(CEGUI::Vector2f(x,y));
	CEGUI::System::getSingleton().getDefaultGUIContext().injectMouseMove(x/float(width),y/float(height));
}


void PlayState::mousePressed(const OIS::MouseEvent &e, OIS::MouseButtonID id)
{
	if (id == OIS::MB_Left)
	{
		_mouseLeftPressed = true;
		_impulseTime = clock();

		if (_player->getState() == EN_CHANGE_WEAPON_DEVASTATOR ||
			_player->getState() == EN_CHANGE_WEAPON_MACHINEGUN)
			return;

		switch(_player->getWeapon()->getWeaponType())
		{
			case EN_DEVASTATOR:
				IntroState::getSingleton().getPowerShotFXPtr()->play();
				break;
			case EN_MACHINEGUN:
cout << "machine gun shoot!" <<  endl;
				IntroState::getSingleton().getMachineGunShotFXPtr()->play();
				_player->getWeapon()->getShootAnimation()->setTimePosition(0.0);
				_player->getWeapon()->getShootAnimation()->setEnabled(true);
				_player->getWeapon()->getShootAnimation()->setLoop(false);

				_player->getWeapon()->shoot(0);
				break;
			case EN_NONE:
			case EN_GUN:
			case EN_SPECIAL_WEAPON:
				break;
		}
	}
	CEGUI::System::getSingleton().getDefaultGUIContext().injectMouseButtonDown(convertMouseButton(id));
}

void PlayState::updateProjectileCounter()
{
	Ogre::OverlayElement *oe;
	oe = _overlayManager->getOverlayElement("txtProjectileCounter");
	oe->setCaption(Ogre::StringConverter::toString(EntityManager::getSingleton().getPlayer()->getWeapon()->getAmmo()));

}

void PlayState::mouseReleased(const OIS::MouseEvent &e, OIS::MouseButtonID id)
{

	if (id == OIS::MB_Left)
	{
		if (_player->getState() == EN_CHANGE_WEAPON_DEVASTATOR ||
			_player->getState() == EN_CHANGE_WEAPON_MACHINEGUN)
			return;

		_mouseLeftPressed = false;
		switch(_player->getWeapon()->getWeaponType())
		{
			case EN_DEVASTATOR:
				if(_player->getWeapon()->getAmmo() > 0)
				{
					float impulseDuration = ( clock() - _impulseTime );
					int impulsePercent = 0;
//std::cout << "TIEMPO PULSACION: " << impulseDuration << " " <<  CLOCKS_PER_SEC << endl;

					// limitamos la pulsacion de raton a 1 segundo (en ticks de reloj)
					if (impulseDuration > CLOCKS_PER_SEC)
						impulseDuration = CLOCKS_PER_SEC;

					// calculamos el porcentaje de fuerza a añadir al tiro
					impulsePercent = (int)(impulseDuration * 100) / (CLOCKS_PER_SEC);
//std::cout << "PORCENTAJE PULSACION: " << impulsePercent << "%" << endl;
					_player->getWeapon()->shoot(impulsePercent);
//
					_player->getWeapon()->getShootAnimation()->setTimePosition(0.0);
					_player->getWeapon()->getShootAnimation()->setEnabled(true);
					_player->getWeapon()->getShootAnimation()->setLoop(false);

					//_player->getWeapon()->decAmmo();
					updateProjectileCounter();
					IntroState::getSingleton().getShotFXPtr()->play();
				}
				else
				{
					IntroState::getSingleton().getNoAmmoFXPtr()->play();
				}
//cout << "AMMO: " << getPlayer()->getWeapon()->getAmmo() << endl;
				showForce(0);
				break;
			case EN_MACHINEGUN:
			case EN_NONE:
			case EN_GUN:
			case EN_SPECIAL_WEAPON:
				break;
		}
	}

	CEGUI::System::getSingleton().getDefaultGUIContext().injectMouseButtonUp(convertMouseButton(id));
}

PlayState* PlayState::getSingletonPtr ()
{
	return msSingleton;
}

PlayState& PlayState::getSingleton ()
{ 
	assert(msSingleton);
	return *msSingleton;
}

void PlayState::createScene()
{
	_charsCreateTime = clock();
	_scene->getPathFindingEnemyTimer()->start();
	_scene->getPathFindingEnemyTimer()->runTimer();

	_scene->getPathFindingHumanTimer()->setOldPlayerX(_player->getCellX());
	_scene->getPathFindingHumanTimer()->setOldPlayerY(_player->getCellY());
	_scene->getPathFindingHumanTimer()->start();
	_scene->getPathFindingHumanTimer()->runTimer();
}

void PlayState::showForce(int force)
{

		Ogre::Overlay *overlayForceGraph20 = _overlayManager->getByName("forceGraph20");
		Ogre::Overlay *overlayForceGraph40 = _overlayManager->getByName("forceGraph40");
		Ogre::Overlay *overlayForceGraph60 = _overlayManager->getByName("forceGraph60");
		Ogre::Overlay *overlayForceGraph80 = _overlayManager->getByName("forceGraph80");
		Ogre::Overlay *overlayForceGraph100 = _overlayManager->getByName("forceGraph100");

		overlayForceGraph20->hide();
		overlayForceGraph40->hide();
		overlayForceGraph60->hide();
		overlayForceGraph80->hide();
		overlayForceGraph100->hide();

		if (force > 0 )
			overlayForceGraph20->show();
		if (force > 20 )
				overlayForceGraph40->show();
		if (force > 40 )
				overlayForceGraph60->show();
		if (force > 60 )
				overlayForceGraph80->show();
		if (force > 80 )
				overlayForceGraph100->show();

}

void PlayState::createHudOverlay()
{
	_overlayManager = Ogre::OverlayManager::getSingletonPtr();

	Ogre::Overlay *overlayTarget = _overlayManager->getByName("target");
	
	if (_startLevel == true)
		overlayTarget->show();

	Ogre::Overlay *overlayForceContainer = _overlayManager->getByName("forceContainer");
	overlayForceContainer->show();

	Ogre::Overlay *overlayProjectileCounter= _overlayManager->getByName("projectileCounter");
	overlayProjectileCounter->show();
	updateProjectileCounter();
}

void PlayState::hideHudOverlay()
{
	_overlayManager = Ogre::OverlayManager::getSingletonPtr();

	Ogre::Overlay *overlayTarget = _overlayManager->getByName("target");
	Ogre::Overlay *overlayMouse = _overlayManager->getByName("mousePointer");
	overlayMouse->show();
	overlayTarget->hide();

	Ogre::Overlay *overlayForceContainer = _overlayManager->getByName("forceContainer");
	overlayForceContainer->hide();
	showForce(0);

	Ogre::Overlay *overlayProjectileCounter= _overlayManager->getByName("projectileCounter");
	overlayProjectileCounter->hide();
}

void PlayState::createInGameCEGUI()
{
	//Sheet
	_ceguiSheet = CEGUI::WindowManager::getSingleton().createWindow("DefaultWindow","inGame");

	// Frame Window (Contenedor principal)
	_inGameCEGUI = CEGUI::WindowManager::getSingleton().loadLayoutFromFile("inGameLayout.layout");

	createMiniMap();
	addMissionsCEGUI();

	// Añadimos el frame contenedor a la ventana CEGUI
	_ceguiSheet->addChild(_inGameCEGUI);
	CEGUI::System::getSingleton().getDefaultGUIContext().setRootWindow(_ceguiSheet);
}

void PlayState::addMissionsCEGUI()
{
	std::vector<Mission*> vMissions;
	std::vector<Mission*>::iterator it_missions;

	vMissions = MissionsManager::getSingleton().getMissions();
	it_missions = vMissions.begin();

	for(int i=1; it_missions != vMissions.end(); it_missions++,i++)
	{
		std::stringstream windowCEGUI;
		windowCEGUI << "Checkbox" << i;

		// establecemos estado de checkbox
		CEGUI::ToggleButton* missionCheck = static_cast<CEGUI::ToggleButton*>(_inGameCEGUI->getChild(windowCEGUI.str()));
		missionCheck->setSelected((*it_missions)->getComplete());

		// establecemos estado del texto de la mision
		windowCEGUI.str("");
		windowCEGUI << "LabelGoal" << i;
		CEGUI::Window* missionLabel = _inGameCEGUI->getChild(windowCEGUI.str());
		std::stringstream missionText;
		missionText << "[font='DejaVuSans-08'][colour='96FFFFFF']" << (*it_missions)->getGoal();
		missionLabel->setProperty("HorzFormatting","LeftAligned");
		missionLabel->setText(missionText.str());

		if((*it_missions)->getTotalUnits() > 0)
		{
			// establecemos estado de unidades completadas
			windowCEGUI.str("");
			windowCEGUI << "LabelUnits" << i;
			CEGUI::Window* missionLabelUnits = _inGameCEGUI->getChild(windowCEGUI.str());
			std::stringstream missionUnitsText;
			// si la mision aun no esta
			missionUnitsText << "[font='DejaVuSans-08'][colour='9600FF00'](" << (*it_missions)->getActualUnits() << " / " << (*it_missions)->getTotalUnits() << ")";
			missionLabelUnits->setProperty("HorzFormatting","LeftAligned");
			missionLabelUnits->setText(missionUnitsText.str());
		}
	}
}

void PlayState::createMiniMap()
{
	_miniMapCamera = _sceneMgr->createCamera("miniMapCamera");
	_player->getNode()->attachObject(_miniMapCamera);
	_miniMapCamera->setOrientation(Ogre::Quaternion::IDENTITY);
	_miniMapCamera->setPosition(Ogre::Vector3(0, 50, 0));
	_miniMapCamera->setNearClipDistance(5);
	_miniMapCamera->setFarClipDistance(300);

	_miniMapCamera->pitch(Ogre::Degree(-90));

	// el aspect ratio de la camara principal es el que usaremos para la camara del minimapa
//	double width	= _viewport->getActualWidth();		// recogemos ancho del viewport actual
//	double height	= _viewport->getActualHeight();		// recogemos alto del viewport actual
	_miniMapCamera->setAspectRatio(1);

	CEGUI::OgreRenderer* renderer = IntroState::getSingleton().getRenderer();

	//Minimap Window
	CEGUI::Window* miniMapCEGUI = _inGameCEGUI->getChild("CamWindow");

	//Render Texture - to - target
	Ogre::TexturePtr tex = _root->getTextureManager()->createManual("RTT",
																    Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME,
																    Ogre::TEX_TYPE_2D,
																    512,
																    512,
																    0,
																    Ogre::PF_R8G8B8,
																    Ogre::TU_RENDERTARGET);

	Ogre::RenderTexture* rtex = tex->getBuffer()->getRenderTarget();

	Ogre::Viewport* v = rtex->addViewport(_miniMapCamera);
	v->setOverlaysEnabled(false);
	v->setClearEveryFrame(true);
	v->setBackgroundColour(Ogre::ColourValue::Black);

	// para no renderizar al player en este viewport del minimapa,
	// hacemos un XOR para excluir los objetos marcados como NO MOSTRAR EN MINIMAPA de la mascara de TODO
	v->setVisibilityMask(0xFFFFFFFF ^ NOT_SHOWN_IN_MINIMAP);

	CEGUI::Texture& guiTex = renderer->createTexture("CEGUITex", tex);
	CEGUI::OgreTexture& rendererTex = static_cast<CEGUI::OgreTexture&>(guiTex);

	rendererTex.setOgreTexture(tex, false);

	CEGUI::BasicImage* image = static_cast<CEGUI::BasicImage*>(&CEGUI::ImageManager::getSingleton().create("BasicImage", "RTTImage"));

	CEGUI::Rectf imageArea= CEGUI::Rectf(0.0f, 0.0f, guiTex.getSize().d_width, guiTex.getSize().d_height);

	image->setArea(imageArea);
	image->setAutoScaled(CEGUI::ASM_Disabled);
	image->setTexture(&rendererTex);

	_RTTWindow = miniMapCEGUI->getChild("RTTWindow");
	_RTTWindow->setProperty("Image","RTTImage");
}

void PlayState::updateMiniMap()
{
	_RTTWindow->invalidate();
}

CEGUI::MouseButton PlayState::convertMouseButton(OIS::MouseButtonID id)
{
	CEGUI::MouseButton ceguiId;
	switch(id)
	{
		case OIS::MB_Left:
			ceguiId = CEGUI::LeftButton;
			break;
		case OIS::MB_Right:
			ceguiId = CEGUI::RightButton;
			break;
		case OIS::MB_Middle:
			ceguiId = CEGUI::MiddleButton;
			break;
		default:
			ceguiId = CEGUI::LeftButton;
	}
	return ceguiId;
}

bool PlayState::isGameOver() const
{
	return _gameOver;
}

int PlayState::getPointsPlayer() const
{
	return _points;
}

Scene* PlayState::getScene()
{
	return _scene;
}

void PlayState::setStartLevel(bool startLevel)
{
	_startLevel = startLevel;
}

bool PlayState::getStartLevel() const
{
	return _startLevel;
}

Ogre::Real PlayState::getDeltaTime() const
{
	return _deltaT;
}

CEGUI::Window* PlayState::getLevelCEGUI()
{
	return _levelCEGUI;
}

void PlayState::fadeOutLevelNumber()
{
	//	//[colour='FFFF0000'] <= AARRGGBB
	const int TOTAL_MILLISECONDS_FADE 	= 2000;	// 2 segundos
	const int ALPHA_LEVEL_INITIAL 		= 0xFF;	// 255. No se puede recoger el alpha actual del texto CEGUI
	static int alphaLevel 				= ALPHA_LEVEL_INITIAL;
	float percentFadeTime				= 0.0f;
	float alphaDecrease 				= 0;

	// calculamos el porcentaje de tiempo total que ha transcurrido desde la ultima llamada....
	percentFadeTime = (_deltaT * 100) / TOTAL_MILLISECONDS_FADE;
	// con ese porcentaje, calculamos el nivel de alfa que tenemos que decrementar....
	alphaDecrease = (percentFadeTime * ALPHA_LEVEL_INITIAL) / 100;

	//Config Window
//	CEGUI::Window* levelCEGUI = CEGUI::WindowManager::getSingleton().loadLayoutFromFile("beginLevel.layout");
	CEGUI::Window* levelCEGUI = PlayState::getSingleton().getLevelCEGUI();

	std::stringstream alphaLevelStream;
	std::stringstream alphaNumLevelStream;
	int level = PlayState::getSingleton().getScene()->getLevel();
	alphaLevel -= alphaDecrease;
	alphaLevelStream << "[font='DoubleFeature40'][colour='" << std::hex << std::uppercase << alphaLevel << "FF0000'] LEVEL: ";
	alphaNumLevelStream << "[font='DoubleFeature100'][colour='" << std::hex << std::uppercase << alphaLevel << "FF0000']" << level;
//cout << "alphaLevelStream: " << alphaLevelStream.str() << " alphaNumLevelStream: " << alphaNumLevelStream.str() << endl;
	levelCEGUI->getChild("labelLevel")->setText(alphaLevelStream.str());
	levelCEGUI->getChild("labelNumLevel")->setText(alphaNumLevelStream.str());

	// si ya hemos terminado de fundir...
	if (alphaLevel == 0)
	{
		CEGUI::System::getSingleton().getDefaultGUIContext().getRootWindow()->hide();
		_fadedLevelNumber = true;
	}
}

void PlayState::destroyAll()
{
	if (_scene != 0)
	{
		delete _scene;
		_scene = 0;
	}

	if (_messageInitLevelTimer != 0)
	{
		_messageInitLevelTimer->stopTimer();
		delete _messageInitLevelTimer;
		_messageInitLevelTimer = 0;
	}

	_fadedLevelNumber = false;

	IntroState::getSingleton().getRenderer()->destroyTexture("CEGUITex");
	CEGUI::ImageManager::getSingleton().destroy("RTTImage");

	if ( _sceneMgr->hasCamera("miniMapCamera") )
		_sceneMgr->destroyCamera(_miniMapCamera);

	// Desactivar el SkyDome:
	_sceneMgr->setSkyDome(false, "");

	// eliminamos entidades
	EntityManager::getSingleton().destroyAll();

	// eliminamos misiones
	MissionsManager::getSingleton().destroyAll();

	// eliminamos fisicas
	PhysicsManager::getSingleton().destroyAll();


}
