#include "EndLevelState.h"

template<> EndLevelState* Ogre::Singleton<EndLevelState>::msSingleton = 0;

void EndLevelState::enter ()
{
	_overlayManager = Ogre::OverlayManager::getSingletonPtr();
	createOverlay();

}

void EndLevelState::createOverlay() {

	Ogre::Overlay *overlay = _overlayManager->getByName("endLevel");
	overlay->show();
}

void EndLevelState::hideOverlay() {
		Ogre::Overlay *overlay = _overlayManager->getByName("endLevel");
		overlay->hide();
}

void EndLevelState::exit () {
	hideOverlay();
}

void EndLevelState::pause () {}

void EndLevelState::resume () {}

bool EndLevelState::frameStarted(const Ogre::FrameEvent& evt)
{
	return true;
}

bool EndLevelState::frameEnded(const Ogre::FrameEvent& evt)
{
	return true;
}

void EndLevelState::keyPressed(const OIS::KeyEvent &e) {

	int _currentLevel = LoadLevelState::getSingletonPtr()->getCurrentLevel();
		if (e.key == OIS::KC_1)
		{
			LoadLevelState::getSingletonPtr()->setCurrentLevel(_currentLevel+1);
			changeState(LoadLevelState::getSingletonPtr());
		}
}

void EndLevelState::keyReleased(const OIS::KeyEvent &e) {}

void EndLevelState::mouseMoved(const OIS::MouseEvent &e)
{
	// Gestion del overlay (CURSOR)-----------------------------
	// posiciones del puntero del raton en pixeles
	int posx = e.state.X.abs;
	int posy = e.state.Y.abs;

	locateOverlayMousePointer(posx,posy);
}

void EndLevelState::locateOverlayMousePointer(int x,int y)
{
	Ogre::OverlayElement *oe;
	oe = _overlayManager->getOverlayElement("panelMousePointer");
	oe->setLeft(x); oe->setTop(y);
}

void EndLevelState::mousePressed(const OIS::MouseEvent &e, OIS::MouseButtonID id) {}

void EndLevelState::mouseReleased(const OIS::MouseEvent &e, OIS::MouseButtonID id) {}

EndLevelState* EndLevelState::getSingletonPtr ()
{
	return msSingleton;
}

EndLevelState& EndLevelState::getSingleton ()
{
	assert(msSingleton);
	return *msSingleton;
}

