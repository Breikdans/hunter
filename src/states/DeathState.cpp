#include "DeathState.h"

template<> DeathState* Ogre::Singleton<DeathState>::msSingleton = 0;

void DeathState::enter ()
{
	_overlayManager = Ogre::OverlayManager::getSingletonPtr();
	_fader			= new Fader("FadeInOut", "FadeMaterial", &_faderCBK);
	Character::setMove(false);
	IntroState::getSingleton().getZombie1FXPtr()->play();
	createOverlay();
//	popState();
	_fader->startFadeOut(4.5);
////	PlayState::getSingleton().decrementLive(1);

}

void DeathState::createOverlay()
{
	_overlayManager = Ogre::OverlayManager::getSingletonPtr();
	Ogre::Overlay *overlay = _overlayManager->getByName("playerDeath");
	overlay->show();
}

void DeathState::hideOverlay()
{
	_overlayManager = Ogre::OverlayManager::getSingletonPtr();
	Ogre::Overlay *overlay = _overlayManager->getByName("playerDeath");
	overlay->hide();
}

void DeathStateFaderCallback::fadeInCallback(void) {}

void DeathStateFaderCallback::fadeOutCallback(void)
{
	DeathState::getSingleton().getFader()->faderOff();
	PlayState::getSingleton().setExitGame(true);
	DeathState::getSingleton().popState();
}

void DeathState::exit () 
{
	hideOverlay();

	if (_fader != 0)
		delete _fader;
}

void DeathState::pause () {}

void DeathState::resume () {}

bool DeathState::frameStarted(const Ogre::FrameEvent& evt)
{
	 // Fade in/out
	 _fader->fade(evt.timeSinceLastFrame);
	return true;
}

bool DeathState::frameEnded(const Ogre::FrameEvent& evt)
{
	return true;
}

void DeathState::keyPressed(const OIS::KeyEvent &e)
{
	if (e.key == OIS::KC_1)
	{
		popState();
	}
}

void DeathState::keyReleased(const OIS::KeyEvent &e) {}

void DeathState::mouseMoved(const OIS::MouseEvent &e)
{
	// Gestion del overlay (CURSOR)-----------------------------
	// posiciones del puntero del raton en pixeles
	int posx = e.state.X.abs;
	int posy = e.state.Y.abs;

	locateOverlayMousePointer(posx,posy);
}

void DeathState::locateOverlayMousePointer(int x,int y)
{
	Ogre::OverlayElement *oe;
	oe = _overlayManager->getOverlayElement("panelMousePointer");
	oe->setLeft(x); oe->setTop(y);
}

void DeathState::mousePressed(const OIS::MouseEvent &e, OIS::MouseButtonID id) {}

void DeathState::mouseReleased(const OIS::MouseEvent &e, OIS::MouseButtonID id) {}

DeathState* DeathState::getSingletonPtr ()
{
	return msSingleton;
}

DeathState& DeathState::getSingleton ()
{
	assert(msSingleton);
	return *msSingleton;
}

