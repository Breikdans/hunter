#include "Importer.h"
#include "Shapes/OgreBulletCollisionsConvexHullShape.h"
#include "Shapes/OgreBulletCollisionsTrimeshShape.h"
#include "Utils/OgreBulletCollisionsMeshToShapeConverter.h"

template<> Importer* Ogre::Singleton<Importer>::msSingleton = 0;

Importer* Importer::getSingletonPtr ()
{
	return msSingleton;
}

Importer& Importer::getSingleton ()
{
	assert(msSingleton);
	return *msSingleton;
}

/**
 *
 */
void Importer::parseScene (const char * path, Scene *scene)
{
	// Inicialización.
	Ogre::SceneNode* baseNode = Ogre::Root::getSingletonPtr()->getSceneManager("SceneManager")->createSceneNode("baseNode");
	EntityManager::getSingleton().setBaseNode(baseNode);

	// Creación de mundo físico
	PhysicsManager::getSingleton().CreateWorld();

	XMLPlatformUtils::Initialize();

	XercesDOMParser* parser = new XercesDOMParser();
	parser->setValidationScheme(XercesDOMParser::Val_Always);

	parser->parse(path);	// indicamos el camino del fichero a parsear

	DOMDocument* xmlDoc;
	DOMElement* elementRoot;

	// Obtener el elemento raíz del documento.
	xmlDoc = parser->getDocument();
	if (xmlDoc==NULL)
	{
		std::string msg = path;
		Ogre::LogManager::getSingleton().logMessage("No se ha encontrado -> "+ msg);
	}
	else
	{
		elementRoot = xmlDoc->getDocumentElement();

		int level	= static_cast<int>(getValueFromTag(elementRoot, "level"));
		int dimX	= static_cast<int>(getValueFromTag(elementRoot, "totalDimensionX"));
		int dimY	= static_cast<int>(getValueFromTag(elementRoot, "totalDimensionY"));
		float dimEdge	= getValueFromTag(elementRoot, "edgeCellDimension");

		if (dimX <= 1 || dimY <= 1)
			throw std::out_of_range("dimensiones de mapa invalidas!");

		scene->setLevel(level);
		scene->setDimensionX(dimX);
		scene->setDimensionY(dimY);
		scene->setDimensionEdge(dimEdge);

		scene->initGrid();

		DB_PARSER(cout << "XXXX level: " << level << endl;)
		cout << "XXXX dimX: " << dimX << endl;
		cout << "XXXX dimY: " << dimY << endl;

		DB_PARSER(cout << "XXXX ROOT NUM NODES: " << elementRoot->getChildNodes()->getLength() << endl;)
		// Procesando los nodos hijos del raíz...
		for (XMLSize_t i = 0; i < elementRoot->getChildNodes()->getLength(); i++ )
		{
			DOMNode* node = elementRoot->getChildNodes()->item(i);
			if (isNodeNamed(node,"grid"))
			{

				parseGrid(node, scene);
				orderGrid(scene);
//				scene->DebugGrid();
			}
			else if (isNodeNamed(node,"player"))
			{
				parsePlayer(node, scene);
			}
			else if (isNodeNamed(node,"zombie1"))
			{
				parseZombie(node);
			}
			else if (isNodeNamed(node,"human1"))
			{
				parseHuman(node);
			}
			else if (isNodeNamed(node,"zombieGenerator"))
			{
				parseZombieGenerator(node, scene);
			}
			else if (isNodeNamed(node,"humanGenerator"))
			{
				parseHumanGenerator(node, scene);
			}
			else if (isNodeNamed(node,"element"))
			{
				parseElement(node, scene);
			}
			else if (isNodeNamed(node,"missions"))
			{
				parseMissions(node);
			}
			else if (isNodeNamed(node,"weapon"))
			{
				parseWeapon(node);
			}

		}// Fin for

		// Pintar objeto en los centros de casilla
		//showCenterCells(scene);

	}

	delete parser;
}

void Importer::showCenterCells(Scene *scene){


	Ogre::SceneManager* sceneMgr 	= Ogre::Root::getSingletonPtr()->getSceneManager("SceneManager");
	for (int x=0;x<scene->getDimensionX();x++) {
		for (int y=0;y<scene->getDimensionY();y++) {
			std::stringstream name;
			name << "centro_" << x << "_" << y;
			Ogre::SceneNode* scnNode 		= sceneMgr->createSceneNode(name.str());
			scnNode->setPosition(scene->grid(x,y).getX(),
								 0,
								 -(scene->grid(x,y).getY()));
			Ogre::Entity *entity 			= sceneMgr->createEntity(name.str(), "centroCasilla.mesh");
			scnNode->attachObject(entity);
			EntityManager::getSingleton().getBaseNode()->addChild(scnNode);

		}
	}
}

void Importer::orderGrid(Scene *scn)
{
	Cell aux;
	int totalDim = scn->getDimensionX() * scn->getDimensionY();
	for (int i=0; i<totalDim; i++)
	{
		for (int j=i+1; j<totalDim; j++)
		{
			if (scn->grid(j).getY() > scn->grid(i).getY())
			{
				aux = scn->grid(j);
				scn->grid(j) = scn->grid(i);
				scn->grid(i) = aux;
			}
		}
	}
	for (int i=0; i<totalDim; i++)
	{
		for (int j=i+1; j<totalDim; j++)
		{
			if (scn->grid(j).getY() == scn->grid(i).getY())
			{
				if (scn->grid(j).getX() < scn->grid(i).getX())
				{
					aux = scn->grid(j);
					scn->grid(j) = scn->grid(i);
					scn->grid(i) = aux;
				}
			}
		}
	}

	// ahora establecemos indexX e indexY
	for (int x=0; x < scn->getDimensionX() ; x++)
	{
		for (int y=0; y < scn->getDimensionY() ; y++)
		{
			scn->grid(x,y).setIndexX(x);
			scn->grid(x,y).setIndexY(y);
		}
	}

}

void Importer::parseCell(DOMNode* node, Scene* scn)
{
	int type = 0;
	int posX = 0, posY = 0;
	
	//int dimX = scn->getDimensionX();
	int dimY = scn->getDimensionY();
	
	int index = atoi(getAttribute(node,"index").c_str());
	std::string	strType	= getAttribute(node, "type");
	
	float x				= getValueFromTag(node, "x");
	float y				= getValueFromTag(node, "y");
	float z				= getValueFromTag(node, "z");
	
	if(strType == "open")
		type = EN_OPEN;
	else if(strType == "closed")
		type = EN_BLOCK;
	
	DB_PARSER(cout << "Cell LEIDO x: " 		<< x << endl;)
	DB_PARSER(cout << "Cell LEIDO y: " 		<< y << endl;)
	DB_PARSER(cout << "Cell LEIDO z: " 		<< z << endl;)
	DB_PARSER(cout << "Cell LEIDO index: " 	<< index << endl;)
	DB_PARSER(cout << "Cell LEIDO type: " 	<< type << endl;)

	// calculamos la posicion en el grid, segun el index leido
	// recibimos las celdas agrupadas por las X (columnas)
	// las Y (filas) empiezan abajo del array
	posX = index / dimY;
	posY = (dimY - (index % dimY)) - 1;

	DB_PARSER(cout << "Cell POSX: " << posX << endl;)
	DB_PARSER(cout << "Cell POSY: " << posY << endl;)
	DB_PARSER(cout << "--------------------" << endl;)

	scn->grid(posX,posY) = Cell(x, y, z, type, posX, posY);

	DB_PARSER(cout << "Cell x: " 			<< scn->grid(posX,posY).getX() << endl;)
	DB_PARSER(cout << "Cell y: " 			<< scn->grid(posX,posY).getY() << endl;)
	DB_PARSER(cout << "Cell z: " 			<< scn->grid(posX,posY).getZ()<< endl;)
	DB_PARSER(cout << "Cell type: " 		<< scn->grid(posX,posY).getType() << endl;)
	DB_PARSER(cout << "Cell index X: " 	<< scn->grid(posX,posY).getIndexX()<< endl;)
	DB_PARSER(cout << "Cell index Y: " 	<< scn->grid(posX,posY).getIndexY()<< endl << endl;)
}

void Importer::parseGrid(DOMNode* node, Scene* scn)
{
	// recorremos todos los hijos del grid
	for (XMLSize_t i = 0; i < node->getChildNodes()->getLength(); i++ )
	{
		DOMNode* frameNode = node->getChildNodes()->item(i);
		if (isNodeNamed(frameNode,"cell"))
		{
			parseCell(frameNode,scn);
		}
	}	
}

void Importer::parsePlayer(DOMNode* node, Scene* scn)
{
	float speed			= getValueFromTag(node, "speed");
	float speedPitch	= getValueFromTag(node, "speedPitch");
	int life			= static_cast<int>(getValueFromTag(node, "life"));
	float x				= getValueFromTag(node, "x");
	float y				= getValueFromTag(node, "y");
	float z				= getValueFromTag(node, "z");
	float rw			= getValueFromTag(node, "rw");
	float rx			= getValueFromTag(node, "rx");
	float ry			= getValueFromTag(node, "ry");
	float rz			= getValueFromTag(node, "rz");
	std::string mesh 	= getStringFromTag(node, "mesh");
	std::string type	= "PLAYER";
	std::string id		= "PLAYER";

	DB_PARSER(cout << "Player LEIDO speed: " << speed << endl;)
	DB_PARSER(cout << "Player LEIDO x: " << x << endl;)
	DB_PARSER(cout << "Player LEIDO y: " << y << endl;)
	DB_PARSER(cout << "Player LEIDO z: " << z << endl;)
	DB_PARSER(cout << "Player LEIDO ammo: " << ammo << endl;)
	DB_PARSER(cout << "Player LEIDO mesh: " << mesh << endl;)

	Player player(0, speed, mesh, speedPitch, 0);
	player.setInitialPosition(Ogre::Vector3(x,z,-y));
	player.setInitialOrientation(Ogre::Quaternion(rw,rx,rz,-ry));
	player.setLife(life);

	getPlayerWeapons(node, player);

	EntityManager::getSingleton().setPlayerType(player);
}

void Importer::getPlayerWeapons(DOMNode* node, Player& player)
{
	// buscamos todos los posibles 'weapon' que contenga el player
	for (XMLSize_t i = 0; i < node->getChildNodes()->getLength(); i++ )
	{
		DOMNode* weaponNode = node->getChildNodes()->item(i);
		if (isNodeNamed(weaponNode,"weapon"))
		{
			std::string weapon = XMLString::transcode(weaponNode->getFirstChild()->getNodeValue());

			EN_WEAPON_TYPE 	weaponType = getWeaponType(weapon);

			if (weaponType != EN_NONE)
				player.addInitialWeapon(weaponType);
		}
	}
}

void Importer::parseZombie(DOMNode* node)
{
	float speed			= getValueFromTag(node, "speed");
	int life			= static_cast<int>(getValueFromTag(node, "life"));
	std::string mesh 	= getStringFromTag(node, "mesh");

	DB_PARSER(cout << "Zombie LEIDO speed: " << speed << endl;)
	DB_PARSER(cout << "Zombie LEIDO life: " << life << endl;)
	DB_PARSER(cout << "Zombie LEIDO mesh: " << mesh << endl;)

	Zombie zombie(0, speed, mesh, life);
	EntityManager::getSingleton().setZombieType(zombie);
}

void Importer::parseHuman(DOMNode* node)
{
	float speed			= getValueFromTag(node, "speed");
	int life			= static_cast<int>(getValueFromTag(node, "life"));
	std::string mesh 	= getStringFromTag(node, "mesh");

	DB_PARSER(cout << "Human LEIDO speed: " << speed << endl;)
	DB_PARSER(cout << "Human LEIDO life: " << life << endl;)
	DB_PARSER(cout << "Human LEIDO mesh: " << mesh << endl;)

	Human human(0, speed, mesh, life);
	EntityManager::getSingleton().setHumanType(human);
}



void Importer::parseElement(DOMNode* node, Scene* scn)
{
	std::string mesh 	= getStringFromTag(node, "mesh");
	float x				= getValueFromTag(node, "x");
	float y				= getValueFromTag(node, "y");
	float z				= getValueFromTag(node, "z");
	float rw			= getValueFromTag(node, "rw");
	float rx			= getValueFromTag(node, "rx");
	float ry			= getValueFromTag(node, "ry");
	float rz			= getValueFromTag(node, "rz");
	std::string	id		= getAttribute(node, "id");
	std::string	type	= getAttribute(node, "type");

	// Se recupera el gestor de escena y la cámara.
	Ogre::SceneManager* sceneMgr 	= Ogre::Root::getSingletonPtr()->getSceneManager("SceneManager");
	Ogre::SceneNode* scnNode 		= sceneMgr->createSceneNode(id);
	scnNode->setPosition(x, z, -y);

	Ogre::Entity *entity 			= sceneMgr->createEntity(id, mesh);

	scnNode->attachObject(entity);
	EntityManager::getSingleton().getBaseNode()->addChild(scnNode);
	scnNode->setOrientation(rw,rx,rz,-ry);

	DB_PARSER(cout << "Atributo LEIDO id: " 	<< id 	<< endl;)
	DB_PARSER(cout << "Element LEIDO mesh: " 	<< mesh << endl;)
	DB_PARSER(cout << "Element LEIDO x: " 		<< x 	<< endl;)
	DB_PARSER(cout << "Element LEIDO y: " 		<< y 	<< endl;)
	DB_PARSER(cout << "Element LEIDO z: " 		<< z 	<< endl;)
	DB_PARSER(cout << "Element LEIDO Rx: " 		<< rx 	<< endl;)
	DB_PARSER(cout << "Element LEIDO Ry: " 		<< ry 	<< endl;)
	DB_PARSER(cout << "Element LEIDO Rz: " 		<< rz 	<< endl;)

	Element* element = new Element(id, scnNode, mesh);
	EntityManager::getSingleton().addElement(element);

	DB_PARSER(cout << "Atributo id: " 	<< scn->getElements().at(0)->getId() << endl;)
	DB_PARSER(cout << "Element mesh: " 	<< scn->getElements().at(0)->getMesh() << endl;)
	DB_PARSER(cout << "Element x: " 	<< scn->getElements().at(0)->getPosition().x << endl;)
	DB_PARSER(cout << "Element y: " 	<< scn->getElements().at(0)->getPosition().y << endl;)
	DB_PARSER(cout << "Element z: " 	<< scn->getElements().at(0)->getPosition().z << endl;)
	DB_PARSER(cout << "Element Rx: " 	<< rx << endl;)
	DB_PARSER(cout << "Element Ry: " 	<< ry << endl;)
	DB_PARSER(cout << "Element Rz: " 	<< rz << endl;)

	PhysicsManager::getSingleton().createRigidBody(entity, type,Ogre::Vector3(x,z,-y),Ogre::Quaternion(rw,rx,rz,-ry));
}

void Importer::parseMissions(DOMNode* node)
{
	for (XMLSize_t i = 0; i < node->getChildNodes()->getLength(); i++ )
	{
		DOMNode* missionNode = node->getChildNodes()->item(i);
		if (isNodeNamed(missionNode,"goal"))
		{
			parseGoal(missionNode);
		}
	}
}

void Importer::parseGoal(DOMNode* node)
{
	std::string	strType	= getAttribute(node, "type");

	std::string 	goal 		= getStringFromTag(node, "title");
	int				totalUnits	= (int)getValueFromTag(node, "units");
	bool			complete	= false;
	int				actualUnits	= 0;
	EN_MISSION_TYPE missionType;

	if(strType == "killEnemies")
		missionType = EN_KILL_ENEMIES;

	else if(strType == "rescueSurvivors")
		missionType = EN_RESCUE_SURVIVORS;

	else if(strType == "specialWeapon")
		missionType = EN_GATHER_SPECIAL_WEAPON;

	else if(strType == "killBoss")
		missionType = EN_KILL_BOSS;

	Mission* mission = new Mission(goal, complete, totalUnits, actualUnits, missionType);
	MissionsManager::getSingleton().addMission(mission);
}

EN_WEAPON_TYPE Importer::getWeaponType(std::string type)
{
	EN_WEAPON_TYPE weaponType = EN_NONE;

	if(type == "devastator")
		weaponType = EN_DEVASTATOR;

	else if(type == "machineGun")
		weaponType = EN_MACHINEGUN;

	else if(type == "gun")
		weaponType = EN_GUN;

	else if(type == "specialWeapon")
		weaponType = EN_SPECIAL_WEAPON;

	return weaponType;
}

void Importer::parseWeapon(DOMNode* node)
{
	std::string	strType	= getAttribute(node, "type");

	std::string mesh 	= getStringFromTag(node, "mesh");
	int ammo			= static_cast<int>(getValueFromTag(node, "ammo"));
	int force			= static_cast<int>(getValueFromTag(node, "force"));
	EN_WEAPON_TYPE weaponType = getWeaponType(strType);

	Weapon weapon;
	weapon.setAmmo(ammo);
	weapon.setForce(force);
	weapon.setMesh(mesh);
	weapon.setWeaponType(weaponType);
	EntityManager::getSingleton().addWeaponType(weapon);
}

void Importer::parseZombieGenerator(DOMNode* node, Scene* scn)
{
	int numCharacters		= static_cast<int>(getValueFromTag(node, "numCharacters"));
	float x				= getValueFromTag(node, "x");
	float y				= getValueFromTag(node, "y");
	float z				= getValueFromTag(node, "z");

	DB_PARSER(cout << "Generator LEIDO numCharacters: "	<< numCharacters << endl;)
	DB_PARSER(cout << "Generator LEIDO x: " 			<< x << endl;)
	DB_PARSER(cout << "Generator LEIDO y: " 			<< y << endl;)
	DB_PARSER(cout << "Generator LEIDO z: " 			<< z << endl;)
	DB_PARSER(cout << "Generator LEIDO Type: " 			<< "EN_ZOMBIE" << endl;)

	Generator* generator = new Generator(Ogre::Vector3(x, z, -y), EN_ZOMBIE, numCharacters);
	scn->addZombieGenerator(generator);

	DB_PARSER(cout << "Generator numCharacters: "	<< scn->getZombieGenerators().at(0)->getNumCharacters() << endl;)
	DB_PARSER(cout << "Generator x: " 			<< scn->getZombieGenerators().at(0)->getPosition().x << endl;)
	DB_PARSER(cout << "Generator y: " 			<< scn->getZombieGenerators().at(0)->getPosition().y << endl;)
	DB_PARSER(cout << "Generator z: " 			<< scn->getZombieGenerators().at(0)->getPosition().z << endl;)
	DB_PARSER(cout << "Generator Type: " 		<< scn->getZombieGenerators().at(0)->getTypeGenerator() << endl;)

}

void Importer::parseHumanGenerator(DOMNode* node, Scene* scn)
{
	int numCharacters		= static_cast<int>(getValueFromTag(node, "numCharacters"));
	float x				= getValueFromTag(node, "x");
	float y				= getValueFromTag(node, "y");
	float z				= getValueFromTag(node, "z");

	DB_PARSER(cout << "Generator LEIDO numCharacters: "	<< numCharacters << endl;)
	DB_PARSER(cout << "Generator LEIDO x: " 			<< x << endl;)
	DB_PARSER(cout << "Generator LEIDO y: " 			<< y << endl;)
	DB_PARSER(cout << "Generator LEIDO z: " 			<< z << endl;)
	DB_PARSER(cout << "Generator LEIDO Type: " 			<< "EN_HUMAN" << endl;)

	Generator* generator = new Generator(Ogre::Vector3(x, z, -y), EN_HUMAN, numCharacters);
	scn->addHumanGenerator(generator);

	DB_PARSER(cout << "Generator numCharacters: "	<< scn->getZombieGenerators().at(0)->getNumCharacters() << endl;)
	DB_PARSER(cout << "Generator x: " 			<< scn->getZombieGenerators().at(0)->getPosition().x << endl;)
	DB_PARSER(cout << "Generator y: " 			<< scn->getZombieGenerators().at(0)->getPosition().y << endl;)
	DB_PARSER(cout << "Generator z: " 			<< scn->getZombieGenerators().at(0)->getPosition().z << endl;)
	DB_PARSER(cout << "Generator Type: " 		<< scn->getZombieGenerators().at(0)->getTypeGenerator() << endl;)

}



/**
 * Dado un nodo y el nombre del tag a buscar dentro de él, nos retorna su valor en formato float
 *
 * @param: const DOMNode* node 		ENTRADA. Nodo dentro del cual queremos comprobar si se encuentra el tag para recuperar su valor
 * @param: const char *tag			ENTRADA. Nombre del tag a buscar para recoger su valor en formato float
 *
 * @return: float					SALIDA. Valor contenido en el tag especificado
 */
float Importer::getValueFromTag(DOMNode* node, const char* tag)
{
	float ret = 0.0;
	XMLCh *tagXMLCh = XMLString::transcode(tag);
	for (XMLSize_t i = 0; i < node->getChildNodes()->getLength(); i++ )
	{
		DOMNode* aux = node->getChildNodes()->item(i);

		if ( aux->getNodeType() == DOMNode::ELEMENT_NODE &&
			 XMLString::equals(aux->getNodeName(), tagXMLCh) )
		{
			char *tempVal = XMLString::transcode(aux->getFirstChild()->getNodeValue());
			ret = atof(tempVal);
			XMLString::release(&tempVal);
			XMLString::release(&tagXMLCh);
			return ret;
		}
	}

	XMLString::release(&tagXMLCh);
	return ret;
}

/**
 * Dado un nodo y el nombre del tag a buscar dentro de él, nos retorna su valor en formato string
 *
 * @param: const DOMNode* node 		ENTRADA. Nodo dentro del cual queremos comprobar si se encuentra el tag para recuperar su valor
 * @param: const char *tag			ENTRADA. Nombre del tag a buscar para recoger su valor en formato float
 *
 * @return: string					SALIDA. Valor contenido en el tag especificado
 */
std::string Importer::getStringFromTag(DOMNode* node, const char* tag)
{
	std::string ret = "";
	XMLCh *tagXMLCh = XMLString::transcode(tag);
	for (XMLSize_t i = 0; i < node->getChildNodes()->getLength(); i++ )
	{
		DOMNode* aux = node->getChildNodes()->item(i);

		if ( aux->getNodeType() == DOMNode::ELEMENT_NODE &&
			 XMLString::equals(aux->getNodeName(), tagXMLCh) )
		{
			ret = XMLString::transcode(aux->getFirstChild()->getNodeValue());
			XMLString::release(&tagXMLCh);
			return ret;
		}
	}

	XMLString::release(&tagXMLCh);
	return ret;
}

/**
 * Dado un nodo y el nombre del nodo que estamos buscando, nos indicará si nos encontramos en el o no
 *
 * @param: const DOMNode* node 		ENTRADA. Nodo del cual queremos comprobar si nos encontramos en el o no
 * @param: const char *attr			ENTRADA. Nombre del nodo a comprobar si estamos en el o no
 *
 * @return: bool					SALIDA. true = encontrado, false = no encontrado
 */
bool Importer::isNodeNamed(DOMNode* node,const char* name)
{
	bool result=false;
	if (node->getNodeType() == DOMNode::ELEMENT_NODE)
	{
		XMLCh* name_ch = XMLString::transcode(name);
		if (XMLString::equals(node->getNodeName(), name_ch))
		{
			result=true;
		}
		XMLString::release(&name_ch);
	}
	return result;
}

/**
 * Dado un nodo y el atributo que queremos recoger, nos devuelve su valor en un string
 *
 * @param: const DOMNode* node 		ENTRADA. Nodo del cual queremos coger el valor de un atributo
 * @param: const char *attr			ENTRADA. Nombre del atributo a recoger su valor
 *
 * @return: string					SALIDA. Valor del Atributo
 */
string Importer::getAttribute(const DOMNode* node, const char *attr)
{
	DOMNamedNodeMap* attributes = node->getAttributes();
	XMLCh* attribute = XMLString::transcode(attr);
	DOMNode* strAttr = attributes->getNamedItem(attribute);
	if(strAttr == 0)
		throw("atributo no existe");
	char *tempVar = XMLString::transcode(strAttr->getNodeValue());
	string result = tempVar;

	XMLString::release(&attribute);
	XMLString::release(&tempVar);
	return result;
}
