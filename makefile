# --------------------------------------------------------------------
#	$@ - Nombre del objetivo
#	$^ - Todas las dependencias
#	$< - Primera dependencia
# --------------------------------------------------------------------

EXEC := hunter

DIRSRC 			:= src/
DIRHEA 			:= include/
DIROBJ 			:= obj/

DIRSRC_MGR 		:= $(DIRSRC)managers/
DIRSRC_STATES	:= $(DIRSRC)states/
DIRSRC_ENTITIES	:= $(DIRSRC)entities/

DIRHEA_MGR 		:= $(DIRHEA)managers/
DIRHEA_STATES	:= $(DIRHEA)states/
DIRHEA_ENTITIES	:= $(DIRHEA)entities/

CXX := g++

# Flags de compilación -----------------------------------------------
CXXFLAGS := -I $(DIRHEA) -I $(DIRHEA_MGR) -I $(DIRHEA_STATES) -I $(DIRHEA_ENTITIES) -Wall -I/usr/local/include/cegui-0/CEGUI -I/usr/local/include/cegui-0
CXXFLAGS += `pkg-config --cflags OGRE OGRE-Overlay`
CXXFLAGS += `pkg-config --cflags OIS`
CXXFLAGS += `pkg-config --cflags SDL2_mixer`
CXXFLAGS +=	`pkg-config --cflags OgreBullet bullet`
#CXXFLAGS += -std=c++11

# Flags del linker ---------------------------------------------------
LDFLAGS := `pkg-config --libs OGRE OGRE-Overlay`
LDFLAGS += `pkg-config --libs-only-L SDL2_mixer`
LDFLAGS += `pkg-config --libs-only-l SDL2_mixer glu`
LDFLAGS += `pkg-config --libs gl xerces-c`
LDFLAGS += `pkg-config --libs-only-l OgreBullet bullet` 
LDFLAGS += -lIce -lIceUtil
LDFLAGS += -lOIS -lGL -lstdc++ -lboost_system  -lCEGUIBase-0 -lCEGUIOgreRenderer-0
LDFLAGS += -lboost_date_time -lboost_thread -lpthread

# Modo de compilación (-mode=release -mode=debug) --------------------
ifeq ($(mode), release) 
	CXXFLAGS += -O2 -D_RELEASE
else 
	CXXFLAGS += -g -D_DEBUG
	mode := debug
endif

# Obtención automática de la lista de objetos a compilar -------------
OBJS := $(subst $(DIRSRC_MGR), $(DIROBJ), $(patsubst %.cpp, %.o, $(wildcard $(DIRSRC_MGR)*.cpp)))
OBJS += $(subst $(DIRSRC_STATES), $(DIROBJ), $(patsubst %.cpp, %.o, $(wildcard $(DIRSRC_STATES)*.cpp)))
OBJS += $(subst $(DIRSRC_ENTITIES), $(DIROBJ), $(patsubst %.cpp, %.o, $(wildcard $(DIRSRC_ENTITIES)*.cpp)))
OBJS += $(subst $(DIRSRC), $(DIROBJ), $(patsubst %.cpp, %.o, $(wildcard $(DIRSRC)*.cpp)))

.PHONY: all clean

all: dirs info $(EXEC)

info:
	@echo '------------------------------------------------------'
	@echo '>>> Using mode $(mode)'
	@echo '    (Please, call "make" with [mode=debug|release])  '
	@echo '------------------------------------------------------'

dirs:
	mkdir -p $(DIROBJ)
# Enlazado -----------------------------------------------------------
$(EXEC): $(OBJS)
	@echo
	@echo 'Enlazando...'
	$(CXX) -o $@  $^ $(LDFLAGS)
	@echo
	@echo 'Compilacion terminada!'

# Compilación --------------------------------------------------------
$(DIROBJ)main.o: $(DIRSRC)main.cpp
	$(CXX) $(CXXFLAGS) -c $< -o $@ $(LDFLAGS)

$(DIROBJ)%.o: $(DIRSRC_MGR)%.cpp $(DIRHEA_MGR)%.h 
	$(CXX) $(CXXFLAGS) -c $< -o $@ $(LDFLAGS)	

$(DIROBJ)%.o: $(DIRSRC_STATES)%.cpp $(DIRHEA_STATES)%.h 
	$(CXX) $(CXXFLAGS) -c $< -o $@ $(LDFLAGS)	

$(DIROBJ)%.o: $(DIRSRC_ENTITIES)%.cpp $(DIRHEA_ENTITIES)%.h 
	$(CXX) $(CXXFLAGS) -c $< -o $@ $(LDFLAGS)		

$(DIROBJ)%.o: $(DIRSRC)%.cpp $(DIRHEA)%.h 
	$(CXX) $(CXXFLAGS) -c $< -o $@ $(LDFLAGS)
	

# Limpieza de temporales ---------------------------------------------
clean:
	rm -f *.log $(EXEC) *~ $(DIROBJ)* $(DIRSRC)*~ $(DIRHEA)*~  



